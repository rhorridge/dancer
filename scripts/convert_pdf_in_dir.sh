#!/usr/bin/env bash

find "$1" -type f -name '*.pdf' -exec scripts/convert_pdf.sh {} \;
