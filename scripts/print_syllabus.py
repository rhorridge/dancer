#!/usr/bin/env python
"""Print the requested syllabus in JSON format.
"""

import argparse
from pydantic import validate_arguments
from dancer import syllabus
from dancer.dance import get_dance, Dance


@validate_arguments
def select_syllabus(dance: Dance) -> syllabus.Syllabus:
    """Return the syllabus for a given dance. Raises an exception if
    the dance does not have a syllabus.
    """

    match dance.abbr:
        case 'W':
            return syllabus.WaltzSyllabus
        case 'T':
            return syllabus.TangoSyllabus
        case 'V':
            return syllabus.VienneseWaltzSyllabus
        case 'F':
            return syllabus.FoxtrotSyllabus
        case 'Q':
            return syllabus.QuickstepSyllabus
        case 'C':
            return syllabus.ChaChaChaSyllabus
        case 'S':
            return syllabus.SambaSyllabus
        case 'R':
            return syllabus.RumbaSyllabus
        case 'P':
            return syllabus.PasoDobleSyllabus
        case 'J':
            return syllabus.JiveSyllabus
    raise NotImplementedError(
        'Syllabus for %s is not implemented yet!' % dance
    )


@validate_arguments
def main(option: str):
    dance = get_dance(option)
    if not dance:
        raise NotImplementedError('Cannot get a dance for %s', option)
    syllabus = select_syllabus(dance)
    print(syllabus.json())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Print the requested syllabus in JSON format.')
    parser.add_argument('--dance', '-d', type=str)
    args = parser.parse_args()
    main(args.dance)
