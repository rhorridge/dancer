#!/usr/bin/env python
# Attempt to scrutineer a competition.

import os
import out
import logging
import sys
from dancer.competition import Competition
from dancer.scrutineer import recall, place

out.configure(
    level=os.environ.get('LOGLEVEL', 'debug'),
    default_level='debug',
    theme='production',
)


#SCRUTLOG = logging.getLogger('dancer.scrutineer.place')
#SCRUTLOG.setLevel(logging.WARNING)
LOG = logging.getLogger(__name__)


def main(competition: Competition):
    """Run the program."""

    for event_number, event in sorted(competition.events.items()):
        LOG.info('Scrutineering Event %2d: %s', event_number, event.dance)
        for round_number, prev_round in sorted(event.rounds.items()):
            LOG.info('Scrutineering Round %1d: %s', round_number, prev_round)
            next_round_number = round_number + 1
            scrutineered_round = recall(
                judges=sorted(competition.adjudicators.keys()),
                marks=prev_round.marks(),
            )
            if prev_round != scrutineered_round:
                LOG.warning(
                    'we recalled %d couples but %d couples were actually recalled',
                    len(scrutineered_round.couples_recalled()),
                    len(prev_round.couples_recalled()),
                )
            if (next_round := event.rounds.get(next_round_number)):
                # Recall n couples and compare
                if len(next_round) != len(prev_round.couples_recalled()):
                    LOG.warning(
                        '%d couples recalled but says that there are %d couples, expect it is reporting all couples as being in that round',
                        len(prev_round.couples_recalled()),
                        len(next_round),
                    )
            else:
                # Next round is a final
                if event.final:
                    if len(event.final) != len(prev_round.couples_recalled()):
                        LOG.warning(
                            "expected %d couples in final but got %d",
                            len(prev_round.couples_recalled()),
                            len(event.final),
                        )
        if not event.final:
            LOG.error('Missing final for event %2d!', event_number)
            continue
        LOG.info('Scrutineering Final: %s', event.final)
        scrutineered_final = place(
            judges=sorted(competition.adjudicators.keys()),
            placings=event.final.placings(),
        )
        if event.final != scrutineered_final:
            LOG.warning(
                'we placed differently!'
            )
            LOG.warning('Them:\n%s', event.final.placings_table())
            LOG.warning('Us  :\n%s', scrutineered_final.placings_table())

        LOG.info('Final Placings:')
        for key, val in sorted(event.final.final_placings().items()):
            LOG.info('%1d: %3d (%s)', key, val, competition.competitor(val))


if __name__ == '__main__':
    LOG.info('Starting!')
    comp = Competition.parse_file(sys.argv[1])
    main(comp)
