#!/usr/bin/env python3
# Scrape an individual competion and print it out.

import sys
import logging
import argparse
from dancer import environ_or_required
from dancer.web.dpaonline import Scraper


LOG = logging.getLogger(__name__)


def parse_args(args: list[str]) -> object:
    """Return parsed arguments."""

    parser = argparse.ArgumentParser(
        prog="dancer-scrape-individual-competition",
        description="Scrape an individual competition and print it out",
    )
    web = parser.add_argument_group(
        "web",
        description="Upstream websites to consume data from",
    )
    web.add_argument(
        "--dpaonline-url",
        help="URL to retrieve DPA formatted information from",
        type=str,
        **environ_or_required("DPAONLINE_URL"),
    )
    web.add_argument(
        "--download",
        help="Whether or not to download anything from the web",
        action=argparse.BooleanOptionalAction,
        default=False,
    )
    file_ = parser.add_argument_group(
        "file",
        description="Local files to consume data from",
    )
    file_.add_argument(
        "--dpaonline-file",
        help="HTML file to retrieve DPA formatted information from",
        type=str,
        **environ_or_required("DPAONLINE_FILE"),
    )
    comp = parser.add_argument_group(
        "competition",
        description="Specific competition we are interested in",
    )
    comp.add_argument(
        "--competition-name",
        help="Name of the competition we are interested in",
        type=str,
        **environ_or_required("COMPETITION_NAME"),
    )

    return parser.parse_args(args)


def main(*args: list[str]) -> None:
    """Run the main program."""

    args = parse_args(args)

    filesystem = Scraper(uri=args.dpaonline_file)
    filesystem.scrape()

    LOG.info("%d competitions in %s", len(filesystem.content), filesystem.uri)

    competition = [
        comp for comp in filesystem.content if comp.comp.name == args.competition_name
    ]
    if len(competition) == 0:
        raise RuntimeError(f"No comp found matching {args.competition_name}!")
    if len(competition) > 1:
        raise RuntimeError(
            f"Multiple comps found matching {args.competition_name}! {competition}"
        )

    competition = competition[0]
    LOG.info("Found competition: %s", competition)

    if args.download:
        competition.scrape()
    print(competition.json(indent=4))


if __name__ == "__main__":
    import out

    out.configure(level="debug", theme="production", highlight=False)
    main(*sys.argv[1:])
