#!/usr/bin/env bash
# Print the syllabus in a tabular format.


python3 -m scripts.print_syllabus --dance "$1" | \
    jq -r '["Name", "Start Foot", "Start Position", "Next Foot", "Next Position"], ["----", "----------", "--------------", "---------", "-------------"], (.figures[] | [.name, .start_foot.foot + " "  + .start_foot.direction, .start_position, .next_foot.foot + " " + .next_foot.direction, .next_position]) | @csv' |\
    column -s ',' -t | \
    tr -d '"'
