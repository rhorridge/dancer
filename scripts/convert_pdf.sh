#!/usr/bin/env bash

OUTFILE="${1%.*}.txt"
if [ -f "${OUTFILE}" ]; then
    echo "$1 already converted"
else
    pdftotext -fixed 6.7 "$1"
fi
