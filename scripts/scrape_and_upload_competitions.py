#!/usr/bin/env python3
# Scrape competitions from online and upload them to Redis and Google Calendar

import sys
import logging
import argparse
from dancer import environ_or_required
from dancer.web.dpaonline import Scraper, ScrapedCompetition, DEFAULT_HEADERS
from dancer.interfaces.redis import competition_event as redis
from dancer.interfaces.google import competition_event as google


LOG = logging.getLogger(__name__)


def parse_args(args: list[str]) -> object:
    """Return parsed arguments."""

    parser = argparse.ArgumentParser(
        prog="dancer-scrape-and-upload-competitions",
        description="Scrape competitions from online resources and upload them to Redis and Google Cloud.",
    )
    redis = parser.add_argument_group(
        "redis", description="Configuration of the Redis server"
    )
    redis.add_argument(
        "--redis-uri",
        help="Full Redis DSN to connect to the Redis server",
        type=str,
        **environ_or_required("REDIS_URI"),
    )
    google = parser.add_argument_group(
        "google",
        description="Configuration to connect to Google Calendar",
    )
    google.add_argument(
        "--token-file",
        help="JSON file to save or retrieve auth token file from",
        type=str,
        **environ_or_required("TOKEN_FILE"),
    )
    google.add_argument(
        "--client-secret-file",
        help="JSON file to retrieve client ID and secret from",
        type=str,
        **environ_or_required("CLIENT_SECRET_FILE"),
    )
    google.add_argument(
        "--calendar-id",
        help="Unique ID of the Google Calendar to use",
        type=str,
        **environ_or_required("CALENDAR_ID"),
    )
    web = parser.add_argument_group(
        "web",
        description="Upstream websites to consume data from",
    )
    web.add_argument(
        "--dpaonline-url",
        help="URL to retrieve DPA formatted information from",
        type=str,
        **environ_or_required("DPAONLINE_URL"),
    )
    web.add_argument(
        "--download",
        help="Whether or not to download anything from the web",
        action=argparse.BooleanOptionalAction,
        default=False,
    )
    file_ = parser.add_argument_group(
        "file",
        description="Local files to consume data from",
    )
    file_.add_argument(
        "--dpaonline-file",
        help="HTML file to retrieve DPA formatted information from",
        type=str,
        **environ_or_required("DPAONLINE_FILE"),
    )

    return parser.parse_args(args)


def main(*args: list[str]) -> None:
    """Run the main program."""

    args = parse_args(args)

    # Set up Redis interface
    r_cnx = redis.CompetitionEventInterface(
        uri=args.redis_uri,
        prefix="competition-event:dpa-ballroom-and-latin",
    )

    # Fetch existing keys
    keys_redis = r_cnx.query()
    LOG.info("%d keys existing in %s", len(keys_redis), r_cnx.prefix)

    filesystem = Scraper(uri=args.dpaonline_file)
    filesystem.scrape()

    LOG.info("%d competitions in %s", len(filesystem.content), filesystem.uri)

    s_keys_redis = set(keys_redis)
    s_keys_filesystem = {elem.comp.id for elem in filesystem.content}

    diff = s_keys_filesystem.difference(s_keys_redis)

    LOG.info("%d keys in %s not in %s", len(diff), filesystem.uri, r_cnx.prefix)

    for elem in filesystem.content:
        if elem.comp.id in diff:
            r_cnx.store(elem.comp.id, elem.comp)

    keys_redis = r_cnx.query()
    LOG.info("%d keys existing in %s", len(keys_redis), r_cnx.prefix)

    for key in s_keys_redis:
        # we want to check if the full competition calendar details are present
        # for each key.
        obj = r_cnx.fetch(key)
        LOG.debug(obj)
        if hasattr(obj, "organiser"):
            # the organiser exists - therefore this is the correct object
            LOG.debug("Full event calendar details already present.")
        else:
            if args.download:
                to_scrape = ScrapedCompetition(
                    url=obj.url,
                    headers={"User-Agent": DEFAULT_HEADERS},
                    comp=obj,
                )
                to_scrape.scrape()
                r_cnx.store(to_scrape.content.id, to_scrape.content)
                LOG.debug(to_scrape.content.json(indent=4))

    if args.download:
        scraper = Scraper(uri=args.dpaonline_url)
        scraper.scrape()

        LOG.info("%d competitions in %s", len(scraper.content), scraper.uri)

        s_keys_redis = set(keys_redis)
        s_keys_scraper = {elem.comp.id for elem in scraper.content}

        diff = s_keys_scraper.difference(s_keys_redis)

        LOG.info("%d keys in %s not in %s", len(diff), scraper.uri, r_cnx.prefix)

        for elem in scraper.content:
            if elem.comp.id in diff:
                to_scrape = ScrapedCompetition(
                    url=elem.comp.url,
                    headers={"User-Agent": DEFAULT_HEADERS},
                    comp=elem.comp,
                )
                to_scrape.scrape()
                r_cnx.store(to_scrape.content.id, to_scrape.content)
                LOG.debug(to_scrape.content.json(indent=4))

        keys_redis = r_cnx.query()
        LOG.info("%d keys existing in %s", len(keys_redis), r_cnx.prefix)

    g_iface = google.CompetitionEventInterface(
        token_file=args.token_file,
        client_secret_file=args.client_secret_file,
        calendar_id=args.calendar_id,
    )

    keys_google = g_iface.query()
    LOG.info(
        "%d keys existing in Google Calendar %s", len(keys_google), g_iface.calendar_id
    )

    for key in keys_redis:
        data = r_cnx.fetch(key)
        try:
            g_iface.store(data.id, data)
        except FileExistsError:
            LOG.error(
                "Event %s already exists in Google Calendar %s!",
                data.id,
                g_iface.calendar_id,
            )
        else:
            LOG.info(
                "Stored event %s in Google Calendar %s", data.id, g_iface.calendar_id
            )

    keys_google = g_iface.query()
    LOG.info(
        "%d keys existing in Google Calendar %s", len(keys_google), g_iface.calendar_id
    )


if __name__ == "__main__":
    import out

    out.configure(level="debug", theme="production", highlight=False)
    main(*sys.argv[1:])
