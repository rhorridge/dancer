#!/usr/bin/env bash
# Print competition events in a tabular format.

python3 -m dancer.competition_events | \
    jq -r '["Name", "Dances", "Restricted?"], ["----", "------", "-----------"], (.[] | [.name, (.dances | map(.name) | join("，")), .restricted]) | @csv' | \
    column -s ',' -t | \
    tr -d '"'
