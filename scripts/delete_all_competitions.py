#!/usr/bin/env python3
# Delete competitions from Google Calendar

import sys
import logging
import argparse
from dancer import environ_or_required
from dancer.web.dpaonline import Scraper
from dancer.interfaces.redis import competition_event as redis
from dancer.interfaces.google import competition_event as google

LOG = logging.getLogger(__name__)


def parse_args(args: list[str]) -> object:
    """Return parsed arguments."""

    parser = argparse.ArgumentParser(
        prog="dancer-delete-all-competitions",
        description="Delete all competions from Google Calendar",
    )
    google = parser.add_argument_group(
        "google",
        description="Configuration to connect to Google Calendar",
    )
    google.add_argument(
        "--token-file",
        help="JSON file to save or retrieve auth token file from",
        type=str,
        **environ_or_required("TOKEN_FILE"),
    )
    google.add_argument(
        "--client-secret-file",
        help="JSON file to retrieve client ID and secret from",
        type=str,
        **environ_or_required("CLIENT_SECRET_FILE"),
    )
    google.add_argument(
        "--calendar-id",
        help="Unique ID of the Google Calendar to use",
        type=str,
        **environ_or_required("CALENDAR_ID"),
    )
    return parser.parse_args(args)


def main(*args: list[str]) -> None:
    """Run the main program."""

    args = parse_args(args)

    g_iface = google.CompetitionEventInterface(
        token_file=args.token_file,
        client_secret_file=args.client_secret_file,
        calendar_id=args.calendar_id,
    )

    keys_google = g_iface.query()
    LOG.info(
        "%d keys existing in Google Calendar %s", len(keys_google), g_iface.calendar_id
    )

    for key in keys_google:
        g_iface.delete(key)


if __name__ == "__main__":
    import out

    out.configure(level="debug", theme="production", highlight=False)
    main(*sys.argv[1:])
