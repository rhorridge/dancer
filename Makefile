.PHONY: test
test: test_unit

.PHONY: test_unit
test_unit:
	poetry run pytest -v -m 'not slow ' .

.PHONY: test_coverage
test_coverage:
	poetry run pytest -m 'not slow ' --cov=dancer .

.PHONY: webcoverage
webcoverage: test_coverage
	poetry run coverage html
	cd htmlcov && python -m http.server 8080
