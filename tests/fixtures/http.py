import json as _json


class Base:
    """Base Response class."""

    @property
    def ok(self) -> bool:
        """Return True if status_code < 400."""

        return self.status_code < 400

    @property
    def content(self) -> bytes:
        """Return byte-content of the JSON data."""

        return self.text.encode("utf-8")


class Response(Base):
    """Override for the `requests.Response` object."""

    def __init__(self, json: dict, *, status_code: int = 200) -> None:
        self.json = json
        self.status_code = status_code

    @property
    def text(self) -> bytes:
        """Return UTF-8 string content of the JSON data."""

        return _json.dumps(self.json)


class HtmlResponse(Base):
    """Response that returns HTML as text."""

    def __init__(self, text: str, *, status_code: int = 200) -> None:
        self.text = text
        self.status_code = status_code

    @property
    def json(self) -> None:
        """Raise an exception."""

        raise ValueError("No JSON here!")
