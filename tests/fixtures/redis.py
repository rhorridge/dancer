"""Fixtures for Redis database.
"""

import pytest
import fakeredis


@pytest.fixture
def redis_db(monkeypatch):
    r_cnx = fakeredis.FakeStrictRedis(decode_responses=True)

    monkeypatch.setattr(
        "redis.from_url",
        lambda *args, **kwargs: r_cnx,
    )

    yield r_cnx
