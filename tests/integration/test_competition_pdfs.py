"""Test viewing and listing competition PDFs.
"""

from dancer.interfaces.fs import CompetitionPDFInterface


def test_read():
    """Test reading of files and directoriea.
    """

    iface = CompetitionPDFInterface(directory='data')
    with iface.get(year=2021, name='Nottingham Varsity Dancesport Competition') as iface:
        iface.get(name='01 Beginner Waltz - Round 2 (1)')
