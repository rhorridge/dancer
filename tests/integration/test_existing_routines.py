"""This is a place to put existing known routines, to see if they are valid.
"""

import pytest
import dancer.choreography as stp
import dancer.syllabus as syl
from dancer.steps import RoutineError
from dancer.routine import Routine


WALTZ_1 = [
    stp.waltz.NaturalTurn1_3,
    stp.waltz.NaturalSpinTurn1_3,
    stp.waltz.TurningLockToR,
    stp.waltz.WeaveFromPP,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.RunningSpinTurn,
    stp.waltz.OutsideChange,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.OpenImpetus,
    stp.waltz.ChasseFromPP,
    stp.waltz.ClosedWing,
    stp.waltz.DoubleReverseSpin,
    stp.waltz.FallawayReverseSlipPivot,
    stp.waltz.DoubleReverseSpin,
    stp.waltz.ProgressiveChasseToR,
    stp.waltz.BackwardLockStep,
    stp.waltz.OpenImpetus,
    stp.waltz.FallawayNaturalTurn,
]

QUICKSTEP_1 = [
    stp.waltz.NaturalTurn1_3,
    stp.waltz.NaturalSpinTurn1_3,
    stp.quickstep.ProgressiveChasseToL,
    stp.quickstep.QuickOpenReverse,
    stp.quickstep.FourQuickRun,
    stp.quickstep.TippleChasseToL1_4,
    stp.quickstep.TippleChasseToR1_4,
    stp.quickstep.ForwardLockStep2_4,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.BackwardLockStep,
    stp.quickstep.OpenRunningFinish,
    stp.quickstep.PassingNaturalTurn,
    stp.quickstep.RunningFinish,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.NaturalSpinTurn1_3,
    stp.quickstep.V6,
]

FOXTROT_1 = [
    stp.foxtrot.FeatherStep,
    stp.foxtrot.ReverseTurn1_3,
    stp.foxtrot.FeatherFinish,
    stp.foxtrot.ThreeStep,
    stp.foxtrot.CurvedFeather,
    stp.waltz.OpenImpetus,
    stp.foxtrot.FeatherEnding,
    stp.waltz.OpenTelemark,
    stp.waltz.NaturalTurn1_3,
    stp.foxtrot.OutsideSwivel,
    stp.foxtrot.WeaveFromPP2,
    stp.foxtrot.ThreeStep,
    stp.foxtrot.ChangeOfDirection,
]

TANGO_1 = [
    stp.quickstep.WalkLF,
    stp.quickstep.WalkRF,
    stp.waltz.FallawayReverseSlipPivot,
    stp.waltz.ReverseTurn,
    stp.foxtrot.ReverseTurn1_3,
    stp.tango.BackCorte,
    stp.tango.OutsideSwivel2,
    stp.tango.NaturalPromenadeTurn,
    stp.tango.NaturalRockTurn,
    stp.tango.BackCorte,
    stp.tango.OpenToPromenade,
    stp.tango.TheChase,
    stp.tango.BrushTap,
]

WALTZ_2 = [
    stp.waltz.ProgressiveChasseToR,
    stp.waltz.BackwardLockStep,
    stp.waltz.OpenImpetus,
    stp.waltz.Wing,
    stp.waltz.ClosedTelemark,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.HesitationChange,
    stp.waltz.ClosedChangeLF,
    stp.quickstep.ProgressiveChasseToL,
    stp.waltz.NaturalTurn1_3,
    stp.waltz.NaturalSpinTurn1_3,
    stp.waltz.TurningLockToL,
]

TANGO_2 = [
    stp.quickstep.WalkLF,
    stp.quickstep.WalkRF,
    stp.waltz.ReverseTurn,
    stp.quickstep.WalkLF,
    stp.quickstep.WalkRF,
    stp.tango.RockOnLF,
    stp.tango.BackCorte,
    stp.tango.FiveStep,
    stp.tango.ClosedPromenade,
]


def test_validate():
    waltz = syl.WaltzSyllabus
    quickstep = syl.QuickstepSyllabus
    foxtrot = syl.FoxtrotSyllabus
    tango = syl.TangoSyllabus

    r1 = Routine.from_list(WALTZ_1)
    r2 = Routine.from_list(QUICKSTEP_1)
    r3 = Routine.from_list(FOXTROT_1)
    r4 = Routine.from_list(TANGO_1)
    r5 = Routine.from_list(WALTZ_2)
    r6 = Routine.from_list(TANGO_2)

    r1.validate()
    # r2.validate()
    # r3.validate()
    r4.validate()
    r5.validate()
    r6.validate()

    waltz.validate(r1)
    quickstep.validate(r2)
    foxtrot.validate(r3)
    tango.validate(r4)
    with pytest.raises(RoutineError):
        waltz.validate(r5)
    tango.validate(r6)
