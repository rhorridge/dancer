import os
import json
import pytest
from dancer import parse
from dancer.competition import Competition

DATA_DIR = "data"


def fmt(jsn: str):
    return json.dumps(json.loads(jsn), indent=4)


@pytest.mark.slow
def test_file():
    comp = Competition(
        name="Nottingham Varsity 2021",
        venue="Nottingham Girls High School",
    )

    data_dir = os.path.join(
        os.getcwd(),
        DATA_DIR,
        "2021",
        "Nottingham Varsity Dancesport Competition",
    )

    res1 = parse.competitors(data_dir)
    res2 = parse.events(data_dir)
    res3 = parse.judges(data_dir)

    comp.competitors = res1
    comp.events = res2
    comp.adjudicators = res3
    for event in comp.events.keys():
        comp.events[event].rounds = parse.rounds(data_dir, event)
        comp.events[event].final = parse.final(data_dir, event)
