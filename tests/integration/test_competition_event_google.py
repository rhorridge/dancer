"""Test connecting to Google APIs.
"""

import datetime
import os
import pytest
from dancer.interfaces.google.competition_event import (
    CompetitionEventInterface,
    CompetitionDetails,
)


@pytest.fixture(scope="module")
def credentials():
    """Return the credentials for connecting to GCal."""

    try:
        return {
            "TOKEN_FILE": os.environ["TOKEN_FILE"],
            "CLIENT_SECRET_FILE": os.environ["CLIENT_SECRET_FILE"],
            "CALENDAR_ID": os.environ["CALENDAR_ID"],
        }
    except KeyError as err:
        raise KeyError("Set Environment variables before running this test!") from err


@pytest.mark.xfail
@pytest.mark.google
def test_connect(credentials):
    iface = CompetitionEventInterface(
        token_file=credentials.get("TOKEN_FILE"),
        client_secret_file=credentials.get("CLIENT_SECRET_FILE"),
        calendar_id=credentials.get("CALENDAR_ID"),
    )
    res = iface.query()
    print(res)

    id_ = res[0]

    event = iface.fetch(id_)
    assert event.id == id_

    iface.store(id_, event)


@pytest.mark.xfail
@pytest.mark.google
def test_create(credentials):
    iface = CompetitionEventInterface(
        token_file=credentials.get("TOKEN_FILE"),
        client_secret_file=credentials.get("CLIENT_SECRET_FILE"),
        calendar_id=credentials.get("CALENDAR_ID"),
    )
    event = CompetitionDetails(name="Test Event", date=datetime.datetime.now().date())
    iface.store(event.id, event)
