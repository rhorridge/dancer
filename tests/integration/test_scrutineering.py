"""Test some scrutineering.
"""

import out
import pytest
from dancer.scrutineer.place import place, transform_placings


@pytest.mark.slow
@pytest.mark.timeout(10)
def test_nudc_2022_preinter_ballroom():
    judges = ["A", "B", "C", "D"]
    placings = {
        30: [{"A": 4, "B": 5, "C": 5, "D": 6}, {"A": 4, "B": 5, "C": 4, "D": 6}],
        31: [{"A": 2, "B": 1, "C": 1, "D": 1}, {"A": 3, "B": 1, "C": 1, "D": 1}],
        50: [{"A": 6, "B": 4, "C": 6, "D": 5}, {"A": 6, "B": 4, "C": 6, "D": 4}],
        58: [{"A": 5, "B": 6, "C": 2, "D": 4}, {"A": 5, "B": 6, "C": 3, "D": 5}],
        69: [{"A": 1, "B": 3, "C": 3, "D": 3}, {"A": 2, "B": 3, "C": 5, "D": 3}],
        96: [{"A": 3, "B": 2, "C": 4, "D": 2}, {"A": 1, "B": 2, "C": 2, "D": 2}],
    }

    assert place(judges=judges, placings=placings).final_placings() == {
        31: 1,
        96: 2,
        69: 3,
        58: 4,
        30: 5,
        50: 6,
    }


@pytest.mark.slow
@pytest.mark.timeout(10)
def test_south_of_england_champs_2022_novice_ballroom():
    judges = ["B", "C", "D", "G", "H"]
    placings = {
        142: [[3, 4, 5, 4, 6], [4, 5, 4, 5, 1]],
        163: [[4, 1, 2, 1, 4], [2, 1, 2, 1, 4]],
        181: [[6, 2, 4, 2, 2], [5, 6, 3, 6, 2]],
        195: [[2, 6, 3, 6, 3], [3, 4, 5, 4, 3]],
        244: [[1, 5, 1, 5, 1], [6, 2, 1, 3, 6]],
        266: [[5, 3, 6, 3, 5], [1, 3, 6, 2, 5]],
    }

    placings = transform_placings(placings, judges)
    res = place(
        judges=judges,
        placings=placings,
    ).final_placings()
    assert res == {
        163: 1,
        244: 2,
        266: 3,
        195: 4,
        181: 5,
        142: 6,
    }
