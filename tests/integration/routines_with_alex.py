import dancer.steps as stp
import dancer.syllabus as syl
from dancer.routine import Routine

WALTZ = [
    stp.NaturalTurn1_3,
    stp.NaturalSpinTurn1_3,
    stp.TurningLockToR,
    stp.WeaveFromPP,
    stp.Ripple,
    stp.CurvedFeather,
    stp.OpenImpetus,
    stp.ChasseFromPP,
    stp.QuickOpenReverse,
    stp.ProgressiveChasseToR,
    stp.NaturalTurn1_3,
    stp.RunningSpinTurn,
    stp.VienneseLock,
    stp.DoubleReverseSpin,
    stp.FallawayReverseSlipPivot,
    stp.ProgressiveChasseToR,
    stp.BackwardLockStep,
    stp.OutsideSpin,
    stp.SpinPivot1_3,
    stp.OutsideChange,
    stp.FallawayNaturalTurn,
]

QUICKSTEP = [
    stp.NaturalTurn1_3,
    stp.NaturalSpinTurn,
    stp.ProgressiveChasseToL,
    stp.QuickOpenReverse,
    stp.RunningQuickWeave,
    stp.TravellingContraCheck,
    stp.RunningFinish,
    stp.PassingNaturalTurn,
    stp.FourQuickRun,
    stp.NaturalTurn1_3,
    stp.SpinPivot,
    stp.SpinPivot,
    stp.Zigzag,
    stp.Zigzag,
]

FOXTROT = [
    stp.FeatherStep,
    stp.FallawayLockStep,
    stp.BounceFallawayWithWeaveEnding,
    stp.ThreeStep,
    stp.CurvedFeather,
    stp.OutsideSwivel,
    stp.WeaveFromPP2,
    stp.HesitationChange,
]
