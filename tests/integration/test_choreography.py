"""Integration tests for dance choreography.
"""

import pytest
from dancer import steps
from dancer import choreography


@pytest.mark.slow
def test_random_choice():
    """Test that a random choice is good enough."""

    figs = choreography.BALLROOM_FIGURES
    choices = [steps.random_figure(figures=figs) for _ in range(1000)]
    opts = set(choices)
    assert len(opts) == len(choreography.BALLROOM_FIGURES)
