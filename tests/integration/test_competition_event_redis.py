"""Test storing competition events.
"""

import os
from dancer.web.dpaonline import Scraper
from dancer.interfaces.redis.competition_event import CompetitionEventInterface
from tests.fixtures.redis import redis_db


def test_storing_and_retrieving(redis_db):
    scraper = Scraper(uri=os.getcwd() + "/dpaonline.html")
    scraper.scrape()

    iface = CompetitionEventInterface(
        uri="redis://localhost",
        prefix="competition-event:dpa-ballroom-and-latin",
    )

    keys = iface.query()
    assert len(keys) == 0

    for elem in scraper.content:
        iface.store(elem.id, elem)

    keys = iface.query()
    assert len(keys) > 0

    for key in keys:
        print(iface.fetch(key).json())
