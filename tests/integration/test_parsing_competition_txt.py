"""Integration tests for parsing txt files.
"""

from dancer import parse


def test_file():
    res = parse.competitors("data/2021/Nottingham Varsity Dancesport Competition")
    assert len(res) > 200


def test_parse():
    res = parse.events("data/2021/Nottingham Varsity Dancesport Competition")
    assert len(list(res.values())[0].entries) > 6


def test_judgess():
    res = parse.judges("data/2021/Nottingham Varsity Dancesport Competition")
    assert len(res) >= 3
