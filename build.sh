#!/usr/bin/env bash
# Build the docker image.

set -e
set -u
set -o pipefail

IMAGE_VERSION="${VERSION:-latest}"
IMAGE_NAME="${NAME:-dancer}"

docker build \
       -t rhorridge/"${IMAGE_NAME}":"${IMAGE_VERSION}" \
       --file docker/Dockerfile \
       --build-arg APP_NAME="${IMAGE_NAME}" \
       .
