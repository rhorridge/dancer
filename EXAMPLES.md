This document shows some examples of output from the program.

To run these locally, ensure you have `jq` installed along with the
usual Linux command line tools (for these scripts, you will need
`column` and `tr`).

# Dancer - Competition Events

This script prints all currently supported competition events as a table.

```bash
poetry run ./scripts/print-competition-events.sh
```

## Output

```
Name                         Dances                                            Restricted?
----                         ------                                            -----------
Beginner W                   Waltz                                             true
Beginner Q                   Quickstep                                         true
Beginner C                   Cha-Cha-Cha                                       true
Beginner J                   Jive                                              true
Beginner WQ                  Waltz，Quickstep                                  true
Beginner CJ                  Cha-Cha-Cha，Jive                                 true
Novice W                     Waltz                                             true
Novice Q                     Quickstep                                         true
Novice C                     Cha-Cha-Cha                                       true
Novice J                     Jive                                              true
Novice T                     Tango                                             true
Novice R                     Rumba                                             true
Novice S                     Samba                                             true
Novice F                     Foxtrot                                           true
Novice WQ                    Waltz，Quickstep                                  true
Novice WVQ                   Waltz，Viennese Waltz，Quickstep                  true
Novice CJ                    Cha-Cha-Cha，Jive                                 true
Novice CSJ                   Cha-Cha-Cha，Samba，Jive                          true
Pre-Intermediate WQ          Waltz，Quickstep                                  false
Pre-Intermediate CJ          Cha-Cha-Cha，Jive                                 false
Intermediate WTQ             Waltz，Tango，Quickstep                           false
Intermediate WFQ             Waltz，Foxtrot，Quickstep                         false
Intermediate WTVQ            Waltz，Tango，Viennese Waltz，Quickstep           false
Intermediate CRJ             Cha-Cha-Cha，Rumba，Jive                          false
Intermediate CSJ             Cha-Cha-Cha，Samba，Jive                          false
Intermediate CSRJ            Cha-Cha-Cha，Samba，Rumba，Jive                   false
Advanced WTQ                 Waltz，Tango，Quickstep                           false
Advanced CRJ                 Cha-Cha-Cha，Rumba，Jive                          false
Advanced WFQ                 Waltz，Foxtrot，Quickstep                         false
Advanced CSJ                 Cha-Cha-Cha，Samba，Jive                          false
Advanced CSRJ                Cha-Cha-Cha，Samba，Rumba，Jive                   false
Advanced WTVQ                Waltz，Tango，Viennese Waltz，Quickstep           false
Advanced CRPJ                Cha-Cha-Cha，Rumba，Paso Doble，Jive              false
Open WQ                      Waltz，Quickstep                                  false
Open WF                      Waltz，Foxtrot                                    false
Open CJ                      Cha-Cha-Cha，Jive                                 false
Open CS                      Cha-Cha-Cha，Samba                                false
Open WTQ                     Waltz，Tango，Quickstep                           false
Open WVQ                     Waltz，Viennese Waltz，Quickstep                  false
Open CRJ                     Cha-Cha-Cha，Rumba，Jive                          false
Open CSJ                     Cha-Cha-Cha，Samba，Jive                          false
Open Foxtrot                 Foxtrot                                           false
Open Samba                   Samba                                             false
Open Viennese Waltz          Viennese Waltz                                    false
Open Paso Doble              Paso Doble                                        false
Open Rumba                   Rumba                                             false
Open Tango                   Tango                                             false
Open Five Dance - Ballroom   Waltz，Tango，Viennese Waltz，Foxtrot，Quickstep  false
Open Five Dance - Latin      Cha-Cha-Cha，Samba，Rumba，Paso Doble，Jive       false
Open Salsa                   Salsa                                             false
Non-Acrobatic Rock 'n' Roll  Non-acrobatic Rock 'n' Roll                       false
Acrobatic Rock 'n' Roll      Acrobatic Rock 'n' Roll                           false
Team Match                   Waltz，Cha-Cha-Cha，Quickstep，Jive               false
```


# Dancer - Competition

This script prints example usage of the competition module.

```bash
poetry run python -m dancer.competition
```

## Output

```python
# Import the module.
from dancer.competition import Competition, Adjudicator, Couple, Event, Round, Final

# Declare a competition
competition = Competition(name="Test Competition")

# Declare an adjudicator
adjudicator = Adjudicator(name="A. Judge")

# Assign the adjudicator to the competition.
competition.adjudicators["A"] = adjudicator

# Set the competition venue.
competition.venue = "Local Sports Hall"

# Set the competition date.
competition.date = "2022-05-25"

# Set up an event. We will import an existing category. It will be event number 1.
from dancer.competition_events import OpenF  # Open Foxtrot
event = Event(dance=OpenF)
competition.events[1] = event

# Enter a couple. They will have a number of 1.
couple = Couple(leader="A. Leader", follower="A. Follower")
competition.competitors[1] = couple

# Enter couple 1 into event 1.
competition.events[1].entries.append(1)

# Run a first round.
round = Round(couples={1: {"marks": [{"marked_by": "A"}], "recalled": True}})
competition.events[1].rounds[1] = round

# Run a final.
final = Final(couples={1: {"placings": [{"placed_by": {"A": 1}}], "final_place": 1}})
competition.events[1].final = final

# Display the adjudicators.
competition.print_adjudicators()

# Display the competitors.
competition.print_competitors()

# Display the events.
competition.print_events()

# Display event 1.
competition.print_event(1)

```

This output can itself be executed as a script, either directly or by
passing it to stdin of a new Python process:

```bash
poetry run python -m dancer.competition | poetry run python
```

### Output

```org
* Adjudicators

  A. A. Judge

* Competitors

    1. A. Leader & A. Follower

* Events

  1.  Open Foxtrot

* Event 1 :: Open Foxtrot :: 1 entries
  Event has 1 dance - Foxtrot

* Entries

    1. A. Leader & A. Follower

* Rounds

** Round 1

    #   A  R
    1.  ✘  ✓


* Final

    #   A  P
    1.  1  1

```



# Dancer - Couple

This script prints example usage of the couple module.

```bash
poetry run python -m dancer.couple
```

## Output

```python
# Import the module.
from dancer.couple import Couple

# Declare a couple.
couple = Couple(leader="Arthur Leader", follower="Annette Follower")

# Display this couple as JSON.
print(couple.json(exclude_none=True, indent=4))

# Assign this couple to a university.
couple.university = "Birmingham"

# Display this couple as JSON again.
print(couple.json(exclude_none=True, indent=4))

```


This output can itself be executed as a script, either directly or by
passing it to stdin of a new Python process:

```bash
poetry run python -m dancer.couple | poetry run python
```

### Output

```js
{
    "leader": "Arthur Leader",
    "follower": "Annette Follower"
}
{
    "leader": "Arthur Leader",
    "follower": "Annette Follower",
    "university": "Birmingham"
}
```


# Dancer - Dance

This script prints JSON of the dance passed as its first argument.

```bash
poetry run python -m dancer.dance waltz
poetry run python -m dancer.dance quickstep
poetry run python -m dancer.dance cha-cha-cha
poetry run python -m dancer.dance jive
```

## Output

```js
{
    "name": "Waltz",
    "abbr": "W"
}
{
    "name": "Quickstep",
    "abbr": "Q"
}
{
    "name": "Cha-Cha-Cha",
    "abbr": "C"
}
{
    "name": "Jive",
    "abbr": "J"
}
```


# Dancer - Main

TODO - Competition Downloading


# Dancer - Parse

TODO - Competition Parsing

# Dancer - Routine

TODO - Dance Routines

# Dancer - Steps

This script finds a random figure from the predefined choreography,
then generates a random routine starting from that step, which is then
printed.

It then prints all possible predefined figures that can precede this
random figure, then all possible figures that can follow the random
figure.

It should be noted that this script does not differentiate between
choreography for different dances and will pick possible figures from
the entire pool. It will switch between ballroom and Latin figures,
which leads to some interesting routines!

```bash
poetry run python -m dancer.steps
```

## Output

```org
Random step: Closed Hip Twist - Start in R Shadow

* Routine starting with Closed Hip Twist - Start in R Shadow
 1. Closed Hip Twist - Start in R Shadow
 2. Hockey Stick
 3. Double Reverse Spin
 4. Fan
 5. Fencing
 6. Outside Swivel (Foxtrot)
 7. Samba Walks - Side
 8. Samba Walks - Promenade - LF
 9. Quick Natural Weave from PP
10. Progressive Side Step

* All possible preceding Closed Hip Twist - Start in R Shadow
- Basic Movement - R Shadow - LF
- Cha Cha Cha Chasse to Right - R Shadow
- Cruzados Walks and Locks - RF
- Foot Change 1 - Open Position to Right Shadow Position
- Foot Change 3 - Open Position to Right Shadow Position
- Samba - Foot Change - 1 - RF
- Samba - Foot Change - 3 - RF - Open Promenade Position
- Samba - Foot Change - 3 - RF - Promenade Position
- Shadow Circular Volta - RF
- Shadow Travelling Voltas - RF
- Sliding Doors

* All possible proceeding from Closed Hip Twist - Start in R Shadow
- Alemana
- Fencing
- Hockey Stick
- Sliding Doors
- Three Alemanas
```

# Dancer - Syllabus

This script generates a random routine for all currently supported
syllabi, and prints them out. It will only pick steps that form part
of that particular syllabus.

```python
poetry run python -m dancer.syllabus
```

## Output

```org
* Waltz routine starting with 1-3 of Natural Turn
 1. 1-3 of Natural Turn
 2. Running Spin Turn
 3. Closed Impetus
 4. Reverse Pivot
 5. Reverse Turn
 6. Whisk
 7. Chasse from PP
 8. Closed Wing
 9. Closed Change LF
10. Closed Change RF

* Quickstep routine starting with Quick Open Reverse
 1. Quick Open Reverse
 2. V-Six
 3. Walk RF
 4. Closed Telemark
 5. Running Right Turn
 6. Tipsy to L
 7. V-Six
 8. 1-4 of Tipple Chasse to L
 9. Running Right Turn
10. Tipple Chasse to L

* Foxtrot routine starting with Hover Feather
 1. Hover Feather
 2. Hover Telemark
 3. Natural Twist Turn
 4. Change of Direction
 5. Extended Reverse Wave
 6. Natural Turn
 7. Natural Twist Turn
 8. Reverse Wave
 9. Natural Hover Telemark
10. Change of Direction

* Tango routine starting with The Chase
 1. The Chase
 2. 1-3 of Reverse Turn
 3. Rock on RF
 4. Progressive Link
 5. Open Promenade
 6. Back Open Promenade
 7. Open Promenade
 8. Outside Swivel (Tango)
 9. Open Promenade
10. Fallaway Four Step

* Viennese Waltz routine starting with 4-6 of Reverse Turn
 1. 4-6 of Reverse Turn
 2. Closed Change LF
 3. Closed Change RF
 4. Closed Change LF
 5. 1-3 of Natural Turn
 6. 4-6 of Natural Turn
 7. 1-3 of Natural Turn
 8. Backward Change LF
 9. 4-6 of Reverse Turn
10. Closed Change LF

* Cha-Cha-Cha routine starting with Aida
 1. Aida
 2. Split Cuban Breaks - RF
 3. Side Step to Right
 4. Cha Cha Cha Chasse to Left - Closed
 5. Basic Movement - Closed - RF
 6. Spot Turn to Left
 7. Curl
 8. Alemana
 9. Switch Turn to Right
10. Side Step to Right

* Rumba routine starting with Three Alemanas
 1. Three Alemanas
 2. Three Threes
 3. Fan
 4. Hockey Stick
 5. Switch Turn to Right
 6. Spot Turn to Left
 7. Three Threes
 8. Three Threes
 9. Cuban Rocks - L
10. Natural Top

* Samba routine starting with Corta Jaca
 1. Corta Jaca
 2. Rhythm Bounce - RF
 3. Promenade & Counter Promenade Runs
 4. Promenade Botafogo
 5. Promenade Botafogo
 6. Criss Cross Voltas
 7. Promenade Botafogo
 8. Promenade Botafogo
 9. Criss Cross Voltas
10. Samba - Foot Change - 3 - RF - Open Promenade Position

* Paso Doble routine starting with Twists
 1. Twists
 2. Coup de Pique changing from LF to RF
 3. Promenade & Counter Promenade
 4. Grand Circle
 5. Travelling Spins from Promenade Position
 6. Grand Circle
 7. Twist Turn
 8. Twists
 9. Coup de Pique changing from LF to RF
10. Promenade
```

# Dancer - Choreography - Direction

TODO - Possible directions for a foot

# Dancer - Choreography - Position

TODO - Possible positions to take with respect to partner

# Dancer - Choreography - Feet

TODO - Possible feet directions

# Dancer - Choreography

This script prints all currently predefined figures as JSON. It does not
guarantee the order, as they are defined in the module as sets to
speed up checks.

```bash
poetry run python -m dancer.choreography
```

## Output

```js
[
    {
        "name": "New York - Left",
        "start_foot": {
            "direction": "Fwd",
            "foot": "R"
        },
        "start_position": "Closed",
        "next_foot": {
            "direction": "Any",
            "foot": "L"
        },
        "next_position": "Closed"
    },
    {
        "name": "Three Threes",
        "start_foot": {
            "direction": "Fwd",
            "foot": "L"
        },
        "start_position": "Closed",
        "next_foot": {
            "direction": "Any",
            "foot": "L"
        },
        "next_position": "Closed"
    },
    # ... truncated for brevity
    {
        "name": "Chasse from PP",
        "start_foot": {
            "direction": "Fwd",
            "foot": "R"
        },
        "start_position": "PP",
        "next_foot": {
            "direction": "Fwd",
            "foot": "R"
        },
        "next_position": "Closed"
    },
    {
        "name": "Change of Direction",
        "start_foot": {
            "direction": "Fwd",
            "foot": "L"
        },
        "start_position": "Closed",
        "next_foot": {
            "direction": "Fwd",
            "foot": "L"
        },
        "next_position": "Closed"
    }
]
```

# Dancer - Choreography - Syllabus Figures - Ballroom

TODO This module contains the predefined syllabus figures for each
Ballroom dance. This is still a work in progress as it requires
research about which steps originate in which dance e.g. Natural Turn
in Waltz, Progressive Chasse in Quickstep.

# Dancer - Choreography - Syllabus Figures - Latin

TODO This module contains the predefined syllabus figures for each
Latin American dance. WIP - needs to make it clear which steps are
from which dance e.g. New York in Cha-Cha-Cha, Aida in Rumba.

# Dancer - Technique - Terms

TODO This module defines terms from typical dance choreography books,
for potentially automating import from these sources. Many of these
terms are defined in other places e.g. `dancer.steps`.

# Dancer - Database

TODO This module contains SQLAlchemy database models for storing data
in a relational database management system (RDBMS). Currently only
competition results are supported, but in future it may manage:
- scrutineering
- routines
- choreography

# Dancer - Web - Easycomp

TODO This module contains web scraping functions to download results
produced by an well known existing scrutineering package. The
environment variable `EASYCOMP_URL` must be set to the desired URL -
this is not included in here to avoid exploit by malicious actors.

Running this script will print the most recent competition results.

```bash
poetry run python -m dancer.web.easycomp
```

# Dancer - Scrutineer - Recall

This module handles the recall of couples between rounds. Its main
export is a function, `recall`, which produces a
`dancer.competition.Round` object based on marks awarded by judges.

Running the script alone prints example usage.

```bash
poetry run python -m dancer.scrutineer.recall
```

## Output

```python
# Import the module.
from dancer.scrutineer.recall import recall

# Declare the judges.
judges = ["A", "B", "C"]

# Declare the marks.
marks = {1: ["A"], 2: [], 3: ["A", "B"]}

# Calculate the recall based on different thresholds.
recall1 = recall(judges=judges, marks=marks)  # default
recall2 = recall(judges=judges, marks=marks, recalling=2)
recall3 = recall(judges=judges, marks=marks, recalling=1)

# Print the recalls as JSON.
print(recall1.json(exclude_none=True, indent=4))
print(recall2.json(exclude_none=True, indent=4))
print(recall3.json(exclude_none=True, indent=4))
```

This output can itself be executed as a script, either directly or by
passing it to stdin of a new Python process:

```bash
poetry run python -m dancer.scrutineer.recall | poetry run python
```

### Output

```js
{
    "couples": {
        "1": {
            "marks": [
                {
                    "marked_by": "A"
                }
            ],
            "recalled": true
        },
        "2": {
            "marks": [],
            "recalled": true
        },
        "3": {
            "marks": [
                {
                    "marked_by": "A"
                },
                {
                    "marked_by": "B"
                }
            ],
            "recalled": true
        }
    }
}
{
    "couples": {
        "1": {
            "marks": [
                {
                    "marked_by": "A"
                }
            ],
            "recalled": true
        },
        "2": {
            "marks": [],
            "recalled": false
        },
        "3": {
            "marks": [
                {
                    "marked_by": "A"
                },
                {
                    "marked_by": "B"
                }
            ],
            "recalled": true
        }
    }
}
{
    "couples": {
        "1": {
            "marks": [
                {
                    "marked_by": "A"
                }
            ],
            "recalled": false
        },
        "2": {
            "marks": [],
            "recalled": false
        },
        "3": {
            "marks": [
                {
                    "marked_by": "A"
                },
                {
                    "marked_by": "B"
                }
            ],
            "recalled": true
        }
    }
}
```

# Dancer - Scrutineer - Place

TODO This module handles the placing of couples in a final. It is in
need of much integration testing with previous known results to ensure
its integrity and to validate its output.

# Scripts

There are some example scripts in here, notably
`scripts/print-syllabus.sh`, which prints a syllabus in tabular
format.

```bash
poetry run ./scripts/print-syllabus.sh waltz
```

## Output

```
Name                           Start Foot  Start Position  Next Foot  Next Position
----                           ----------  --------------  ---------  -------------
Closed Change RF               R Fwd       Closed          L Fwd      Closed
Closed Change LF               L Fwd       Closed          R Fwd      Closed
Natural Turn                   R Fwd       Closed          R Fwd      Closed
1-3 of Natural Turn            R Fwd       Closed          L Bwd      Closed
Reverse Turn                   L Fwd       Closed          L Fwd      Closed
Natural Spin Turn              L Bwd       Closed          L Fwd      Closed
1-3 of Natural Spin Turn       L Bwd       Closed          R Bwd      Closed
Whisk                          L Fwd       Closed          R Fwd      PP
Chasse from PP                 R Fwd       PP              R Fwd      Closed
Hesitation Change              L Bwd       Closed          L Fwd      Closed
Progressive Chasse to R        L Fwd       Closed          L Bwd      Closed
Closed Impetus                 L Bwd       Closed          R Bwd      Closed
Outside Change                 L Bwd       Closed          R Fwd      Closed
Reverse Corte                  R Bwd       Closed          L Fwd/Bwd  Closed
Back Whisk                     L Bwd       Closed          R Fwd      PP
Basic Weave (Waltz)            R Bwd       Closed          R Fwd      Closed
Double Reverse Spin            L Fwd       Closed          L Fwd      Closed
Drag Hesitation                L Fwd       Closed          L Fwd/Bwd  Closed
Backward Lock Step             L Bwd       Closed          L Bwd      Closed
Forward Lock Step              R Fwd       Closed          R Fwd      Closed
Reverse Pivot                  R Bwd       Closed          L Fwd      Closed
Closed Telemark                L Fwd       Closed          R Fwd      Closed
Open Telemark                  L Fwd       Closed          R Fwd      PP
Cross Hesitation               R Fwd       PP              R Fwd      Closed
Wing                           R Fwd       PP              L Fwd      Closed
Open Impetus                   L Bwd       Closed          R Fwd      PP
Weave from PP (Waltz)          R Fwd       PP              R Fwd      Closed
Outside Spin                   L Bwd       Closed          R Fwd      Closed
Turning Lock to L              R Bwd       Closed          R Fwd      Closed
Turning Lock to R              R Bwd       Closed          R Fwd      PP
Closed Wing                    R Fwd       Closed          L Fwd      Closed
Fallaway Reverse & Slip Pivot  L Fwd       Closed          L Fwd      Closed
Hover Corte                    R Bwd       Closed          L Bwd      Closed
Fallaway Natural Turn          R Fwd       PP              R Fwd      Closed
Running Spin Turn              L Bwd       Closed          L Bwd      Closed
Fallaway Whisk                 L Bwd       Closed          R Fwd      PP

```
