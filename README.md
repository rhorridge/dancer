All-in-one dance choreography, routine and competition management,
as a Python package and HTTP API.

# Installing

We use [Poetry](https://python-poetry.org/) for testing and
development of this package.

With Poetry installed, clone this repository to a directory of your
choice and run the following from within the directory:

```bash
poetry update
```


# Testing

To run the unit tests:

```bash
poetry run pytest dancer/
```

To run the integration tests (be aware that these require certain
files to be downloaded from competition websites):

```bash
poetry run pytest tests/integration/
```

To run all tests, deselecting slow ones:

```bash
poetry run pytest --durations=10 -m 'not slow'
```

# Running

Organisation of the command line application is still ongoing, so this
documentation may need updating!

## dancer.competition_events

This script will print out all of the currently supported competition
events, as JSON.

```bash
poetry run python -m dancer.competition_events
```



# Issues

Please view the issues tracker to view existing issues and suggest new
ones.

# Contributing

Please do! There is a lot of work to be done to improve this project
and make it useful. Good starting points:

- Validating the syllabi
- Adding different syllabi (e.g. WDSF)
- Improving the logic for generating routines
- More test cases for choreography
- More test cases for scrutineering

# License

This project is licensed under the terms of the GNU Affero General
Public License, version 3.
