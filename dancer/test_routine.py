"""Unit tests for routine.
"""

from . import routine


class TestRoutine:
    def test_from_list(self):
        r = routine.Routine.from_list([])
        assert r.steps == []

    def test_validate(self):
        r = routine.Routine(steps=[])
        r.validate()
