"""Represents possible age restrictions.
"""

import re
from enum import Enum
from pydantic import BaseModel


class AboveOrBelow(str, Enum):
    """Represents whether something is above or below."""

    OVER = 'O'
    UNDER = 'U'

    def __str__(self) -> str:
        """Return a human-readable representation."""

        return self.value

    @classmethod
    def from_str(cls, string: str) -> object:
        """Convert a string into this object."""

        match string:
            case 'O' | 'Over':
                return cls.OVER
            case 'U' | 'Under':
                return cls.UNDER
        raise ValueError('could not parse %s to an instance of AboveOrBelow' % string)


class AgeRestriction(BaseModel):
    """Represents a possible age restriction."""

    age: int
    under_or_over: AboveOrBelow

    def __str__(self) -> str:
        """Return a human-readable representation of an object."""

        return f'{str(self.under_or_over)}{self.age}'

    @classmethod
    def from_str(cls, string: str) -> object:
        """Convert a string into this object."""

        strns = string.split()
        strn = strns[0]
        match strn:
            case 'Juvenile' | 'Juv':
                return JUVENILE
            case 'Junior' | 'Jun':
                return JUNIOR
            case 'Youth':
                return YOUTH
            case 'Amateur':
                return None

        if len(strns) == 1:
            if strn.startswith('U'):
                return cls.from_str(' '.join((string[0], string[1:])))
            if strn.startswith('O'):
                return cls.from_str(' '.join((string[0], string[1:])))

        if len(strns) >= 2:
            try:
                a_or_b = AboveOrBelow.from_str(strns[0])
            except ValueError:
                match strns[0]:
                    case 'Senior':
                        match int(strns[1]):
                            case 1:
                                return AgeRestriction(age=55, under_or_over='O')
                            case 2:
                                return AgeRestriction(age=60, under_or_over='O')
                            case 3:
                                return AgeRestriction(age=65, under_or_over='O')
                            case 4:
                                return AgeRestriction(age=70, under_or_over='O')
                            case _:
                                raise ValueError('unsupported Senior category: %d' % int(strns[0]))
                    case _:
                        raise NotImplementedError('not supported word 1 & 2: %s & %s' % (strns[0], strns[1]))
            else:
                age = int(re.sub("[^0-9]", "", strns[-1]))
                return cls(
                    under_or_over=a_or_b,
                    age=age,
                )

        raise ValueError('could not parse %s to an instance of AgeRestriction' % string)


U35 = AgeRestriction(age=35, under_or_over='U')
O35 = AgeRestriction(age=35, under_or_over='O')
O50 = AgeRestriction(age=50, under_or_over='O')
JUVENILE = AgeRestriction(age=10, under_or_over='U')
JUNIOR = AgeRestriction(age=14, under_or_over='U')
YOUTH = AgeRestriction(age=18, under_or_over='U')
U21 = AgeRestriction(age=21, under_or_over='U')
SENIOR1 = AgeRestriction(age=55, under_or_over='O')
SENIOR2 = AgeRestriction(age=60, under_or_over='O')
SENIOR3 = AgeRestriction(age=65, under_or_over='O')
SENIOR4 = AgeRestriction(age=70, under_or_over='O')
