"""Abstractions to handle routines.
"""

from pydantic import BaseModel, validate_arguments
from dancer.steps import RoutineError, Figure


class Routine(BaseModel):
    """
    Represents a routine. Currently, mainly used to validate that
    a routine is possible.
    """

    steps: list[Figure]

    @classmethod
    @validate_arguments
    def from_list(cls, routine: list[Figure]) -> object:
        """Create this object from a list of steps."""

        obj = cls(steps=routine)
        return obj

    def validate(self):
        """
        Validate that a routine is possible. Does not check that it
        conforms to syllabus!

        Does not return a value. Raises a RoutineError if the routine is
        invalid.
        """

        prevstep = None

        for step in self.steps:
            if not prevstep:
                prevstep = step
                continue
            try:
                assert prevstep.valid_next(step)
            except AssertionError as e:
                raise RoutineError(
                    "%s not a valid step from %s"
                    % (
                        step.name,
                        prevstep.name,
                    )
                ) from e
            prevstep = step
