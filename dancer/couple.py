"""Implementations of dance couples.
"""

from typing import Optional
from pydantic import BaseModel
from dancer.university import University


class Couple(BaseModel):
    leader: str
    follower: str
    university: Optional[University]

    class Config:
        validate_assignment = True

    def __str__(self) -> str:
        return self.fmt()

    def fmt(self) -> str:
        """Return a string representing this couple."""

        if self.follower == "":
            strn = self.leader
        else:
            strn = "%s & %s" % (self.leader.strip(), self.follower)
        if self.university:
            strn += " - %s" % self.university
        return strn

    def to_sqlalchemy(self):
        return {"university": self.university}


if __name__ == "__main__":
    print("# Import the module.")
    print("from dancer.couple import Couple")
    print()
    print("# Declare a couple.")
    print('couple = Couple(leader="Arthur Leader", follower="Annette Follower")')
    print()
    print("# Display this couple as JSON.")
    print("print(couple.json(exclude_none=True, indent=4))")
    print()
    print("# Assign this couple to a university.")
    print('couple.university = "Birmingham"')
    print()
    print("# Display this couple as JSON again.")
    print("print(couple.json(exclude_none=True, indent=4))")
