#!/usr/bin/env python3
"""Main program. Handle possible options.
"""

import sys
from . import main

if __name__ == "__main__":
    import out

    out.configure(level="info", default_level="info", theme="production")
    main(*sys.argv[1:])
