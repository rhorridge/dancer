from pydantic import BaseModel


class University(BaseModel):
    """Represents an individual University."""

    system_name: str
    display_name: str

    def __str__(self) -> str:
        """Print a human-readble representation."""

        return self.display_name

    @classmethod
    def from_str(cls, string: str) -> object:
        """Return an instance of this class from a string."""

        return cls(
            system_name=string.lower(),
            display_name=string,
        )
