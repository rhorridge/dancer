from sqlalchemy import (
    Boolean,
    String,
    Integer,
    Date,
    ForeignKey,
    Column,
    JSON,
    UniqueConstraint,
)
from sqlalchemy.orm import relationship
from sqlalchemy.orm import declarative_base

Base = declarative_base()
metadata = Base.metadata


class Competition(Base):
    __tablename__ = "competition"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32), nullable=False, index=True)
    venue = Column(String(32), nullable=True)
    date = Column(Date, nullable=True)
    competition_adjudicator = relationship("CompetitionAdjudicator")
    event = relationship("Event")
    entry = relationship(
        "CompetitionEntry",
        back_populates="competition",
    )


class CompetitionAdjudicator(Base):
    __tablename__ = "competition_adjudicator"

    id = Column(Integer, primary_key=True, autoincrement=True)
    letter = Column(String(1), nullable=False)
    competition_id = Column(Integer, ForeignKey("competition.id"))
    adjudicator_id = Column(Integer, ForeignKey("adjudicator.id"))
    adjudicator = relationship(
        "Adjudicator",
        back_populates="competition_adjudicator",
    )

    __tableargs__ = (UniqueConstraint(competition_id, adjudicator_id), {})


class Adjudicator(Base):
    __tablename__ = "adjudicator"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32), nullable=False)

    competition_adjudicator = relationship(
        "CompetitionAdjudicator",
        back_populates="adjudicator",
    )


class Event(Base):
    __tablename__ = "event"

    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, nullable=False, index=True)
    competition_id = Column(Integer, ForeignKey("competition.id"))
    category = relationship(
        "Category",
        secondary="event_category",
        back_populates="event",
    )
    entries = relationship("EventEntry")
    round_ = relationship("Round")
    final = relationship("Final")

    __tableargs__ = (UniqueConstraint(event_id, competition_id), {})


class Category(Base):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32), unique=True, nullable=False)
    restricted = Column(Boolean, nullable=False)
    dances = Column(JSON)
    event = relationship(
        "Event",
        secondary="event_category",
        back_populates="category",
    )


class EventCategory(Base):
    __tablename__ = "event_category"

    event_id = Column(Integer, ForeignKey("event.id"), primary_key=True)
    category_id = Column(Integer, ForeignKey("category.id"), primary_key=True)


class EventEntry(Base):
    __tablename__ = "event_entry"

    id = Column(Integer, primary_key=True, autoincrement=True)
    competition_entry_id = Column(
        Integer,
        ForeignKey("competition_entry.id"),
    )
    event_id = Column(
        Integer,
        ForeignKey("event.id"),
    )

    __tableargs__ = (UniqueConstraint(competition_entry_id, event_id), {})


class CompetitionEntry(Base):
    __tablename__ = "competition_entry"

    id = Column(Integer, primary_key=True, autoincrement=True)
    number = Column(Integer, unique=False, nullable=False)
    couple_id = Column(Integer, ForeignKey("couple.id"))
    competition_id = Column(Integer, ForeignKey("competition.id"))

    competition = relationship("Competition", back_populates="entry")
    couple = relationship("Couple", back_populates="competition")

    __tableargs__ = (UniqueConstraint(competition_id, number), {})


class Couple(Base):
    __tablename__ = "couple"

    id = Column(Integer, primary_key=True, autoincrement=True)
    leader = relationship("Dancer", secondary="couple_leader")
    follower = relationship("Dancer", secondary="couple_follower")
    university = Column(String, unique=False, nullable=True, index=True)
    competition = relationship(
        "CompetitionEntry",
        back_populates="couple",
    )


class Dancer(Base):
    __tablename__ = "dancer"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(32), unique=True, nullable=False, index=True)


class CoupleLeader(Base):
    __tablename__ = "couple_leader"

    couple_id = Column(Integer, ForeignKey("couple.id"), primary_key=True)
    dancer_id = Column(Integer, ForeignKey("dancer.id"), primary_key=True)


class CoupleFollower(Base):
    __tablename__ = "couple_follower"

    couple_id = Column(Integer, ForeignKey("couple.id"), primary_key=True)
    dancer_id = Column(Integer, ForeignKey("dancer.id"), primary_key=True)


class Round(Base):
    __tablename__ = "round"

    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, ForeignKey("event.id"), index=True)
    number = Column(Integer, nullable=False)
    couples = relationship("CoupleRound")


class CoupleRound(Base):
    __tablename__ = "couple_round"

    id = Column(Integer, primary_key=True, autoincrement=True)
    event_entry_id = Column(Integer, ForeignKey("event_entry.id"))
    round_id = Column(Integer, ForeignKey("round.id"))
    marks = Column(JSON)
    recalled = Column(Boolean)

    __tableargs__ = (UniqueConstraint(event_entry_id, round_id), {})


class Final(Base):
    __tablename__ = "final"

    id = Column(Integer, primary_key=True, autoincrement=True)
    event_id = Column(Integer, ForeignKey("event.id"), index=True)
    couples = relationship("CoupleFinal")


class CoupleFinal(Base):
    __tablename__ = "couple_final"

    id = Column(Integer, primary_key=True, autoincrement=True)
    event_entry_id = Column(Integer, ForeignKey("event_entry.id"))
    final_id = Column(Integer, ForeignKey("final.id"))
    placings = Column(JSON)
    final_place = Column(Integer, nullable=True)

    __tableargs__ = (UniqueConstraint(event_entry_id, final_id), {})
