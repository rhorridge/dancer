-- All couples with their names
select a.id, a.university, d.name as leader, e.name as follower from couple a join couple_leader b on a.id = b.couple_id join couple_follower c on a.id = c.couple_id join dancer d on b.dancer_id = d.id join dancer e on c.dancer_id = e.id;

-- All couples with their numbers
select b.number, a.leader, a.follower from all_couples a join competition_entry b on b.couple_id = a.id order by b.number;
