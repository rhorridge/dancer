import logging
from dancer.database import models
from sqlalchemy import create_engine, orm, exc

LOG = logging.getLogger(__name__)


Session = None


def sessionmaker(uri="sqlite://"):
    engine = create_engine(uri)
    models.metadata.create_all(engine)
    sess = orm.sessionmaker(bind=engine)
    return sess


class SessionScope:
    """A context manager to run SQL queries in sessions from SESSIONMAKER."""

    def __init__(self):
        global Session
        if not Session:
            Session = sessionmaker()

        self.session = orm.scoped_session(Session)

    def __enter__(self):
        return self.session

    def __exit__(self, typ, value, traceback):
        if not typ:
            self.session.commit()
            return True
        if typ is exc.OperationalError or typ is exc.DatabaseError:
            self.session.rollback()
            LOG.error(value)
        raise value
