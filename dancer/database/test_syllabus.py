from .syllabus import get_syllabus, valid_options


def test_get_syllabus():
    assert get_syllabus("waltz").name == "Waltz"
    assert get_syllabus("quickstep").name == "Quickstep"
    assert get_syllabus("foxtrot").name == "Foxtrot"
    assert get_syllabus("viennese-waltz").name == "Viennese Waltz"
    assert get_syllabus("tango").name == "Tango"


def test_valid_options():
    assert len(valid_options()) == 10
