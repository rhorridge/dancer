"""Implement data store for dance syllabi.
"""

from typing import Optional
from enum import Enum
from pydantic import validate_arguments
from dancer import dance
from dancer.syllabus import (
    Syllabus,
    WaltzSyllabus,
    TangoSyllabus,
    VienneseWaltzSyllabus,
    FoxtrotSyllabus,
    QuickstepSyllabus,
    ChaChaChaSyllabus,
    SambaSyllabus,
    RumbaSyllabus,
    PasoDobleSyllabus,
    JiveSyllabus,
)


_DATABASE = {
    dance.Waltz: WaltzSyllabus,
    dance.Tango: TangoSyllabus,
    dance.VienneseWaltz: VienneseWaltzSyllabus,
    dance.Foxtrot: FoxtrotSyllabus,
    dance.Quickstep: QuickstepSyllabus,
    dance.ChaChaCha: ChaChaChaSyllabus,
    dance.Samba: SambaSyllabus,
    dance.Rumba: RumbaSyllabus,
    dance.PasoDoble: PasoDobleSyllabus,
    dance.Jive: JiveSyllabus,
}


def get_syllabus(option: str) -> Optional[Syllabus]:
    """Return a dance syllabus."""

    option = dance.get_dance(option)

    return _DATABASE.get(option)


def valid_options() -> list[str]:
    return [key.name for key in _DATABASE.keys()]
