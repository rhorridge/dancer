"""Unit tests for dancer.age_restriction.
"""

import pytest
from dancer import age_restriction


def test_above_or_below():
    """Test for converting the Above or Below category."""

    cls = age_restriction.AboveOrBelow
    assert cls.from_str('O') == cls.OVER
    assert cls.from_str('Over') == cls.OVER
    assert cls.from_str('U') == cls.UNDER
    assert cls.from_str('Under') == cls.UNDER

    with pytest.raises(ValueError):
        cls.from_str('foobar')


def test_age_restriction():
    """Test for converting an Age Restriction."""

    cls = age_restriction.AgeRestriction
    assert cls.from_str('U35') == age_restriction.U35
    assert cls.from_str('Under 35') == age_restriction.U35
    assert cls.from_str('Under 35s') == age_restriction.U35
    assert cls.from_str('O35') == age_restriction.O35
    assert cls.from_str('Over 35') == age_restriction.O35
    assert cls.from_str('Over 35s') == age_restriction.O35
    assert cls.from_str('Juvenile') == age_restriction.JUVENILE
    assert cls.from_str('Junior') == age_restriction.JUNIOR
    assert cls.from_str('Youth') == age_restriction.YOUTH
    assert cls.from_str('U21') == age_restriction.U21
    assert cls.from_str('Under 21') == age_restriction.U21
    assert cls.from_str('Under 21s') == age_restriction.U21
    assert cls.from_str('O50') == age_restriction.O50
    assert cls.from_str('Over 50') == age_restriction.O50
    assert cls.from_str('Over 50s') == age_restriction.O50
    assert cls.from_str('Senior 1') == age_restriction.SENIOR1
    assert cls.from_str('Senior 2') == age_restriction.SENIOR2
    assert cls.from_str('Senior 3') == age_restriction.SENIOR3
    assert cls.from_str('Senior 4') == age_restriction.SENIOR4

    with pytest.raises(ValueError):
        cls.from_str('Senior 0')
    with pytest.raises(ValueError):
        cls.from_str('Senior 5')
    with pytest.raises(NotImplementedError):
        cls.from_str('Foobar Baz')
    with pytest.raises(ValueError):
        cls.from_str('Foobarbaz')
