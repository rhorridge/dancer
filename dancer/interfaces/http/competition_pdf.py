"""Interface for reading (probably not writing) Competition PDF files
from the WWW.
"""

import datetime
from contextlib import contextmanager
from typing import Optional
from pathlib import Path
from pydantic import HttpUrl
from bs4 import BeautifulSoup
import requests
from dancer import Competition
from dancer.interfaces.base.competition_pdf import CompetitionPDFInterface as Base
from dancer.interfaces.base.competition_pdf import (
    CompetitionEventPDFInterface as BaseEvent,
)


class CompetitionEventPDFInterface(BaseEvent):
    """HTTP interface for reading/writing PDF files for a specific event.

    Attributes:
        url: base URL to use to retrieve PDFs.
    """

    url: HttpUrl
    names: list[str]
    session: object
    headers: dict

    def get(self, *, name: str) -> bytes:
        """Fetch a competition event PDF.

        Args:
            name: the name of the individual file (full name minus .pdf).

        Returns:
            byte data representing a Portable Document Format (PDF) document.
        """

        url = self.url.build(
            scheme=self.url.scheme,
            host=self.url.host,
            path=f"{self.url.path}/{name}.pdf",
        )

        result = self.session.get(
            url,
            headers=self.headers,
        )

        if not result.ok:
            raise FileNotFoundError(f"Failed to GET {url}: {result.status_code:d}")

        return result.content

    def store(self, pdf: bytes, /, *, name: str) -> None:
        """Store a competition event PDF. UNIMPLEMENTED INTENTIONALLY."""

        raise NotImplementedError(
            "HTTP is not the intended method for storing a competition PDF."
        )

    def query(self) -> list[str]:
        """List available competition event PDFs for a given competition.

        Returns:
            a list of 0 or more event PDF names for the given competition.
        """

        return self.names


class CompetitionPDFInterface(Base):
    """HTTP interface for querying the available competitions.

    Attributes:
        url: base URL that makes the PDF files available.
    """

    url: HttpUrl
    session: Optional[object] = None
    headers: dict = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"
    }

    @contextmanager
    def get(self, *, name: str, year: int) -> bytes:
        """Create a context manager for returning an interface for a specific
        competition.

        Args:
            name: the event name (unique within a given year).
            year: the event year.

        Yields:
            an instance of CompetitionEventPDFInterface for the given event, if
                exists.
        """

        if not self.session:
            self.session = requests.session()

        current_year = datetime.date.today().year
        if year == current_year or year == current_year - 1:
            # No need to search archive
            url = self.url.build(
                scheme=self.url.scheme,
                host=self.url.host,
                path="/results.php",
            )
        else:
            # Older - must be in archive
            url = self.url.build(
                scheme=self.url.scheme,
                host=self.url.host,
                path="/results-archive.php",
                query=f"year={year:d}",
            )

        result = self.session.post(
            url,
            data={"folder": name},
            headers=self.headers,
        )
        if not result.ok:
            # we try again if LAST YEAR
            if year == current_year - 1:
                # Try the archive.
                url = self.url.build(
                    scheme=self.url.scheme,
                    host=self.url.host,
                    path="/results-archive.php",
                    query=f"year={year:d}",
                )
                result = self.session.post(
                    url,
                    data={"folder": name},
                    headers=self.headers,
                )

        if not result.ok:
            raise FileNotFoundError(
                f"Failed to retrieve {year:d} -> {name} from {url} : status code {result.status_code:d}"
            )

        soup = BeautifulSoup(result.text, "html.parser")
        links = soup.find_all("a")
        links = [link.get("href") for link in links]
        links = [Path(link) for link in links if name in link]

        if len(links) == 0:
            raise FileNotFoundError(
                f"No files found at url {url} / folder {name}: Response HTML:\n{soup}"
            )

        event_url = self.url.build(
            scheme=self.url.scheme,
            host=self.url.host,
            path=f"/{links[0].parent}",
        )

        names = [link.stem for link in links]

        yield CompetitionEventPDFInterface(
            url=event_url,
            names=names,
            session=self.session,
            headers=self.headers,
        )

    def query(self, *, year: int) -> list[str]:
        """List available competition PDFs for a given year.

        Args:
            year: the event year.

        Returns:
            a list of 0 or more event names for the given year.
        """

        # Logic for determining whether to search Archive or not.
        # Basically, if we have requested year = $CURRENT_YEAR,
        # we do not need to look in the archive.

        current_year = datetime.date.today().year
        if year == current_year:
            # No need to search Archive.
            url = self.url.build(
                scheme=self.url.scheme,
                host=self.url.host,
                path="/results.php",
            )
        else:
            # Search Archive for the year specified.
            url = self.url.build(
                scheme=self.url.scheme,
                host=self.url.host,
                path="/results-archive.php",
                query="year={year:d}",
            )

        if not self.session:
            self.session = requests.session()

        result = self.session.get(
            url,
            headers=self.headers,
        )

        soup = BeautifulSoup(result.text, "html.parser")
        links = soup.find_all("td")
        links = [link.findAll(text=True) for link in links]
        it = iter(links)
        res = zip(it, it)

        return [
            Competition(
                name=row[0][0],
                date=datetime.datetime.strptime(row[1][0], "%d/%m/%Y").date(),
            )
            for row in res
        ]

    def create(self, *, year: int, name: str) -> None:
        """Create a new event to store competiton PDFs. UNIMPLEMENTED INTENTIONALLY.

        Args:
            name: the event name (unique within a given year).
            year: the event year.
        """

        raise NotImplementedError(
            "HTTP is not the intended method for storing a competition PDF."
        )
