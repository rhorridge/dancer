"""Unit tests for .competition_pdf.
"""

import datetime
import pytest
from .competition_pdf import CompetitionPDFInterface
from tests.fixtures.http import Response, HtmlResponse


CURRENT_YEAR = datetime.date.today().year


def test_methods(monkeypatch, tmpdir):
    """Test that all of these methods exist."""

    monkeypatch.setattr(
        "requests.sessions.Session.get", lambda *args, **kwargs: Response({})
    )

    monkeypatch.setattr(
        "requests.sessions.Session.post",
        lambda *args, **kwargs: HtmlResponse(
            '<a href="web/results/test/TEST.pdf"></a>'
        ),
    )

    obj = CompetitionPDFInterface(url="http://127.0.0.1")

    assert obj.query(year=CURRENT_YEAR) == []

    with obj.get(name="test", year=CURRENT_YEAR) as obj2:
        assert obj2.query() == ["TEST"]
        assert obj2.get(name="TEST") == b"{}"

    with obj.get(name="test", year=CURRENT_YEAR - 1) as obj2:
        assert obj2.query() == ["TEST"]
        assert obj2.get(name="TEST") == b"{}"

    with obj.get(name="test", year=CURRENT_YEAR - 2) as obj2:
        assert obj2.query() == ["TEST"]
        assert obj2.get(name="TEST") == b"{}"


def test_methods_fail(monkeypatch, tmpdir):
    """Test that all of these methods fail."""

    monkeypatch.setattr(
        "requests.sessions.Session.get",
        lambda *args, **kwargs: Response({}, status_code=404),
    )

    monkeypatch.setattr(
        "requests.sessions.Session.post",
        lambda *args, **kwargs: HtmlResponse("", status_code=404),
    )

    obj = CompetitionPDFInterface(url="http://127.0.0.1")

    with pytest.raises(FileNotFoundError):
        with obj.get(name="test", year=CURRENT_YEAR) as obj2:
            pass

    with pytest.raises(FileNotFoundError):
        with obj.get(name="test", year=CURRENT_YEAR - 1) as obj2:
            pass

    assert obj.query(year=CURRENT_YEAR - 1) == []

    monkeypatch.setattr(
        "requests.sessions.Session.post",
        lambda *args, **kwargs: HtmlResponse('<a href=""></a>', status_code=200),
    )

    with pytest.raises(FileNotFoundError):
        with obj.get(name="test", year=CURRENT_YEAR) as obj2:
            pass

    monkeypatch.setattr(
        "requests.sessions.Session.post",
        lambda *args, **kwargs: HtmlResponse(
            '<a href="test/foo.pdf"></a>', status_code=200
        ),
    )

    with obj.get(name="test", year=CURRENT_YEAR) as obj2:
        with pytest.raises(FileNotFoundError):
            obj2.get(name="FO")

        with pytest.raises(NotImplementedError):
            obj2.store(b"", name="FO")

    with pytest.raises(NotImplementedError):
        obj.create(year=1, name="1")
