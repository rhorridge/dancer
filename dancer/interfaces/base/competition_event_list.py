"""Base interface for storing and retrieving competition eventf liers
(PDFs/ images/txt files).
"""

from abc import abstractmethod
from pydantic import BaseModel
from dancer.competition import Event


class CompetitionEventListInterface(BaseModel):
    """Base class for storing and retrieving information about a particular
    competition from a PDF / txt / image."""

    @abstractmethod
    def get_all(self) -> list[Event]:
        """Retrieve a competition event.

        Returns:
            a list of `Event` objects.
        """
