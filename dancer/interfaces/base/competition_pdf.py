"""Abstract interface for reading and writing competition PDFs.
"""

from abc import abstractmethod
from contextlib import contextmanager
from pydantic import BaseModel
from dancer import Competition


class CompetitionEventPDFInterface(BaseModel):
    """Abstract interface to competition event PDFs.

    May raise an exception.
    """

    @abstractmethod
    def get(self, *, name: str) -> bytes:
        """Fetch a competition event PDF.

        Args:
            name: the name of the individual file (full name minus .pdf).

        Returns:
            byte data representing a Portable Document Format (PDF) document.
        """

    @abstractmethod
    def store(self, pdf: bytes, /, *, name: str) -> None:
        """Store a competition event PDF.

        Args:
            pdf: byte data representing a Portable Document Format (PDF) document.
            name: the name of the individual file (full name minus .pdf).
        """


class CompetitionPDFInterface(BaseModel):
    """Abstract interface to competition PDFs.

    May raise an exception.
    """

    @abstractmethod
    @contextmanager
    def get(self, *, name: str, year: int) -> CompetitionEventPDFInterface:
        """Create a context manager for returning an interface for a specific
        competition..

        Args:
            name: the event name (unique within a given year).
            year: the event year.

        Yields:
            an instance of CompetitionEventPDFInterface for the given event, if
                exists.
        """

    @abstractmethod
    def query(self, *, year: int) -> list[Competition]:
        """List available competitions for a given year.

        Args:
            year: the event year.

        Returns:
            a list of 0 or more `Competition`s for the given year.
        """

    @abstractmethod
    def create(self, *, year: int, name: str) -> None:
        """Create a new event to store competiton PDFs.

        Args:
            name: the event name (unique within a given year).
            year: the event year.
        """
