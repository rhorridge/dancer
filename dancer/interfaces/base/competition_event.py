"""Base interface for storing and retrieving competition events.
"""

from typing import Union
from abc import abstractmethod
from pydantic import BaseModel
from dancer.event.competition import CompetitionCalendarDetails, CompetitionDetails


class CompetitionEventInterface(BaseModel):
    """Base class for storing and retrieving competition events."""

    @abstractmethod
    def store(
        self, key: str, data: Union[CompetitionCalendarDetails, CompetitionDetails]
    ) -> None:
        """Store a competition event.

        Args:
            key: a string key to uniquely identify the object.
            data: an instance of `CompetitionDetails`.
        """

    @abstractmethod
    def fetch(self, key: str) -> Union[CompetitionCalendarDetails, CompetitionDetails]:
        """Retrieve a competition event.

        Args:
            key: a string key to uniquely identify the object.

        Returns:
            an instance of `CompetitionDetails`.
        """

    @abstractmethod
    def query(self) -> list[str]:
        """List available keys for a competition event.

        Returns:
            a list of keys available that identify `CompetitionDetails`.
        """
