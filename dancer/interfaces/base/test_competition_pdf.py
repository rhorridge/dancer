"""Unit tests for .competition_pdf..
"""

import pytest
from .competition_pdf import CompetitionPDFInterface


def test_methods():
    """Test that all of these methods exist."""

    with pytest.raises(TypeError):
        obj = CompetitionPDFInterface()
        obj.get(name='test', year=2022)
        obj.store(b'', name='test', year=2022)
        obj.query(year=2022)
