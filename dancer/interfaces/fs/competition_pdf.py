"""Interface for reading and writing competition PDFs to/from the filesystem.
"""

from contextlib import contextmanager
from pydantic import DirectoryPath
from dancer import Competition
from dancer.interfaces.base.competition_pdf import CompetitionPDFInterface as Base
from dancer.interfaces.base.competition_pdf import (
    CompetitionEventPDFInterface as BaseEvent,
)


class CompetitionEventPDFInterface(BaseEvent):
    """Filesystem interface for reading/writing PDF files.

    Attributes:
        directory: base directory to store the file in, under directories for the year.
    """

    directory: DirectoryPath

    def get(self, *, name: str) -> bytes:
        """Fetch a competition event PDF.

        Args:
            name: the name of the individual file (full name minus .pdf).

        Returns:
            byte data representing a Portable Document Format (PDF) document.
        """

        filename = self.directory / f"{name}.pdf"

        with filename.open("rb") as fp_:
            return fp_.read()

    def store(self, pdf: bytes, /, *, name: str) -> None:
        """Store a competition event PDF.

        Args:
            pdf: byte data representing a Portable Document Format (PDF) document.
            name: the event/round name (unique within a given competition).
        """

        filename = self.directory / f"{name}.pdf"

        with filename.open("wb") as fp_:
            fp_.write(pdf)

    def query(self) -> list[str]:
        """List available competition event PDFs for a given competition.

        Returns:
            a list of 0 or more event PDF names for the given competition.
        """

        return [file_.stem for file_ in self.directory.iterdir()]


class CompetitionPDFInterface(Base):
    """Filesystem interface for reading/writing PDF files.

    Attributes:
        directory: base directory to store the file in, under directories for the year.
    """

    directory: DirectoryPath

    @contextmanager
    def get(self, *, name: str, year: int) -> bytes:
        """Create a context manager for returning an interface for a specific
        competition..

        Args:
            name: the event name (unique within a given year).
            year: the event year.

        Yields:
            an instance of CompetitionEventPDFInterface for the given event, if
                exists.

        Raises:
            FileNotFoundError: if the event does not exist.
        """

        directory = self.directory / f"{year:d}" / f"{name}"

        if not directory.is_dir():
            raise FileNotFoundError(f"Directory {directory} must be created first!")

        yield CompetitionEventPDFInterface(directory=directory)

    def query(self, *, year: int) -> list[Competition]:
        """List available competition PDFs for a given year.

        Args:
            year: the event year.

        Returns:
            a list of 0 or more event names for the given year.
        """

        directory = self.directory / f"{year:d}"

        if not directory.is_dir():
            return []
        return [Competition(name=file_.stem) for file_ in directory.iterdir()]

    def create(self, *, year: int, name: str) -> None:
        """Create a new event to store competiton PDFs.

        Args:
            name: the event name (unique within a given year).
            year: the event year.
        """

        directory = self.directory / f"{year:d}" / f"{name}"

        directory.mkdir(parents=True, exist_ok=True)
