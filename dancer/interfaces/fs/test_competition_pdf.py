"""Unit tests for .competition_pdf.
"""

import pytest
from .competition_pdf import CompetitionPDFInterface


def test_methods(tmpdir):
    """Test that all of these methods exist."""

    obj = CompetitionPDFInterface(directory=tmpdir)

    with pytest.raises(FileNotFoundError):
        with obj.get(name="test", year=2022):
            pass

    assert obj.query(year=2022) == []

    obj.create(name="test", year=2022)

    with obj.get(name="test", year=2022) as obj2:
        with pytest.raises(FileNotFoundError):
            obj2.get(name="TEST")
        assert obj2.query() == []
        obj2.store(b"", name="TEST")
        assert obj2.query() == ["TEST"]
        assert obj2.get(name="TEST") == b""
