"""Google Calendar interface for storing and retrieving competition events.
"""

from typing import Union
import logging
import json
from pathlib import Path
from typing import Optional
import datetime
from abc import abstractmethod
from pydantic import BaseModel, FilePath, EmailStr, HttpUrl, ValidationError
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from dancer.interfaces.base.competition_event import CompetitionEventInterface as Base
from dancer.interfaces.base.competition_event import (
    CompetitionDetails,
    CompetitionCalendarDetails,
)


logging.getLogger("googleapiclient.discovery").setLevel(logging.WARNING)
LOG = logging.getLogger(__name__)


class GoogleCalendarUser(BaseModel):
    email: EmailStr
    displayName: Optional[str] = None
    self: Optional[bool] = None


class GoogleCalendarTime(BaseModel):
    date: datetime.date


class GoogleCalendarReminders(BaseModel):
    useDefault: bool


class GoogleCalendarEvent(BaseModel):
    kind: str
    etag: str
    id: str
    status: str
    htmlLink: HttpUrl
    created: datetime.datetime
    updated: datetime.datetime
    summary: str
    creator: GoogleCalendarUser
    organizer: GoogleCalendarUser
    start: GoogleCalendarTime
    end: GoogleCalendarTime
    transparency: Optional[str]
    iCalUID: str
    sequence: int
    reminders: GoogleCalendarReminders
    eventType: str

    def to_competition_details(self) -> CompetitionDetails:
        """Create an instance of `CompetitionDetails` from this class.

        Returns:
            an instance of `CompetitionDetails`.
        """

        return CompetitionDetails(
            name=self.summary,
            date=self.start.date,
            url=self.htmlLink,
        )


class CompetitionEventInterface(Base):
    """Google Calendar interface for storing and retrieving competition events.

    Attributes:
        client_id: an OAuth2 client ID
    """

    token_file: Path
    client_secret_file: FilePath
    calendar_id: str
    scopes: list[str] = ["https://www.googleapis.com/auth/calendar.events"]
    credentials: object = None
    service: object = None

    def _create_credentials(self) -> None:
        """Create credentials to access Google services."""

        if self.token_file.exists():
            self.credentials = Credentials.from_authorized_user_file(
                self.token_file,
                self.scopes,
            )
        if not self.credentials or not self.credentials.valid:
            if (
                self.credentials
                and self.credentials.expired
                and self.credentials.refresh_token
            ):
                self.credentials.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    self.client_secret_file, self.scopes
                )
                self.credentials = flow.run_local_server(port=0)
            with self.token_file.open("w", encoding="utf-8") as fp_:
                fp_.write(self.credentials.to_json())

    def _connect(self) -> None:
        """connect to the API."""

        self.service = build("calendar", "v3", credentials=self.credentials)

    def store(
        self, key: str, data: Union[CompetitionCalendarDetails, CompetitionDetails]
    ) -> None:
        """Store competition events.

        Args:
            key: a string key to uniquely identify the object.
            data: an instance of CompetitionDetails.
        """

        if not self.credentials:
            self._create_credentials()
        if not self.service:
            self._connect()

        # First, we check if the event already exists - we do not want
        # duplicates!
        try:
            event = self.fetch(key)
        except FileNotFoundError:
            LOG.debug("Event %s does not yet exist!", key)
        else:
            if event == data:
                raise FileExistsError(
                    f"Event {key} already exists! Details: {event.json(indent=4)}"
                )
            # delete it, because we have new data.
            self.delete(key)

        start_time = datetime.datetime.combine(data.date, datetime.time(0, 0, 0))
        end_time = start_time + datetime.timedelta(days=1)
        if hasattr(data, "organiser"):
            if data.organiser.url:
                source = dict(
                    title=data.organiser.name,
                    url=data.organiser.url,
                )
            else:
                source = None
        else:
            source = None
        try:
            res = (
                self.service.events()
                .insert(
                    calendarId=self.calendar_id,
                    body=dict(
                        start=dict(date=data.date.isoformat()),
                        end=dict(date=data.date.isoformat()),
                        summary=data.name,
                        description=data.html_description(),
                        location=data.full_location(),
                        source=source,
                    ),
                )
                .execute()
            )
        except HttpError as err:
            raise ValueError("Failed to store %s", data.json(indent=4)) from err

    def fetch(self, key: str) -> CompetitionDetails:
        """Retrieve competition events.

        Args:
            key: a string key to uniquely identify the object.

        Returns:
            an instance of CompetitionDetails.
        """

        if not self.credentials:
            self._create_credentials()
        if not self.service:
            self._connect()

        # raise NotImplementedError('Not implemented for this interface!')
        date = datetime.date.fromisoformat(key.split(":")[0])
        time_min = (
            datetime.datetime.combine(
                date,
                datetime.time(0, 0, 0),
            )
            - datetime.timedelta(hours=2)
        ).isoformat() + "Z"
        res = (
            self.service.events()
            .list(
                calendarId=self.calendar_id,
                timeMin=time_min,
                maxResults=10,
                singleEvents=True,
                orderBy="startTime",
            )
            .execute()
        )

        items = res["items"]
        events = [GoogleCalendarEvent(**row).to_competition_details() for row in items]
        match = [event for event in events if event.id == key]
        if not match:
            raise FileNotFoundError(
                "Event of key %s not found in Google Calendar! Found: %s", key, res
            )
        return match[0]

    def query(self) -> list[str]:
        """List available keys for competition events.

        Returns:
            a list of keys available that identify CompetitionDetails.
        """

        if not self.credentials:
            self._create_credentials()
        if not self.service:
            self._connect()

        now = datetime.datetime.utcnow().isoformat() + "Z"
        res = (
            self.service.events()
            .list(
                calendarId=self.calendar_id,
                singleEvents=True,
                orderBy="startTime",
            )
            .execute()
        )

        items = res["items"]

        events = [GoogleCalendarEvent(**row).to_competition_details() for row in items]
        return [event.id for event in events]

    def delete(self, key: str) -> None:
        """Delete the event with the given key."""

        if not self.credentials:
            self._create_credentials()
        if not self.service:
            self._connect()

        date = datetime.date.fromisoformat(key.split(":")[0])
        time_min = (
            datetime.datetime.combine(date, datetime.time(0, 0, 0))
            - datetime.timedelta(hours=2)
        ).isoformat() + "Z"

        res = (
            self.service.events()
            .list(
                calendarId=self.calendar_id,
                timeMin=time_min,
                maxResults=10,
                singleEvents=True,
                orderBy="startTime",
            )
            .execute()
        )

        items = res["items"]
        events = [GoogleCalendarEvent(**row) for row in items]
        match = [event for event in events if event.to_competition_details().id == key]
        LOG.info("There are %d events matching", len(match))

        for item in match:
            self.service.events().delete(
                calendarId=self.calendar_id,
                eventId=item.id,
            ).execute()
            LOG.info("Deleted event %s", item.id)
