"""Redis interface for storing and retrieving competition events.
"""

from typing import Union
import json
import redis
from abc import abstractmethod
from pydantic import RedisDsn, ValidationError
from dancer.interfaces.base.competition_event import CompetitionEventInterface as Base
from dancer.interfaces.base.competition_event import (
    CompetitionDetails,
    CompetitionCalendarDetails,
)


class CompetitionEventInterface(Base):
    """Redis interface for storing and retrieving competition events.

    Attributes:
        uri: a URL to the Redis database used.
    """

    uri: RedisDsn
    prefix: str = "competition-event"
    connection: object = None

    def _create_connection(self) -> None:
        """Create the Redis connection."""

        self.connection = redis.from_url(self.uri, decode_responses=True)

    def _connection(self) -> object:
        """Return the active connection, creating if it does not exist.

        Returns:
            the `redis.Redis` object.
        """

        if not self.connection:
            self._create_connection()
        return self.connection

    def _generate_key(self, key: str) -> str:
        """Generate the actual Redis key to be used.

        Args:
            key: a string key to uniquely identify the object.

        Returns:
            a key in the Redis database.
        """

        return f"{self.prefix}:{key}"

    def store(
        self, key: str, data: Union[CompetitionCalendarDetails, CompetitionDetails]
    ) -> None:
        """Store competition events.

        Args:
            key: a string key to uniquely identify the object.
            data: an instance of CompetitionDetails.
        """

        pipeline = self._connection().pipeline()
        pipeline.multi()
        pipeline.set(
            self._generate_key(key),
            data.json(),
        )
        pipeline.sadd(f"{self.prefix}-set", key)
        pipeline.execute()

    def fetch(self, key: str) -> Union[CompetitionCalendarDetails, CompetitionDetails]:
        """Retrieve competition events.

        Args:
            key: a string key to uniquely identify the object.

        Returns:
            an instance of CompetitionDetails.
        """

        string = self._connection().get(self._generate_key(key))
        if not string:
            raise FileNotFoundError(
                f"{key} not found at {self._generate_key(key)} in {self.uri}!"
            )
        try:
            return CompetitionCalendarDetails(**json.loads(string))
        except ValidationError:
            return CompetitionDetails(**json.loads(string))

    def query(self) -> list[str]:
        """List available keys for competition events.

        Returns:
            a list of keys available that identify CompetitionDetails.
        """

        return self._connection().smembers(f"{self.prefix}-set")
