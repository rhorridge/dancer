"""Module to scrape Easycomp for results.
"""

import datetime
import os
import requests
import logging
from typing import Optional, Type
from bs4 import BeautifulSoup
from dancer.interfaces import CompetitionPDFInterface
from dancer.interfaces.http import CompetitionPDFInterface as HttpInterface

LOG = logging.getLogger(__name__)


def download_comp_results(
    url: str, *, name: str, year: int, interface: Type[CompetitionPDFInterface]
) -> None:
    """Download competition results to a directory."""

    web_iface = HttpInterface(url=url)
    competition = name
    existing_comps = interface.query(year=year)
    if competition not in existing_comps:
        interface.create(year=year, name=competition)

    with web_iface.get(name=competition, year=year) as http_iface:
        with interface.get(year=year, name=competition) as iface:
            existing_events = iface.query()
            for link in http_iface.query():
                if link in existing_events:
                    LOG.debug(
                        "%s already in %d existing events for %d %s",
                        link,
                        len(existing_events),
                        year,
                        competition,
                    )
                    continue

                pdf = http_iface.get(name=link)
                iface.store(pdf, name=link)


def view_all_available_comp_results(url: str):
    """View all available competition results."""

    web_iface = HttpInterface(url=url)
    year = datetime.date.today().year

    return web_iface.query(year=year)


if __name__ == "__main__":
    lst = view_all_available_comp_results(os.environ.get("EASYCOMP_URL"))

    i = 0
    for comp in lst:
        print(comp)
        i += 1
