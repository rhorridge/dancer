"""scrape competition results and other
data from the web.
"""

import os
import argparse
from dancer.main import preprocess
from .easycomp import download_comp_results


def main(event: str):
    """main program."""

    results = download_comp_results(
        os.environ.get("EASYCOMP_URL"),
        event,
        "data/",
    )
    preprocess(event, "data/")
    print(results)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Scrape competition results and whatnot"
    )
    parser.add_argument("--event", "-e", type=str)
    args = parser.parse_args()
    main(args.event)
