"""test web scraper
"""

import datetime
from .dpaonline import date_parse


def test_date_parse():
    """Test that dates are correctly parsed."""

    year = datetime.date.today().year
    res = date_parse("November 27")
    assert res.year == year
    assert res.month == 11
    assert res.day == 27
    res = date_parse("February 28, 2021")
    assert res.year == 2021
    assert res.month == 2
    assert res.day == 28
