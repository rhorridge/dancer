"""Scrape data from the DPA online calendar.
"""

import logging
import datetime
import os
from typing import Optional, Union
from pathlib import Path
import requests
from bs4 import BeautifulSoup
from pydantic import BaseModel, HttpUrl, FilePath, validate_arguments
from dancer.interfaces.redis.competition_event import CompetitionEventInterface
from dancer.event.competition import CompetitionDetails, CompetitionCalendarDetails

LOG = logging.getLogger(__name__)

DEFAULT_HEADERS = "Mozilla/5.0 (X11; Linux x86_64; rv:91.0) Gecko/20100101 Firefox/91.0"


def date_parse(datestr: str) -> datetime.date:
    """Parse datestr as an instance of datetime.date.
    This function expects the input to be of the
        format used for the DPA calendar:
        long month followed by day.
    """

    datestr = datestr.strip()
    try:
        parsed = datetime.datetime.strptime(datestr, "%d %B, %Y")
    except ValueError:
        # first try parsing it the other way around
        try:
            parsed = datetime.datetime.strptime(datestr, "%B %d, %Y")
        except ValueError:
            try:
                parsed = datetime.datetime.strptime(datestr, "%d %B")
                parsed = parsed.replace(year=datetime.date.today().year)
            except ValueError:
                # day and month are the wrong way around
                parsed = datetime.datetime.strptime(datestr, "%B %d")
                parsed = parsed.replace(year=datetime.date.today().year)
    return parsed.date()


class ScrapedCompetition(BaseModel):
    """Represents an existing calendar event that holds further URL information.

    Attributes:
        url: the URL to a webpage with more information.
    """

    url: HttpUrl
    headers: dict[str, str]
    comp: CompetitionDetails
    content: Optional[CompetitionCalendarDetails] = None

    @property
    def id(self) -> str:
        """Return a unique ID for this competition."""

        return self.comp.id

    def scrape(self) -> None:
        """Update the content with a new CompetionCalendarDetails."""

        with requests.session() as sess:
            result = sess.get(self.url, headers=self.headers)
            if result.status_code >= 400:
                raise RuntimeError(
                    f"Requests to {self.url} failed with status {result.status}"
                )

            soup = BeautifulSoup(
                result.text,
                "html.parser",
            )

        result = soup.find_all("div", class_="primary")
        result = result[0]
        LOG.debug(result)
        start_date = result.find_all("abbr", class_="tribe-events-start-date")
        category = result.find_all("dd", class_="tribe-events-event-categories")
        event_url = result.find_all("dd", class_="tribe-events-event-url")
        venue_name = result.find_all("dd", class_="tribe-venue")
        venue_street = result.find_all("span", class_="tribe-street-address")
        venue_locality = result.find_all("span", class_="tribe-locality")
        venue_postcode = result.find_all("span", class_="tribe-postal-code")
        venue_country = result.find_all("span", class_="tribe-country-name")
        google_maps = result.find_all("a", class_="tribe-events-gmap")
        organiser = result.find_all("dd", class_="tribe-organizer")
        organiser_telephone = result.find_all("dd", class_="tribe-organizer-tel")
        organiser_email = result.find_all("dd", class_="tribe-organizer-email")
        organiser_url = result.find_all("dd", class_="tribe-organizer-url")

        start_date = start_date[0].get("title")
        category = category[0].find_all("a")[0].text
        if event_url:
            event_url = event_url[0].find_all("a")[0].get("href")
        else:
            event_url = None
        venue_name = venue_name[0].text.strip()
        venue_street = venue_street[0].text.strip()
        if venue_locality:
            venue_locality = venue_locality[0].text.strip().strip(".")
        else:
            venue_locality = venue_street.split(",")[-1].strip()
        venue_postcode = venue_postcode[0].text.strip()
        if venue_country:
            venue_country = venue_country[0].text.strip()
        else:
            venue_country = "United Kingdom"
        if google_maps:
            google_maps = google_maps[0].get("href")
        else:
            google_maps = None
        if organiser:
            organiser = organiser[0].text.strip()
        else:
            organiser = None
        if organiser_telephone:
            organiser_telephone = organiser_telephone[0].text.strip()
        else:
            organiser_telephone = None
        if organiser_email:
            organiser_email = organiser_email[0].text.strip()
            organiser_emails = [
                email for email in organiser_email.split(" ") if "@" in email
            ]
        else:
            organiser_emails = None
        if organiser_url:
            organiser_url = organiser_url[0].find_all("a")[0].get("href")
        else:
            organiser_url = None

        LOG.debug(start_date)
        LOG.debug(category)
        LOG.debug(event_url)
        LOG.debug(venue_name)
        LOG.debug(venue_street)
        LOG.debug(venue_locality)
        LOG.debug(venue_postcode)
        LOG.debug(venue_country)
        LOG.debug(google_maps)
        LOG.debug(organiser)
        LOG.debug(organiser_telephone)
        LOG.debug(organiser_emails)
        LOG.debug(organiser_url)

        self.content = CompetitionCalendarDetails(
            **self.comp.dict(exclude_none=True),
            venue=venue_name,
            event_category=category,
            event_url=event_url,
            venue_address={
                "street_address": venue_street,
                "locality": venue_locality,
                "postcode": venue_postcode,
                "country": venue_country,
                "map_url": google_maps,
            },
            organiser={
                "name": organiser,
                "telephone": organiser_telephone,
                "email": organiser_emails,
                "url": organiser_url,
            },
        )


class Scraper(BaseModel):
    uri: Union[HttpUrl, FilePath]
    headers: dict[str, str] = {"User-Agent": DEFAULT_HEADERS}
    content: list[ScrapedCompetition] = []

    class Config:
        validate_assignment: True

    @validate_arguments
    def _download(self, filename: Path) -> None:
        """Download a file."""

        with requests.session() as sess:
            result = sess.get(
                self.uri,
                headers=self.headers,
            )
            with filename.open("w", encoding="utf-8") as fp:
                fp.write(result.text)
            self.uri = filename

    class Config:
        validate_assignment: True

    @validate_arguments
    def _download(self, filename: Path) -> None:
        """Download a file."""

        with requests.session() as sess:
            result = sess.get(
                self.uri,
                headers=self.headers,
            )
            with filename.open("w", encoding="utf-8") as fp:
                fp.write(result.text)
            self.uri = filename

    def scrape(self) -> None:
        """Scrape the details from the website."""

        if str(self.uri).startswith("http"):
            self._download("./dpaonline.html")

        with self.uri.open("r", encoding="utf-8") as file_:
            html = file_.read()
            soup = BeautifulSoup(
                html,
                "html.parser",
            )
            diva = soup.find_all(class_="fusion-events-meta")
            for i in diva:
                LOG.debug(i)
                urls = i.find_all("a")
                if len(urls) == 1:
                    name = urls[0].text.strip()
                    url = urls[0]["href"]
                    dates = i.find_all(class_="tribe-event-date-start")
                    if len(dates) == 1:
                        date = date_parse(dates[0].text)
                        obj = CompetitionDetails(
                            name=name,
                            url=url,
                            date=date,
                        )
                        scraped = ScrapedCompetition(
                            url=obj.url,
                            headers=self.headers,
                            comp=obj,
                        )
                        self.content.append(scraped)

        with self.uri.open("r", encoding="utf-8") as file_:
            html = file_.read()
            soup = BeautifulSoup(
                html,
                "html.parser",
            )
            diva = soup.find_all(class_="fusion-events-meta")
            for i in diva:
                urls = i.find_all("a")
                if len(urls) == 1:
                    name = urls[0].text
                    url = urls[0]["href"]
                    dates = i.find_all(class_="tribe-event-date-start")
                    if len(dates) == 1:
                        date = date_parse(dates[0].text)
                        obj = CompetitionDetails(
                            name=name,
                            url=url,
                            date=date,
                        )
                        self.content.append(obj)


if __name__ == "__main__":
    import out

    out.configure(level="debug", default_level="info", theme="production")
    scraper = Scraper(uri=os.getcwd() + "/dpaonline.html")
    scraper.scrape()

    iface = CompetitionEventInterface(
        uri="redis://localhost",
        prefix="competition-event:dpa-ballroom-and-latin",
    )
    keys = iface.query()

    LOG.info("%d keys existing in %s", len(keys), iface.prefix)

    for elem in scraper.content:
        if elem.id in keys:
            LOG.info("%s already exists in %s", elem.id, iface.prefix)
            continue
        LOG.info("Storing %s in %s", elem.id, iface.prefix)
        iface.store(elem.id, elem)

    keys = iface.query()

    LOG.info("%d keys existing in %s", len(keys), iface.prefix)
