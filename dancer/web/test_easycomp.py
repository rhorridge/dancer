"""Unit tests for .easycomp.
"""

import datetime
from . import easycomp
from pathlib import Path
from typing import Optional
from pydantic import DirectoryPath
from dancer.interfaces.fs import CompetitionPDFInterface
from tests.fixtures.http import Response


CURRENT_YEAR = datetime.date.today().year


def test_download_comp_results(monkeypatch, tmpdir):
    """Test the (mocked) downloading of competition results.

    TODO: replace the patching below with interfaces.
    """

    dir1 = tmpdir / "1"
    dir2 = tmpdir / "2"

    dir1.mkdir()
    dir2.mkdir()

    monkeypatch.setattr(
        f"{easycomp.__name__}.HttpInterface",
        lambda *, url: CompetitionPDFInterface(directory=dir1),
    )

    tmp = CompetitionPDFInterface(directory=dir1)
    tmp.create(year=2022, name="competition")

    interface = CompetitionPDFInterface(directory=dir2)
    interface.create(year=2022, name="competition")

    easycomp.download_comp_results(
        "http://127.0.0.1", name="competition", year=2022, interface=interface
    )

    with interface.get(year=2022, name="competition") as iface:
        assert iface.query() == []

    with tmp.get(year=2022, name="competition") as iface:
        iface.store(b"", name="TEST")

    # For adding extra branch coverage
    easycomp.download_comp_results(
        "http://127.0.0.1", name="competition", year=2022, interface=interface
    )

    with interface.get(year=2022, name="competition") as iface:
        assert iface.query() == ["TEST"]

    # For adding extra branch coverage
    easycomp.download_comp_results(
        "http://127.0.0.1", name="competition", year=2022, interface=interface
    )


def test_download_comp_results_fail(monkeypatch, tmpdir):
    """Test the (mocked, failed) downloading of competition results.

    TODO: replace the patching below with interfaces.
    """

    monkeypatch.setattr(
        f"{easycomp.__name__}.HttpInterface",
        lambda *, url: CompetitionPDFInterface(directory=tmpdir),
    )

    interface = CompetitionPDFInterface(directory=tmpdir)
    easycomp.download_comp_results(
        "http://127.0.0.1", name="competition", year=2022, interface=interface
    )


def test_view_all_available_comp_results(tmpdir, monkeypatch):
    """Test viewing of competition results."""

    monkeypatch.setattr(
        f"{easycomp.__name__}.HttpInterface",
        lambda *, url: CompetitionPDFInterface(directory=tmpdir),
    )

    interface = CompetitionPDFInterface(directory=tmpdir)
    assert easycomp.view_all_available_comp_results("url") == []

    interface.create(year=CURRENT_YEAR, name="TEST")

    assert easycomp.view_all_available_comp_results("url")[0].name == "TEST"
