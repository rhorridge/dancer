"""Some standard event types.
"""

from pydantic import BaseModel
from dancer.competition import Category
from dancer.dance import (
    Waltz,
    Quickstep,
    ChaChaCha,
    Jive,
    Tango,
    Rumba,
    VienneseWaltz,
    PasoDoble,
    Foxtrot,
    Samba,
    Salsa,
    AcroRockNRoll,
    NonAcroRockNRoll,
    ClassicalSequence,
)


class EventsList(BaseModel):
    __root__: list[Category]


# Beginner Ballroom
BeginnerW = Category(name="Beginner W", dances=[Waltz], restricted=True)
BeginnerQ = Category(name="Beginner Q", dances=[Quickstep], restricted=True)
BeginnerWQ = Category(name="Beginner WQ", dances=[Waltz, Quickstep], restricted=True)
# Beginner Latin
BeginnerC = Category(name="Beginner C", dances=[ChaChaCha], restricted=True)
BeginnerJ = Category(name="Beginner J", dances=[Jive], restricted=True)
BeginnerCJ = Category(name="Beginner CJ", dances=[ChaChaCha, Jive], restricted=True)

# Novice Ballroom
NoviceW = Category(name="Novice W", dances=[Waltz], restricted=True)
NoviceQ = Category(name="Novice Q", dances=[Quickstep], restricted=True)
NoviceV = Category(name="Novice V", dances=[VienneseWaltz], restricted=True)
NoviceT = Category(name="Novice T", dances=[Tango], restricted=True)
NoviceF = Category(name="Novice F", dances=[Foxtrot], restricted=True)
NoviceWQ = Category(name="Novice WQ", dances=[Waltz, Quickstep], restricted=True)
NoviceWVQ = Category(
    name="Novice WVQ", dances=[Waltz, VienneseWaltz, Quickstep], restricted=True
)
# Novice Latin
NoviceC = Category(name="Novice C", dances=[ChaChaCha], restricted=True)
NoviceJ = Category(name="Novice J", dances=[Jive], restricted=True)
NoviceR = Category(name="Novice R", dances=[Rumba], restricted=True)
NoviceS = Category(name="Novice S", dances=[Samba], restricted=True)
NoviceCJ = Category(name="Novice CJ", dances=[ChaChaCha, Jive], restricted=True)
NoviceCSJ = Category(
    name="Novice CSJ", dances=[ChaChaCha, Samba, Jive], restricted=True
)

# Pre-Intermediate Ballroom
PreIntermediateWQ = Category(name="Pre-Intermediate WQ", dances=[Waltz, Quickstep])

# Pre-Intermediate Latin
PreIntermediateCJ = Category(name="Pre-Intermediate CJ", dances=[ChaChaCha, Jive])

# Intermediate Ballroom
IntermediateWTQ = Category(name="Intermediate WTQ", dances=[Waltz, Tango, Quickstep])
IntermediateWFQ = Category(name="Intermediate WFQ", dances=[Waltz, Foxtrot, Quickstep])
IntermediateWTVQ = Category(
    name="Intermediate WTVQ", dances=[Waltz, Tango, VienneseWaltz, Quickstep]
)

# Intermediate Latin
IntermediateCRJ = Category(name="Intermediate CRJ", dances=[ChaChaCha, Rumba, Jive])
IntermediateCSJ = Category(name="Intermediate CSJ", dances=[ChaChaCha, Samba, Jive])
IntermediateCSRJ = Category(
    name="Intermediate CSRJ", dances=[ChaChaCha, Samba, Rumba, Jive]
)

# Advanced Ballroom
AdvancedWQ = Category(name="Advanced WQ", dances=[Waltz, Quickstep])
AdvancedWTQ = Category(name="Advanced WTQ", dances=[Waltz, Tango, Quickstep])
AdvancedWFQ = Category(name="Advanced WFQ", dances=[Waltz, Foxtrot, Quickstep])
AdvancedWTVQ = Category(
    name="Advanced WTVQ", dances=[Waltz, Tango, VienneseWaltz, Quickstep]
)
AdvancedF = Category(name="Advanced F", dances=[Foxtrot])

# Advanced Latin
AdvancedCJ = Category(name="Advanced CJ", dances=[ChaChaCha, Jive])
AdvancedCSJ = Category(name="Advanced CSJ", dances=[ChaChaCha, Samba, Jive])
AdvancedCRJ = Category(name="Advanced CRJ", dances=[ChaChaCha, Rumba, Jive])
AdvancedCSRJ = Category(name="Advanced CSRJ", dances=[ChaChaCha, Samba, Rumba, Jive])
AdvancedP = Category(name="Advanced P", dances=[PasoDoble])
AdvancedCRPJ = Category(
    name="Advanced CRPJ", dances=[ChaChaCha, Rumba, PasoDoble, Jive]
)

# Open Ballroom
OpenWQ = Category(name="Open WQ", dances=[Waltz, Quickstep])
OpenWF = Category(name="Open WF", dances=[Waltz, Foxtrot])
OpenWFQ = Category(name="Open WFQ", dances=[Waltz, Foxtrot, Quickstep])
OpenWTQ = Category(name="Open WTQ", dances=[Waltz, Tango, Quickstep])
OpenWVQ = Category(name="Open WVQ", dances=[Waltz, VienneseWaltz, Quickstep])
OpenT = Category(name="Open Tango", dances=[Tango])
OpenF = Category(name="Open Foxtrot", dances=[Foxtrot])
OpenV = Category(name="Open Viennese Waltz", dances=[VienneseWaltz])
OpenWTVFQ = Category(
    name="Open Five Dance - Ballroom",
    dances=[Waltz, Tango, VienneseWaltz, Foxtrot, Quickstep],
)

# Open Latin
OpenCJ = Category(name="Open CJ", dances=[ChaChaCha, Jive])
OpenCS = Category(name="Open CS", dances=[ChaChaCha, Samba])
OpenCRJ = Category(name="Open CRJ", dances=[ChaChaCha, Rumba, Jive])
OpenCSJ = Category(name="Open CSJ", dances=[ChaChaCha, Samba, Jive])
OpenS = Category(name="Open Samba", dances=[Samba])
OpenR = Category(name="Open Rumba", dances=[Rumba])
OpenP = Category(name="Open Paso Doble", dances=[PasoDoble])
OpenCSRPJ = Category(
    name="Open Five Dance - Latin", dances=[ChaChaCha, Samba, Rumba, PasoDoble, Jive]
)

# Salsa and R & R
OpenSalsa = Category(name="Open Salsa", dances=[Salsa])
NonAcroRnR = Category(name="Non-Acrobatic Rock 'n' Roll", dances=[NonAcroRockNRoll])
AcroRnR = Category(name="Acrobatic Rock 'n' Roll", dances=[AcroRockNRoll])


# Team Match
TeamMatch = Category(name="Team Match", dances=[Waltz, ChaChaCha, Quickstep, Jive])

# Open Circuit - National League
NationalLeague = Category(
    name="National League - Unknown Event", dances=[], league="NL"
)

# Unknown Event
Unknown = Category(name="Unknown", dances=[], league="UNKNOWN")

# Sequence
ClassicalSequence = Category(name="Classical Sequence", dances=[ClassicalSequence])


ALL_EVENTS = EventsList(
    __root__=[
        BeginnerW,
        BeginnerQ,
        BeginnerC,
        BeginnerJ,
        BeginnerWQ,
        BeginnerCJ,
        NoviceW,
        NoviceQ,
        NoviceC,
        NoviceJ,
        NoviceT,
        NoviceR,
        NoviceS,
        NoviceF,
        NoviceWQ,
        NoviceWVQ,
        NoviceCJ,
        NoviceCSJ,
        PreIntermediateWQ,
        PreIntermediateCJ,
        IntermediateWTQ,
        IntermediateWFQ,
        IntermediateWTVQ,
        IntermediateCRJ,
        IntermediateCSJ,
        IntermediateCSRJ,
        AdvancedCRJ,
        AdvancedWFQ,
        AdvancedWTQ,
        AdvancedCSJ,
        AdvancedCSRJ,
        AdvancedWTVQ,
        AdvancedCRPJ,
        OpenWQ,
        OpenWF,
        OpenCJ,
        OpenCS,
        OpenWTQ,
        OpenWFQ,
        OpenWVQ,
        OpenCRJ,
        OpenCSJ,
        OpenF,
        OpenS,
        OpenV,
        OpenP,
        OpenR,
        OpenT,
        OpenWTVFQ,
        OpenCSRPJ,
        OpenSalsa,
        NonAcroRnR,
        AcroRnR,
        TeamMatch,
    ]
)


if __name__ == "__main__":
    print(ALL_EVENTS.json(indent=4, exclude_none=True))
