"""Utility functions for reporting scrutineering results.
"""


def convert_to_place_order(placings: dict[int, int]) -> dict[int, int]:
    """Convert placings - a dictionary of {number: place}
    into a dictionary of {place: number}.


    Args:
        placings: a dictionary of all couples that danced
            in a final with their corresponding place.

    Returns:
        a dictionary of all the places (1-n) with the
            the corresponding couple.

    Raises:
        a Keyerror if there are two (or more)
            couples with the same place.
    """

    places = set(placings.values())
    if len(places) != len(placings):
        # there is a mismatch - most likely a duplicate
        # place.
        raise KeyError(
            f"Expected {len(placings)} unique places: likely a duplicate in {placings.values()}"
        )

    return {value: key for key, value in placings.items()}
