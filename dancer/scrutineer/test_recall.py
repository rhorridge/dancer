from dancer.competition import create_marks
from .recall import *


def test_calculate_recall():
    assert calculate_recall(1) == 1
    assert calculate_recall(6) == 6
    assert calculate_recall(10) == 6
    assert calculate_recall(12) == 6
    assert calculate_recall(16) == 8
    assert calculate_recall(20) == 12
    assert calculate_recall(24) == 12
    assert calculate_recall(28) == 14


def test_recall():
    judges = ["A"]
    marks = {
        1: ["A"],
        2: [""],
    }
    res = recall(
        judges=judges,
        marks=marks,
        recalling=2,
    )
    assert res.couples_recalled() == {1, 2}

    judges = ["G", "H", "I", "J", "K", "L"]
    marks = {
        40: ["GHJKL", "KL"],
        75: ["KL", "HJ"],
        80: ["GHJKL", "GHJKL"],
        85: ["GHIL", "GHIJKL"],
        95: ["GJL", "IJL"],
        178: ["GHI", ""],
        202: ["IL", "I"],
        229: ["GIKL", "GHIJKL"],
        270: ["HJ", "GHI"],
        411: ["HJK", "HIJKL"],
        423: ["IJ", "G"],
        470: ["IK", "GK"],
    }

    res = recall(
        judges=judges,
        marks=marks,
    )
    assert res.couples_recalled() == {40, 80, 85, 95, 229, 411}
    assert str(res) == "Recalled 6 from 12"
