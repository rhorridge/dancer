"""Recall couples based on their marks.
"""

import logging
from typing import Optional
from pydantic import validate_arguments
from dancer.competition import Marks, CoupleRound, Round, create_marks


LOG = logging.getLogger(__name__)


@validate_arguments
def calculate_recall(num_couples: int) -> int:
    """Calculate the number of couples to recall."""

    if num_couples < 6:
        return num_couples

    if num_couples <= 12:
        return 6

    if num_couples <= 14:
        return 7

    if num_couples <= 16:
        return 8

    if num_couples <= 24:
        return 12

    if num_couples <= 26:
        return 13

    if num_couples <= 28:
        return 14

    if num_couples <= 40:
        return 24

    if num_couples <= 56:
        return 32

    if num_couples <= 96:
        return int((num_couples * 2) / 3)

    return int((num_couples * 3) / 4)


@validate_arguments
def total_marks(marks: list[str]) -> int:
    """Return the total number of marks in the current set of dances."""

    acc = 0
    for mark in marks:
        acc += len(mark)
    return acc


@validate_arguments
def recall(
    *,
    judges: list[str],
    marks: dict[int, list[str]],
    recalling: Optional[int] = None,
) -> Round:
    """Recall into the next round based on a threshold."""

    if not recalling:
        recalling = calculate_recall(len(marks))

    # start off needing all judges to mark in all dances
    current = len(judges) * len(marks)
    recalled = []
    while len(recalled) < recalling:
        recalled = [key for key, mark in marks.items() if total_marks(mark) >= current]
        LOG.trace("%d recalled with %d marks: %s", len(recalled), current, recalled)
        current = current - 1
        if recalling == 0:
            LOG.warning("Recalling threshold is now zero!")
            break

    return Round(
        couples={
            key: CoupleRound(
                marks=[create_marks(itm) for itm in val],
                recalled=(key in recalled),
            )
            for key, val in marks.items()
        }
    )


if __name__ == "__main__":
    print("# Import the module.")
    print("from dancer.scrutineer.recall import recall")
    print()
    print("# Declare the judges.")
    print('judges = ["A", "B", "C"]')
    print()
    print("# Declare the marks.")
    print('marks = {1: ["A"], 2: [], 3: ["A", "B"]}')
    print()
    print("# Calculate the recall based on different thresholds.")
    print("recall1 = recall(judges=judges, marks=marks)  # default")
    print("recall2 = recall(judges=judges, marks=marks, recalling=2)")
    print("recall3 = recall(judges=judges, marks=marks, recalling=1)")
    print()
    print("# Print the recalls as JSON.")
    print("print(recall1.json(exclude_none=True, indent=4))")
    print("print(recall2.json(exclude_none=True, indent=4))")
    print("print(recall3.json(exclude_none=True, indent=4))")
