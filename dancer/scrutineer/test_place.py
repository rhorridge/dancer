"""Unit tests for place.*.
"""

from .place import *
from .utility import convert_to_place_order


def test_calculate_majority():
    """Test for majority."""

    assert calculate_majority(["A", "B"]) == 2
    assert calculate_majority(["A", "B", "C"]) == 2
    assert calculate_majority(["A", "B", "C", "D"]) == 3


def test_count_place():
    """Test counting placings."""

    place = {
        "A": 1,
        "B": 2,
        "C": 3,
    }

    assert count_place(0, place) == 0
    assert count_place(1, place) == 1
    assert count_place(2, place) == 2
    assert count_place(3, place) == 3


def test_n_or_higher():
    """Test whether n or higher can be returned."""

    placings = {
        40: {"G": 5, "H": 4, "I": 5, "J": 6, "K": 4, "L": 6, "M": 6},
        80: {"G": 2, "H": 2, "I": 3, "J": 2, "K": 2, "L": 3, "M": 2},
        85: {"G": 4, "H": 3, "I": 4, "J": 1, "K": 5, "L": 2, "M": 3},
        95: {"G": 1, "H": 6, "I": 2, "J": 3, "K": 6, "L": 1, "M": 5},
        229: {"G": 3, "H": 5, "I": 1, "J": 4, "K": 3, "L": 4, "M": 4},
        411: {"G": 6, "H": 1, "I": 6, "J": 5, "K": 1, "L": 5, "M": 1},
    }
    assert n_or_higher(placings, 0) == {
        40: 0,
        80: 0,
        85: 0,
        95: 0,
        229: 0,
        411: 0,
    }
    assert n_or_higher(placings, 1) == {
        40: 0,
        80: 0,
        85: 1,
        95: 2,
        229: 1,
        411: 3,
    }
    assert n_or_higher(placings, 2) == {
        40: 0,
        80: 5,
        85: 2,
        95: 3,
        229: 1,
        411: 3,
    }


def test_place_dance():
    judges = ["G", "H", "I", "J", "K", "L", "M"]
    placings = {
        40: {"G": 5, "H": 4, "I": 5, "J": 6, "K": 4, "L": 6, "M": 6},
        80: {"G": 2, "H": 2, "I": 3, "J": 2, "K": 2, "L": 3, "M": 2},
        85: {"G": 4, "H": 3, "I": 4, "J": 1, "K": 5, "L": 2, "M": 3},
        95: {"G": 1, "H": 6, "I": 2, "J": 3, "K": 6, "L": 1, "M": 5},
        229: {"G": 3, "H": 5, "I": 1, "J": 4, "K": 3, "L": 4, "M": 4},
        411: {"G": 6, "H": 1, "I": 6, "J": 5, "K": 1, "L": 5, "M": 1},
    }

    res = place_dance(
        judges=judges,
        placings=placings,
        majority=4,
    )
    assert res == {
        80: 1,
        95: 2,
        85: 3,
        229: 4,
        411: 5,
        40: 6,
    }

    placings = {
        40: {"G": 5, "H": 4, "I": 5, "J": 6, "K": 4, "L": 6, "M": 5},
        80: {"G": 2, "H": 1, "I": 1, "J": 3, "K": 2, "L": 3, "M": 2},
        85: {"G": 1, "H": 2, "I": 2, "J": 1, "K": 3, "L": 4, "M": 6},
        95: {"G": 4, "H": 5, "I": 4, "J": 2, "K": 6, "L": 1, "M": 4},
        229: {"G": 3, "H": 6, "I": 3, "J": 5, "K": 1, "L": 2, "M": 3},
        411: {"G": 6, "H": 3, "I": 6, "J": 4, "K": 5, "L": 5, "M": 1},
    }

    res = place_dance(
        judges=judges,
        placings=placings,
        majority=4,
    )

    assert res == {
        80: 1,
        85: 2,
        229: 3,
        95: 4,
        411: 5,
        40: 6,
    }


def test_convert_placings_to_list():
    placings = {1: [{"A": 1}]}
    assert convert_placings_to_list(placings) == [{1: {"A": 1}}]


def test_rank_by_value():
    placings = {5: 2, 4: 5, 3: 3, 2: 8, 1: 1}
    assert rank_by_value(placings) == {
        1: 1,
        5: 2,
        3: 3,
        4: 4,
        2: 5,
    }


def test_find_equal_values():
    placings = {1: 2, 2: 2, 3: 3}
    assert find_equal_values(placings) == {
        1: 2,
        2: 2,
    }


def test_rule_9_calculations():
    placings = [{1: 1}, {1: 2}]
    assert rule_9_calculations(placings).ok == {1: 1}


def test_place():
    judges = ["G", "H", "I", "J", "K", "L", "M"]
    placings = {
        40: [
            {"G": 5, "H": 4, "I": 5, "J": 6, "K": 4, "L": 6, "M": 6},
            {"G": 5, "H": 4, "I": 5, "J": 6, "K": 4, "L": 6, "M": 5},
        ],
        80: [
            {"G": 2, "H": 2, "I": 3, "J": 2, "K": 2, "L": 3, "M": 2},
            {"G": 2, "H": 1, "I": 1, "J": 3, "K": 2, "L": 3, "M": 2},
        ],
        85: [
            {"G": 4, "H": 3, "I": 4, "J": 1, "K": 5, "L": 2, "M": 3},
            {"G": 1, "H": 2, "I": 2, "J": 1, "K": 3, "L": 4, "M": 6},
        ],
        95: [
            {"G": 1, "H": 6, "I": 2, "J": 3, "K": 6, "L": 1, "M": 5},
            {"G": 4, "H": 5, "I": 4, "J": 2, "K": 6, "L": 1, "M": 4},
        ],
        229: [
            {"G": 3, "H": 5, "I": 1, "J": 4, "K": 3, "L": 4, "M": 4},
            {"G": 3, "H": 6, "I": 3, "J": 5, "K": 1, "L": 2, "M": 3},
        ],
        411: [
            {"G": 6, "H": 1, "I": 6, "J": 5, "K": 1, "L": 5, "M": 1},
            {"G": 6, "H": 3, "I": 6, "J": 4, "K": 5, "L": 5, "M": 1},
        ],
    }

    res = place(
        judges=judges,
        placings=placings,
        majority=4,
    )
    assert res.final_placings() == {
        80: 1,
        85: 2,
        95: 3,
        229: 4,
        411: 5,
        40: 6,
    }

    judges = ["A", "B", "C", "D"]
    placings = {
        174: [{"A": 3, "B": 1, "C": 4, "D": 3}, {"A": 4, "B": 1, "C": 4, "D": 5}],
        182: [{"A": 6, "B": 5, "C": 6, "D": 4}, {"A": 5, "B": 5, "C": 5, "D": 3}],
        184: [{"A": 2, "B": 2, "C": 3, "D": 6}, {"A": 2, "B": 4, "C": 2, "D": 6}],
        186: [{"A": 1, "B": 3, "C": 1, "D": 2}, {"A": 1, "B": 3, "C": 1, "D": 1}],
        202: [{"A": 4, "B": 6, "C": 5, "D": 5}, {"A": 6, "B": 6, "C": 6, "D": 4}],
        275: [{"A": 5, "B": 4, "C": 2, "D": 1}, {"A": 3, "B": 2, "C": 3, "D": 2}],
    }

    res = place(
        judges=judges,
        placings=placings,
        majority=3,
    )

    assert res.final_placings() == {
        186: 1,
        275: 2,
        174: 3,
        184: 4,
        182: 5,
        202: 6,
    }

    judges = ["A", "B", "C", "D"]
    placings = {
        30: [{"A": 4, "B": 5, "C": 5, "D": 6}, {"A": 4, "B": 5, "C": 4, "D": 6}],
        31: [{"A": 2, "B": 1, "C": 1, "D": 1}, {"A": 3, "B": 1, "C": 1, "D": 1}],
        50: [{"A": 6, "B": 4, "C": 6, "D": 5}, {"A": 6, "B": 4, "C": 6, "D": 4}],
        58: [{"A": 5, "B": 6, "C": 2, "D": 4}, {"A": 5, "B": 6, "C": 3, "D": 5}],
        69: [{"A": 1, "B": 3, "C": 3, "D": 3}, {"A": 2, "B": 3, "C": 5, "D": 3}],
        96: [{"A": 3, "B": 2, "C": 4, "D": 2}, {"A": 1, "B": 2, "C": 2, "D": 2}],
    }

    res = place(
        judges=judges,
        placings=placings,
    )
    assert res.final_placings() == {
        31: 1,
        96: 2,
        69: 3,
        58: 4,
        30: 5,
        50: 6,
    }


def test_split_by_rank():
    assert split_by_rank({1: 2, 2: 2, 3: 5}) == [
        {
            1: 2,
            2: 2,
        },
        {3: 5},
    ]


def test_calculate_next_to_place():
    assert calculate_next_to_place([1, 2, 3, 4, 5, 6], 6) == []
    assert calculate_next_to_place([1, 2, 3, 4, 5], 6) == [6]
    assert calculate_next_to_place([1, 2, 3, 4, 5], 7) == [6, 7]
    assert calculate_next_to_place([1, 3, 4, 5], 6) == [2]
    assert calculate_next_to_place([3, 4, 5], 6) == [1, 2]
    assert calculate_next_to_place([], 6) == [1, 2, 3, 4, 5, 6]
    assert calculate_next_to_place([1, 2, 3, 4], 6) == [5, 6]


def test_placed_n_or_higher():
    assert placed_n_or_higher(
        [{1: 1, 2: 3}, {1: 2, 2: 1}],
        2,
    ) == {1: 2, 2: 1}


def test_filter_placings():
    assert filter_placings(
        [{1: 2, 2: 1}, {1: 1, 2: 2}],
        {1},
    ) == [{1: 2}, {1: 1}]


def test_combine_couples_dances():
    assert combine_couples_dances(
        [{"A": 1, "B": 2, "C": 3}, {"A": 3, "B": 2, "C": 1}]
    ) == {
        "1A": 1,
        "1B": 2,
        "1C": 3,
        "2A": 3,
        "2B": 2,
        "2C": 1,
    }


def test_combine_dances():
    assert combine_dances(
        {
            55: [{"A": 1, "B": 2, "C": 3}, {"A": 3, "B": 2, "C": 1}],
            66: [{"A": 3, "B": 2, "C": 1}, {"A": 1, "B": 2, "C": 3}],
        }
    ) == {
        55: {
            "1A": 1,
            "1B": 2,
            "1C": 3,
            "2A": 3,
            "2B": 2,
            "2C": 1,
        },
        66: {
            "1A": 3,
            "1B": 2,
            "1C": 1,
            "2A": 1,
            "2B": 2,
            "2C": 3,
        },
    }


def test_increment_placings():
    assert increment_placings(
        {1: 1, 2: 2, 3: 3},
        2,
    ) == {1: 2, 2: 3, 3: 4}


def test_calculate_average():
    assert calculate_average(1, [1, 2]) == 1.5
    assert calculate_average(1, [1, 2, 3]) == 2
    assert calculate_average(1, [1, 2, 3, 4]) == 2.5
    assert calculate_average(1, [1, 2, 3, 4, 5]) == 3
    assert calculate_average(2, [1, 2]) == 2.5


def test_transform_placings():
    assert transform_placings({1: [[2, 3], [3, 2]]}, ["A", "B"]) == {
        1: [{"A": 2, "B": 3}, {"A": 3, "B": 2}]
    }
