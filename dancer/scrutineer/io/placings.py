"""Read / write placings (txt file).
"""


import logging
import re
from pydantic import FilePath, BaseModel
from dancer.scrutineer.place import place, Final

LOG = logging.getLogger(__name__)


class PlacingsFile(BaseModel):
    """Handle placings from a txt file."""

    filename: FilePath
    judges: list[str] = []
    content: dict[int, list[dict[str, float]]] = {}

    def _read_file(self) -> None:
        """Read the contents in the filename."""

        lines = self.filename.open("r", encoding="utf-8").readlines()

        first_line = lines[0]
        regexp = re.compile("[A-Z]+")
        if regexp.match(first_line):
            # first line contains judges
            self.judges = [char for char in first_line.strip()]
        else:
            # first line contains marks
            _, marks = first_line.split()
            self.judges = [chr(i) for i in range(ord("A"), ord("A") + len(marks))]

        for line in lines:
            if not line.split():
                LOG.info("Moving to next dance")
                continue
            couple, marks = line.split()
            couple = int(couple)
            marks = [float(char) for char in marks]

            this = {
                judge: mark for judge, mark in zip(self.judges, marks)
            }

            if (existing := self.content.get(couple)):
                existing.append(this)
            else:
                existing = []
                existing.append(this)
                self.content[couple] = existing

    def read(self) -> dict[int, list[dict[str, float]]]:
        """Read txt file containing placings awarded by judges."""

        if not self.content:
            self._read_file()

        return self.content

    def validate(self) -> bool:
        """Validate whether the marks read from the file make sense i.e.
        each dance has 1-n marked from each judge.
        """

        if not self.content:
            self._read_file()

        couples = [val for _, val in self.content.items()]
        num_dances = len(couples[0])

        for i in range(num_dances):
            # get all of the placings for each judge in each dance
            for judge in self.judges:
                marks = {couple[i].get(judge, 0) for couple in couples}
                event = [couple[i] for couple in couples]
                print(event)
                print(judge)
                if 0 in marks:
                    LOG.error(
                        "Missing a mark in dance %d! %s",
                        i + 1,
                        marks
                    )
                    return False
                if len(marks) != len(couples):
                    LOG.error(
                        "%d different marks awarded to %d couples in dance %d!",
                        len(marks),
                        len(couples),
                        i + 1,
                    )
                    return False

        return True

    def place(self) -> Final:
        """Place the couples in this file."""

        self.validate()

        return place(
            judges=self.judges,
            placings=self.read(),
        )
