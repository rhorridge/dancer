"""Implement placing of couples in final dances.
"""

import logging
import math
import operator
import collections
import functools
from typing import Optional
from enum import Enum
from pydantic import BaseModel, validate_arguments
from dancer.competition import Final

LOG = logging.getLogger(__name__)


def calculate_majority(judges: list[str]) -> int:
    """Return the majority required from the judges here."""

    res = math.ceil(len(judges) / 2)
    if res == len(judges) / 2:
        res += 1
    return res


def count_place(n: int, place: dict[str, int]) -> int:
    """Count the number of placings at `n` or higher for this couple."""

    return len([item for item in place.values() if item <= n])


def sum_place(n: int, place: dict[str, int]) -> int:
    """Sum the number of placings at `n` or higher for this couple."""

    return sum([item for item in place.values() if item <= n])


def sum_n_or_higher(
    placings: dict[int, dict[str, int]],
    n: int,
) -> dict[int, int]:
    """Return the sum of 'n or higher' placings  for all couples."""

    return {key: sum_place(n, val) for key, val in placings.items()}


def n_or_higher(
    placings: dict[int, dict[str, int]],
    n: int,
) -> dict[int, int]:
    """Return the number of judges that have placed couples N or higher in this
    dance.

    e.g. {
        5: 1
    } indicates 1 judge has placed couple 5 N or higher.

    TODO cache intermediate results
    """

    res = {key: count_place(n, val) for key, val in placings.items()}
    return res


def placed_n_or_higher(
    placings: list[dict[int, int]],
    n: int,
) -> dict[int, int]:
    """Return the number of 'n or higher' placings in the dances for each couple.

    Dances are represented as a list of dictionaries, each dictionary represents
    the place of each key (couple) in that dance.
    """

    retval = {}
    for dance in placings:
        for key, val in dance.items():
            if not retval.get(key):
                retval[key] = 0
            if val <= n:
                retval[key] += 1

    return retval


def summed_n_or_higher(
    placings: list[dict[int, int]],
    n: int,
) -> dict[int, int]:
    """Return the sum of 'n or higher' placings in the dances for each couple.

    Dances are represented as a list of dictionaries, each dictionary represents
    the place of each key (couple) in that dance.
    """

    retval = {}
    for dance in placings:
        for key, val in dance.items():
            if not retval.get(key):
                retval[key] = 0
            if val <= n:
                retval[key] += val

    return retval


def one_with_lowest(summed_placings: dict[int, int]) -> list[int]:
    """Return the keys corresponding to the lowest scores."""

    minval = min(summed_placings.values())
    return [key for key, val in summed_placings.items() if val == minval]


def one_with_highest(summed_majorities: dict[int, int]) -> list[int]:
    """Return the keys corresponding to the highest majorities."""

    maxval = max(summed_majorities.values())
    return [key for key, val in summed_majorities.items() if val == maxval]


class Rule5Error(int, Enum):
    """Error under rule 5."""

    SUCCESS = 0
    NO_COUPLE_HAS_MAJORITY = 1
    MORE_THAN_ONE_COUPLE_HAS_SAME_MAJORITY = 2


class Rule5Result(BaseModel):
    ok: Optional[int] = None
    err: Rule5Error = Rule5Error.SUCCESS
    extra: dict = {}


def rule_5_calculations(
    *,
    placings: dict[int, dict[str, int]],
    majority: int,
    place: int,
    already_placed: set[int],
    consider_n_or_higher: Optional[int] = None,
) -> Rule5Result:
    """Place couples under rule 5 for a specific place."""

    LOG.debug("Place %d, already placed: %s", place, already_placed)

    if not consider_n_or_higher:
        consider_n_or_higher = place

    if len(already_placed) > place:
        LOG.warning("Already placed all %d couples!", len(already_placed))
        return Rule5Result()

    n_higher = n_or_higher(placings, consider_n_or_higher)
    majorities = {
        key: val
        for key, val in n_higher.items()
        if val >= majority and key not in already_placed
    }
    if len(majorities) == 1:
        key = list(majorities.keys())[0]
        return Rule5Result(ok=key)

    LOG.debug("More than one couple, need majority of %d", majority)
    if len(majorities) > 1:
        highest = one_with_highest(majorities)
        LOG.debug(
            "Highest majority for place %d (placing for %d): %s %s",
            consider_n_or_higher,
            place,
            highest,
            majorities,
        )
        if len(highest) == 1:
            return Rule5Result(ok=highest[0])
        return Rule5Result(
            err=Rule5Error.MORE_THAN_ONE_COUPLE_HAS_SAME_MAJORITY,
            extra=majorities,
        )
    LOG.debug("No couple has majority for place %d: %s", place, majorities)
    return Rule5Result(
        err=Rule5Error.NO_COUPLE_HAS_MAJORITY,
        extra={
            key: val for key, val in placings.items() if key in set(n_higher.keys())
        },
    )


class Rule8Error(int, Enum):
    """Errors occurring during rule 8."""

    SUCCESS = 0
    TIED_ON_SUM = 3


class Rule8Result(BaseModel):
    ok: Optional[dict[int, int]] = None
    err: Rule8Error = Rule8Error.SUCCESS
    extra: dict[int, dict[str, int]] = {}


def rule_8_calculations(
    *,
    placings: dict[int, dict[str, int]],
    majorities: dict[int, int],
    place: int,
    already_placed: set[int],
    consider_n_or_higher: Optional[int] = None,
) -> Rule8Result:
    """Place couples under rule 8 for a specific place."""

    LOG.debug("%d %s", place, consider_n_or_higher)

    if not consider_n_or_higher:
        consider_n_or_higher = place

    couples = set(majorities.keys())
    rule_8_couples = {key: val for key, val in placings.items() if key in couples}
    rule_8_calcs = sum_n_or_higher(rule_8_couples, consider_n_or_higher)
    LOG.debug("Rule 8 calcs: %s", rule_8_calcs)
    lowest = one_with_lowest(rule_8_calcs)
    LOG.debug("Lowest: %s", lowest)
    result = {}
    if len(lowest) == 1:
        result[lowest[0]] = place
        place += 1
        majorities.pop(lowest[0])
        if len(majorities) > 1:
            return Rule8Result(
                ok=result,
                err=Rule8Error.TIED_ON_SUM,
                extra={key: placings.get(key) for key in majorities.keys()},
            )

        result[list(majorities.keys())[0]] = place
        return Rule8Result(ok=result)
    return Rule8Result(
        err=Rule8Error.TIED_ON_SUM,
        extra={key: placings.get(key) for key in lowest},
    )


def calculate_average(place: int, couples: list[int]) -> float:
    """Return the average place for tied couples that are being placed at `place`."""

    # 1 2 -> 1.5
    # 1 3 -> 2
    # 1 4 -> 2.5

    return place + (len(couples) - 1) / 2


def place_dance(
    *,
    judges: list[str],
    placings: dict[int, dict[str, int]],
    majority: int,
    # This should only be set for Rule 11!
    placing_for: Optional[int] = None,
) -> dict[int, int]:
    """Place all couples in a single dance ."""

    if not placing_for:
        placing_for = len(placings)
    num_placings = len(placings)
    LOG.debug("Majority %d, placing %d", majority, num_placings)
    placed = set()
    final_placings = {}
    n = 0
    i = None
    while len(placed) < num_placings:
        if n <= len(placed):
            n = n + 1
            continue

        if n == 2 and len(placings) == 2 and len(placed) == 1:
            # Handles the edge-cases of rule 11
            LOG.debug("Only one couple left to place!")
            to_place = {key: val for key, val in placings.items() if key not in placed}
            couple = list(to_place.keys())[0]
            final_placings[couple] = n
            placed.add(couple)
            continue

        LOG.info("Attempting to place for place %d", n)
        res = rule_5_calculations(
            placings=placings,
            majority=majority,
            place=n,
            already_placed=placed,
        )
        if res.ok:
            place = n
            while place in set(final_placings.values()):
                place += 1
            final_placings[res.ok] = place
            placed.add(res.ok)
            LOG.debug("Placed couple %3d in position %d through rule 5", res.ok, place)
            continue
        i = n
        while res.err:
            LOG.debug("Attempting place: %1d (%1d or higher) with %s", n, i, res)
            if res.err == Rule5Error.MORE_THAN_ONE_COUPLE_HAS_SAME_MAJORITY:
                LOG.info("Same majority for place: %d (%d or higher)", n, i)
                res = rule_8_calculations(
                    placings=placings,
                    majorities=res.extra,
                    place=n,
                    already_placed=placed,
                    consider_n_or_higher=i,
                )
                if res.ok:
                    LOG.warning(res)
                    for couple, place in res.ok.items():
                        while place in set(final_placings.values()):
                            place += 1
                        final_placings[couple] = place
                        placed.add(couple)
                        LOG.debug(
                            "Placed couple %3d in position %d through rule 8",
                            couple,
                            place,
                        )
                    break

            elif res.err == Rule5Error.NO_COUPLE_HAS_MAJORITY:
                if i < placing_for:
                    LOG.debug("incrementing place %d to %d", i, i + 1)
                    i = i + 1
                res = rule_5_calculations(
                    placings=res.extra,
                    majority=majority,
                    place=n,
                    already_placed=placed,
                    consider_n_or_higher=i,
                )
                if res.ok:
                    LOG.debug("To place: %d", n)
                    place = n
                    while place in set(final_placings.values()):
                        place += 1
                    final_placings[res.ok] = place
                    placed.add(res.ok)
                    LOG.debug(
                        "Placed couple %3d in position %d through rule 5",
                        res.ok,
                        place,
                    )
                    break
            elif res.err == Rule8Error.TIED_ON_SUM:
                res = rule_5_calculations(
                    placings=res.extra,
                    majority=majority,
                    place=n,
                    already_placed=placed,
                    consider_n_or_higher=i,
                )
                LOG.warning(res)

                if res.ok:
                    place = n
                    while place in set(final_placings.values()):
                        place += 1
                    final_placings[res.ok] = place
                    placed.add(res.ok)
                    LOG.debug(
                        "Placed couple %3d in position %d through rule 5",
                        res.ok,
                        place,
                    )
                    break

                if (
                    i >= num_placings
                    and res.err == Rule5Error.MORE_THAN_ONE_COUPLE_HAS_SAME_MAJORITY
                ):
                    LOG.debug("%s %s", res, final_placings)
                    LOG.error("Could not resolve tie for place %d", n)
                    avg_place = calculate_average(
                        n,
                        res.extra.keys(),
                    )

                    avg_place = calculate_average(
                        n,
                        res.extra.keys(),
                    )
                    for num in res.extra.keys():
                        final_placings[num] = avg_place
                        placed.add(num)
                        LOG.debug(
                            "Placed couple %3d in position %d - tied with %d other couples",
                            num,
                            avg_place,
                            len(res.extra) - 1,
                        )
                    n = n + len(res.extra) - 1
                    res = Rule5Result()
                    break

                if res.err == Rule5Error.MORE_THAN_ONE_COUPLE_HAS_SAME_MAJORITY:
                    LOG.debug(
                        "More than one couple has same majority for n or higher %1d (placing for %1d)",
                        i,
                        n,
                    )
                    # TODO i think this is where i need to fix it
                    # TODO i think this is where i need to fix it
                    i = i + 1
                    # Hack but it works?
                    res = rule_5_calculations(
                        placings={
                            key: val
                            for key, val in placings.items()
                            if key in res.extra
                        },
                        majority=majority,
                        place=n,
                        already_placed=placed,
                        consider_n_or_higher=i,
                    )
                    if res.ok:
                        place = n
                        while place in set(final_placings.values()):
                            place += 1
                        final_placings[res.ok] = place
                        placed.add(res.ok)
                        LOG.debug(
                            "Placed couple %3d in position %d through rule 5",
                            res.ok,
                            place,
                        )

                continue

            else:
                LOG.error("Unknown, unresolvable error! %s", res.err)
                raise RuntimeError("Unknown error")
    LOG.info("Final placings: %s", final_placings)
    return final_placings


def convert_placings_to_list(
    placings: dict[int, list[dict[str, int]]]
) -> list[dict[int, dict[str, int]]]:
    """Convert placings from couple: [dances] to [dance: couples] order."""

    num_dances = len(list(placings.values())[0])
    retval = []
    for i in range(num_dances):
        dic = {}
        for key, val in placings.items():
            dic[key] = val[i]
        retval.append(dic)
    return retval


class Rule9Error(int, Enum):
    SUCCESS = 0
    EQUAL_PLACINGS_GIVEN = 4


class Rule9Result(BaseModel):
    ok: Optional[dict[int, int]] = None
    err: Rule9Error = Rule9Error.SUCCESS
    extra: dict[int, int] = {}


def rank_by_value(
    placings: dict[int, int], skip_ranks: Optional[list] = None
) -> dict[int, int]:
    """Assign ranks to all values in placings. If `skip_ranks` is set,
    ignore the ranks that are passed.

    WARNING: This does not attempt to check for ties! They must be
    resolved separately.
    """

    if not skip_ranks:
        skip_ranks = []
    skip = set(skip_ranks)
    dic = dict(sorted(placings.items(), key=lambda x: x[1]))
    place = 1
    for key, val in dic.items():
        while place in skip:
            place += 1
        dic[key] = place
        place += 1
    return dic


def find_equal_values(placings: dict[int, int]) -> dict[int, int]:
    """Find any key: val pairs in `placings` that are equal i.e. have
    the same score as another pair.
    """

    already_seen = {}
    for key, val in placings.items():
        if val not in already_seen:
            already_seen[val] = set()
        already_seen[val].add(key)

    retval = {}
    for key, val in already_seen.items():
        if len(val) > 1:
            for itm in val:
                retval[itm] = key
    return retval


def rule_9_calculations(placings: list[dict[int, int]]) -> dict[int, int]:
    """Perform rule 9 calculations to combine these dances."""

    summed = dict(
        functools.reduce(
            operator.add,
            map(
                collections.Counter,
                placings,
            ),
        )
    )

    if len(set(list(summed.values()))) == len(placings[0]):
        return Rule9Result(ok=rank_by_value(summed))

    # Needs Rule 10 calculations
    missing_placings = find_equal_values(summed)
    missing = set(missing_placings.keys())
    tmp_placings = rank_by_value(summed)
    completed_placings = {
        key: val for key, val in tmp_placings.items() if key not in missing
    }
    return Rule9Result(
        ok=completed_placings,
        err=Rule9Error.EQUAL_PLACINGS_GIVEN,
        extra=missing_placings,
    )


class Rule10Error(int, Enum):
    SUCCESS = 0
    UNRESOLVABLE_TIE = 5


class Rule10Result(BaseModel):
    ok: Optional[dict[int, int]] = None
    err: Rule10Error = Rule10Error.SUCCESS
    extra: dict[int, int] = {}


def split_by_rank(tied: dict[int, int]) -> list[dict[int, int]]:
    """Split tied positions by their rank, in order from lowest to highest."""

    ranks = sorted(list(set(tied.values())))
    retval = [{} for rank in ranks]
    for rank, item in zip(ranks, retval):
        for key, val in tied.items():
            if val == rank:
                item[key] = val
    return retval


def calculate_next_to_place(already_placed: list[int], total_places: int):
    """Calculate the next positions to place, based on the missing positions."""

    prev = 0
    to_place = []
    for place in already_placed:
        diff = place - prev
        if diff > 1:
            while place - 1 > prev:
                prev += 1
                to_place.append(prev)
            return to_place
        prev = place
    if prev < total_places:
        while prev < total_places:
            prev += 1
            to_place.append(prev)
    return sorted(to_place)


def filter_placings(placings: list[dict[int, int]], couples: set[int]):
    """Filter placings to give only the placings of couples in `couples`."""

    return [
        {key: val for key, val in dance.items() if key in couples} for dance in placings
    ]


def rule_10_calculations(
    *,
    judges: list[str],
    placings: list[dict[int, float]],
    majority: int,
    tied: dict[int, float],
    positions_placed: list[int],
) -> Rule10Result:
    """Resolve `tied` placings using Rule 10."""

    # split the tied positions into groups based on their tie
    to_resolve = split_by_rank(tied)
    total_places = len(placings[0])

    tie = to_resolve[0]

    to_place = calculate_next_to_place(sorted(positions_placed), total_places)
    couples_to_place = set(tie.keys())

    # If there are not enough places available for this tie, we are in trouble
    if len(to_place) < len(tie):
        raise RuntimeError(
            "Number of places available %d is less than number of couples %d!"
            % (
                len(to_place),
                len(tie),
            )
        )

    next_place = to_place[0]
    rank_of_placings = placed_n_or_higher(
        filter_placings(
            placings,
            couples_to_place,
        ),
        next_place,
    )
    top = one_with_highest(rank_of_placings)
    if len(top) == 1:
        return Rule10Result(ok={top[0]: next_place})
    if len(top) > 1:
        sum_of_placings = summed_n_or_higher(
            filter_placings(
                placings,
                set(top),
            ),
            next_place,
        )
        top = one_with_lowest(sum_of_placings)
        if len(top) == 1:
            return Rule10Result(ok={top[0]: next_place})
        if len(top) > 1:
            LOG.warning("Unresolvable tie under rule 10: %s", sum_of_placings)

        if len(top) < len(couples_to_place):
            LOG.debug(
                "%d couple(s) can still be placed under rule 10",
                len(couples_to_place) - len(top),
            )
        unplaced = couples_to_place - set(top)
        to_place = {
            key: val for key, val in rank_of_placings.items() if key in unplaced
        }
        if len(to_place) == 1:
            place = next_place + len(top)
            couple = list(to_place.keys())[0]
            LOG.info(
                "Placed one remaining couple %d in the lowest place available: %d",
                couple,
                place,
            )
            return Rule10Result(
                ok={couple: place},
                err=Rule10Error.UNRESOLVABLE_TIE,
                extra=sum_of_placings,
            )
        return Rule10Result(
            err=Rule10Error.UNRESOLVABLE_TIE,
            extra=sum_of_placings,
        )


def combine_couples_dances(dances: list[dict[str, int]]) -> dict[str, int]:
    """Combine all dances in a list into a single dance."""

    retval = {}
    for i, dance in enumerate(dances):
        for key, val in dance.items():
            retval[f"{i+1}{key}"] = val
    return retval


def combine_dances(
    placings: dict[int, list[dict[str, int]]]
) -> dict[int, dict[str, int]]:
    """Combine the dances into a single dance, for the purpose of applying Rule 11."""

    retval = {}
    for key, val in placings.items():
        retval[key] = combine_couples_dances(val)
    return retval


class Rule11Error(int, Enum):
    SUCCESS = 0
    UNRESOLVABLE_TIE = 6


class Rule11Result(BaseModel):
    ok: Optional[dict[int, int]] = None
    err: Rule11Error = Rule11Error.SUCCESS
    extra: dict[int, int] = {}


def increment_placings(placings: dict[int, int], highest_place: int):
    """Increment all placings so that the highest position is `highest_place`."""

    return {key: val + highest_place - 1 for key, val in placings.items()}


def rule_11_calculations(
    *,
    judges: list[str],
    placings: dict[int, list[dict[str, int]]],
    majority: int,
    tied: dict[int, int],
    positions_placed: list[int],
) -> Rule11Result:
    """Place tied couples under rule 11, by marking all dances as a single dance.

    May result in a tie under Rule 11.
    """

    to_be_placed = set(tied.keys())

    total_places = len(placings)
    to_place = calculate_next_to_place(sorted(positions_placed), total_places)
    if len(to_place) < len(tied):
        raise RuntimeError(
            "Number of places available %d is less than number of couples %d!"
            % (
                len(to_place),
                len(tied),
            )
        )

    LOG.error(total_places)
    LOG.error(to_place)
    placings = {key: val for key, val in placings.items() if key in to_be_placed}
    placings = combine_dances(placings)
    LOG.error(placings)

    max_placed = max(positions_placed) if positions_placed else 0
    overall_placings = place_dance(
        judges=judges,
        placings=placings,
        majority=majority,
        # Highest place to check - for doing the incrementing for Rule 8 calcs
        placing_for=max_placed + len(placings),
    )
    overall_placings = increment_placings(overall_placings, to_place[0])
    return Rule11Result(ok=overall_placings)


@validate_arguments
def place(
    *,
    judges: list[str],
    placings: dict[int, list[dict[str, float]]],
    majority: Optional[int] = None,
) -> Final:
    """Place the couples according to the majority opinion, according to
    rules 5 - 11 in the Skating System.

    Rule 5: The winning couple is the one placed first by the majority of judges.
    """

    def make_result(result) -> Final:
        return Final(
            couples={
                num: {
                    "placings": val,
                    "final_place": result.get(num),
                }
                for num, val in placings.items()
            }
        )

    if not majority:
        majority = calculate_majority(judges)
    num_places = len(placings)
    dance_placings = convert_placings_to_list(placings)
    num_dances = len(dance_placings)
    overall_placings = [
        place_dance(
            judges=judges,
            placings=plce,
            majority=majority,
        )
        for plce in dance_placings
    ]
    res = rule_9_calculations(overall_placings)
    if res.err == Rule9Error.SUCCESS:
        return make_result(res.ok)

    unplaced = res.extra

    successfully_placed = {}
    while len(successfully_placed) < num_places:
        if res.ok:
            successfully_placed.update(res.ok)
        if res.extra:
            tied = res.extra
        else:
            tied = {
                key: val
                for key, val in unplaced.items()
                if key not in set(successfully_placed.keys())
            }
        res = rule_10_calculations(
            judges=judges,
            placings=overall_placings,
            majority=calculate_majority(overall_placings),
            tied=tied,
            positions_placed=list(successfully_placed.values()),
        )
        if res.err == Rule10Error.SUCCESS:
            successfully_placed.update(res.ok)
        elif res.err == Rule10Error.UNRESOLVABLE_TIE:
            if res.ok:
                LOG.info(
                    "Unresolvable tie under Rule 10, successfully placed: %s",
                    res.ok,
                )
                successfully_placed.update(res.ok)
            res = rule_11_calculations(
                judges=judges,
                placings=placings,
                majority=calculate_majority(num_dances * judges),
                tied=res.extra,
                positions_placed=list(successfully_placed.values()),
            )
            if res.ok:
                LOG.info(
                    "Resolved tie using Rule 11, successfully placed: %s",
                    res.ok,
                )
                successfully_placed.update(res.ok)
    return make_result(successfully_placed)


def transform_placing(placing: list[list[int]], judges: list[str]) -> dict[str, int]:
    """Transform placing for an individual dance."""

    return [{judge: place for judge, place in zip(judges, event)} for event in placing]


def transform_placings(
    placings: dict[int, list[list[int]]], judges: list[str]
) -> dict[int, dict[str, int]]:
    """Transform lists of placings for each dance into corresponding judges'
    placements.
    """

    res = {key: transform_placing(val, judges) for key, val in placings.items()}
    return res
