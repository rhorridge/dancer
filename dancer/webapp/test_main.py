from fastapi.testclient import TestClient
from .__main__ import app

client = TestClient(app)


def test_get_syllabus():
    res = client.get("/syllabus/waltz")
    assert res.status_code == 200
    assert res.json()["alt_name"] == "Slow Waltz"
    assert len(res.json()["figures"]) == 36
