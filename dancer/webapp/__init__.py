from fastapi import FastAPI, HTTPException
from fastapi.testclient import TestClient
from pydantic import ValidationError
from dancer.syllabus import Syllabus
from dancer.database.syllabus import get_syllabus, valid_options
from dancer.competition_events import ALL_EVENTS


app = FastAPI()


@app.get("/syllabus/{dance}")
async def _syllabus(dance: str):
    syllabus = get_syllabus(dance)
    if not syllabus:
        raise HTTPException(
            status_code=404,
            detail="unknown option passed: %s, valid options: %s"
            % (dance, valid_options()),
        )
    return syllabus


@app.get("/competition/events")
async def _competition_events():
    """ "Return all competition events."""

    return ALL_EVENTS


# TODO competition API
# TODO couple API
# TODO dance API


@app.get("/competition/easycomp/{competition_name}")
async def _competition_easycomp_competition_name(competition_name: str):
    """Fetch a competition."""

    # TODO
    pass
