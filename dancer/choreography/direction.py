"""Exports various common directions.
"""

from dancer.steps import Direction


Fwd = Direction.FORWARD
Bwd = Direction.BACKWARD
Either = Direction.FORWARD_OR_BACKWARD
AnyDirection = Direction.ANY

MoveRight = Direction.RIGHT
MoveLeft = Direction.LEFT

LatinCrossBwd = Direction.LATIN_CROSS
LatinCrossFwd = Direction.LATIN_CROSS_FWD

Flick = Direction.FLICK
Appel = Direction.APPEL
SlipAppel = Direction.SLIP_APPEL
