"""Unit tests for .feet.
"""

from . import feet


def test_foot_samefoot():
    """Assert that the samefoot method works correctly."""

    assert feet.RFFwd.samefoot(feet.RFFwd) is True
    assert feet.RFFwd.samefoot(feet.RFBwd) is False
    assert feet.RFFwd.samefoot(feet.LFFwd) is False
    assert feet.RFFwd.samefoot(feet.LFBwd) is False
    assert feet.RFFwd.samefoot(feet.RFEither) is True
    assert feet.RFFwd.samefoot(feet.LFEither) is False
    assert feet.RFBwd.samefoot(feet.RFBwd) is True
    assert feet.RFBwd.samefoot(feet.LFFwd) is False
    assert feet.RFBwd.samefoot(feet.LFBwd) is False
    assert feet.RFBwd.samefoot(feet.RFEither) is True
    assert feet.RFBwd.samefoot(feet.LFEither) is False
    assert feet.LFFwd.samefoot(feet.LFFwd) is True
    assert feet.LFFwd.samefoot(feet.LFBwd) is False
    assert feet.LFFwd.samefoot(feet.RFEither) is False
    assert feet.LFFwd.samefoot(feet.LFEither) is True
    assert feet.LFBwd.samefoot(feet.LFBwd) is True
    assert feet.LFBwd.samefoot(feet.RFEither) is False
    assert feet.LFBwd.samefoot(feet.LFEither) is True
    assert feet.RFEither.samefoot(feet.RFEither) is True
    assert feet.RFEither.samefoot(feet.LFEither) is False
    assert feet.LFEither.samefoot(feet.LFEither) is True
