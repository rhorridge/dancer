"""Exports different foot positions.
"""

from dancer.steps import Foot
from .direction import (
    Fwd,
    Bwd,
    Either,
    AnyDirection,
    MoveRight,
    MoveLeft,
    LatinCrossBwd,
    LatinCrossFwd,
    Flick,
    Appel,
    SlipAppel,
)


RFFwd = Foot(direction=Fwd, foot="R")
RFBwd = Foot(direction=Bwd, foot="R")
LFFwd = Foot(direction=Fwd, foot="L")
LFBwd = Foot(direction=Bwd, foot="L")
RFEither = Foot(direction=Either, foot="R")
LFEither = Foot(direction=Either, foot="L")
# Closing RF to LF
RFClose = Foot(direction=Either, foot="R")
# Closing LF to RF
LFClose = Foot(direction=Either, foot="L")
# Any direction with RF
RFAny = Foot(direction=AnyDirection, foot="R")
# Any direction with LF
LFAny = Foot(direction=AnyDirection, foot="L")

# Right with RF
RFRight = Foot(direction=MoveRight, foot="R")
# Left with LF
LFLeft = Foot(direction=MoveLeft, foot="L")

# Right foot behind left foot
RFLatinCrossBwd = Foot(direction=LatinCrossBwd, foot="R")
# Left foot behind right foot
LFLatinCrossBwd = Foot(direction=LatinCrossBwd, foot="L")

# Right foot in front of left foot
RFLatinCrossFwd = Foot(direction=LatinCrossFwd, foot="R")
# Left foot in front of right foot
LFLatinCrossFwd = Foot(direction=LatinCrossFwd, foot="L")

# Right foot flick forward
RFFlick = Foot(direction=Flick, foot="R")
# Left foot in front of right foot
LFFlick = Foot(direction=Flick, foot="L")

# Right Foot Appel - lowering of weight
RFAppel = Foot(direction=Appel, foot="R")
# Right Foot Slip Appel - lowering of weight and turning
RFSlipAppel = Foot(direction=SlipAppel, foot="R")
