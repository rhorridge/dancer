"""Exports figures that are typically from the Rumba syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    LFAny,
    LFClose,
    LFLeft,
    RFRight,
    RFBwd,
    RFAny,
    RFClose,
    OpenPosition,
    FanPosition,
    RightShadowPosition,
)


AlternativeBasicL = Figure(
    name="Alternative Basic - L",
    start_foot=LFClose,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

AlternativeBasicR = Figure(
    name="Alternative Basic - R",
    start_foot=RFClose,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

ForwardWalk = Figure(
    name="Forward Walk",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

BackwardWalk = Figure(
    name="Backward Walk",
    start_foot=RFBwd,
    next_foot=RFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

CucarachasL = Figure(
    name="Cucarachas - L",
    start_foot=LFLeft,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

CucarachasR = Figure(
    name="Cucarachas - R",
    start_foot=RFRight,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

CubanRocksL = Figure(
    name="Cuban Rocks - L",
    start_foot=LFLeft,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

CubanRocksR = Figure(
    name="Cuban Rocks - R",
    start_foot=RFRight,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

OpeningOutToRight = Figure(
    name="Opening Out to Right",
    start_foot=LFLeft,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

OpeningOutToLeft = Figure(
    name="Opening Out to Left",
    start_foot=RFRight,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

# TODO natural opening out movement?
# TODO spiral turns?
# TODO The Spiral
# TODO Spiral with different endings

# TODO figures turned to Open CPP
# TODO Alemana checked to Open CPP
# TODO advanced opening out movement

SlidingDoors = Figure(
    name="Sliding Doors",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=FanPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.2,
)

Fencing = Figure(
    name="Fencing",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=FanPosition,
    # rhythm=Beat.2,
)

ThreeThrees = Figure(
    name="Three Threes",
    start_foot=LFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

# TODO Three Threes variations

ThreeAlemanas = Figure(
    name="Three Alemanas",
    start_foot=LFFwd,
    start_position=FanPosition,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

# TODO Advanced Hip Twist

ContinuousHipTwist = Figure(
    name="Continuous Hip Twist",
    start_foot=LFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

ContinuousCircularHipTwist = Figure(
    name="Continuous Circular Hip Twist",
    start_foot=LFFwd,
    next_foot=LFAny,
    next_position=FanPosition,
    # rhythm=Beat.2,
)
