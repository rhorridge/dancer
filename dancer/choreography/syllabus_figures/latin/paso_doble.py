"""Exports figures that are typically from the Paso Doble syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    LFAny,
    LFClose,
    LFBwd,
    LFLeft,
    LFLatinCrossFwd,
    LFFlick,
    RFRight,
    RFFwd,
    RFBwd,
    RFAny,
    RFClose,
    RFLatinCrossFwd,
    RFLatinCrossBwd,
    RFFlick,
    RFAppel,
    RFSlipAppel,
    ClosedPosition,
    OpenPosition,
    LeftContraPosition,
    RightContraPosition,
    RightSidePosition,
    RightShadowPosition,
    PromenadePosition,
    OpenPromenadePosition,
    OpenCounterPromenadePosition,
    InvertedPromenadePosition,
    InvertedCounterPromenadePosition,
)

BasicMovementSurPlace = Figure(
    name="Basic Movement - Sur Place",
    start_foot=RFClose,
    next_foot=RFAny,
)

BasicMovementMarchForward = Figure(
    name="Basic Movement - March - Fwd",
    start_foot=RFFwd,
    next_foot=RFAny,
)

BasicMovementMarchBackward = Figure(
    name="Basic Movement - March - Bwd",
    start_foot=RFBwd,
    next_foot=RFAny,
)

ChassesToR = Figure(
    name="Chasses to R",
    start_foot=RFRight,
    next_foot=RFAny,
)

ChassesToL = Figure(
    name="Chasses to L",
    start_foot=RFAppel,
    next_foot=RFAny,
)

Drag = Figure(
    name="Drag",
    start_foot=RFRight,
    next_foot=RFAny,
)

# TODO Deplacement

Attack = Figure(
    name="Attack",
    start_foot=RFAppel,
    next_foot=RFAny,
)

PromenadeLink = Figure(
    name="Promenade Link",
    start_foot=RFAppel,
    next_foot=RFAny,
)

PromenadeClose = Figure(
    name="Promenade Close",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=InvertedPromenadePosition,
)
# TODO promenade close

Ecart = Figure(
    name="Ecart",
    start_foot=RFAppel,
    next_foot=RFAny,
    next_position=PromenadePosition,
)

# TODO Fallaway Whisk

Huit = Figure(
    name="Huit",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=PromenadePosition,
)

Promenade = Figure(
    name="Promenade",
    start_foot=RFAppel,
    next_foot=RFAny,
)

Separation = Figure(
    name="Separation",
    start_foot=RFAppel,
    next_foot=RFAny,
)

SeparationFallawayEnding = Separation.copy()
SeparationFallawayEnding.name = f"Fallaway Ending to {Separation.name}"

Sixteen = Figure(
    name="Sixteen",
    start_foot=RFAppel,
    next_foot=RFAny,
)

PromenadeAndCounterPromenade = Figure(
    name="Promenade & Counter Promenade",
    start_foot=RFAppel,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

GrandCircle = Figure(
    name="Grand Circle",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=PromenadePosition,
)

# TODO Alternate entries to promenade position

OpenTelemark = Figure(
    name="Open Telemark (Paso Doble)",
    start_foot=RFSlipAppel,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

TwistTurn = Figure(
    name="Twist Turn",
    start_foot=RFAppel,
    next_foot=RFAny,
)

LaPasse = Figure(
    name="La Passe",
    start_foot=RFAppel,
    next_foot=RFAny,
)

Banderillas = Figure(
    name="Banderillas",
    start_foot=RFClose,
    next_foot=RFAppel,
)

FallawayReverse = Figure(
    name="Fallaway Reverse",
    start_foot=RFSlipAppel,
    next_foot=RFAny,
)

CoupDePique = Figure(
    name="Coup de Pique",
    start_foot=RFFlick,
    next_foot=RFAny,
)

CoupDePiqueRFToLF = CoupDePique.copy()
CoupDePiqueRFToLF.name = f"{CoupDePique.name} changing from RF to LF"
CoupDePiqueRFToLF.next_foot = LFAny

CoupDePiqueLFToRF = CoupDePique.copy()
CoupDePiqueLFToRF.name = f"{CoupDePique.name} changing from LF to RF"
CoupDePiqueLFToRF.start_foot = LFClose

# TODO syncopated / couplet coup de pique

LeftFootVariation = Figure(
    name="Left Foot Variation",
    start_foot=LFFwd,
    next_foot=RFAny,
)

SpanishLineInvertedCPP = Figure(
    name="Spanish Line - Inverted Counter Promenade Position",
    start_foot=RFFwd,
    next_foot=LFAny,
    start_position=PromenadePosition,
    next_position=InvertedCounterPromenadePosition,
)

SpanishLineInvertedPP = Figure(
    name="Spanish Line - Inverted Promenade Position",
    start_foot=LFFwd,
    next_foot=RFAny,
    start_position=InvertedCounterPromenadePosition,
    next_position=InvertedPromenadePosition,
)

FlamencoTapsInvertedCPP = Figure(
    name="Flamenco Taps - Inverted Counter Promenade Position",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=InvertedCounterPromenadePosition,
    next_position=InvertedCounterPromenadePosition,
)

FlamencoTapsInvertedPP = Figure(
    name="Flamenco Taps - Inverted Promenade Position",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=InvertedPromenadePosition,
    next_position=InvertedPromenadePosition,
)

SyncopatedSeparation = Figure(
    name="Syncopated Separation",
    start_foot=RFAppel,
    next_foot=LFAny,
)

# TODO foot changes

TravellingSpinsFromPP = Figure(
    name="Travelling Spins from Promenade Position",
    start_foot=RFAppel,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

TravellingSpinsFromCPP = Figure(
    name="Travelling Spins from Counter Promenade Position",
    start_foot=RFAppel,
    next_foot=RFAny,
)

Fregolina = Figure(
    name="Fregolina",
    start_foot=RFAppel,
    next_foot=RFAny,
)

Farol = Fregolina.copy()
Farol.name = "Farol"

Twists = Figure(
    name="Twists",
    start_foot=RFAppel,
    next_foot=LFAny,
)

ChasseCape = Figure(
    name="Chasse Cape",
    start_foot=RFRight,
    next_foot=LFAny,
    next_position=InvertedCounterPromenadePosition,
)

# TODO ChasseCapeIncludingOutsideTurn
