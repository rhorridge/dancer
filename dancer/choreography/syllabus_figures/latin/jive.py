"""Exports figures that are typically from the Jive syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    LFAny,
    LFClose,
    LFFlick,
    LFBwd,
    LFLeft,
    RFRight,
    ClosedPosition,
    OpenPosition,
    PromenadePosition,
)


ChasseToLeft = Figure(
    name="Chasse to Left",
    start_foot=LFLeft,
    next_foot=RFRight,
)

ChasseToRight = Figure(
    name="Chasse to Right",
    start_foot=RFRight,
    next_foot=LFAny,
)

BasicInPlace = Figure(
    name="Basic In Place",
    start_foot=LFClose,
    next_foot=LFAny,
)

FallawayRockAction = Figure(
    name="Fallaway Rock Action",
    start_foot=LFBwd,
    next_foot=LFAny,
)

# TODO is this the correct figure?
BasicInFallaway = Figure(
    name="Basic In Fallaway",
    start_foot=LFLeft,
    next_foot=LFAny,
    start_position=PromenadePosition,
)

FallawayThrowaway = Figure(
    name="Fallaway Throwaway",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=OpenPosition,
)

Link = Figure(
    name="Link",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
)

ChangeOfPlaceRightToLeft = Figure(
    name="Change of Place(s) Right to Left",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=OpenPosition,
)

ChangeOfPlaceLeftToRight = Figure(
    name="Change of Place(s) Left to Right",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

ChangeOfHandsBehindTheBack = Figure(
    name="Change of Hands Behind the Back",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

PromenadeWalks = Figure(
    name="Promenade Walks",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=PromenadePosition,
)

HipBump = Figure(
    name="Hip Bump",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

Whip = Figure(
    name="Whip",
    start_foot=LFBwd,
    next_foot=LFAny,
)

AmericanSpin = Figure(
    name="American Spin",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

StopAndGo = Figure(
    name="Stop and Go",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

Mooch = Figure(
    name="Mooch",
    start_foot=LFBwd,
    next_foot=LFAny,
)

WhipThrowaway = Figure(
    name="Whip Throwaway (Throwaway Whip)",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=OpenPosition,
)

ReverseWhip = Figure(
    name="Reverse Whip",
    start_foot=LFBwd,
    next_foot=LFAny,
)

Windmill = Figure(
    name="Windmill",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

SpanishArms = Figure(
    name="Spanish Arms",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

RollingOffTheArm = Figure(
    name="Rolling off the Arm",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

SimpleSpin = Figure(
    name="Simple Spin",
    start_foot=LFClose,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

MiamiSpecial = Figure(
    name="Miami Special",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

ChangeOfPlaceRightToLeftWithDoubleSpin = ChangeOfPlaceRightToLeft.copy()
ChangeOfPlaceRightToLeftWithDoubleSpin.name = (
    f"{ChangeOfPlaceRightToLeft.name} with Double Spin"
)

OverturnedChangeOfPlaceLeftToRight = ChangeOfPlaceLeftToRight.copy()
OverturnedChangeOfPlaceLeftToRight.name = f"Overturned {ChangeOfPlaceLeftToRight.name}"

# TODO Double Cross Whip

CurlyWhip = Figure(
    name="Curly Whip",
    start_foot=LFFwd,
    next_foot=LFAny,
)

OverturnedFallawayThrowaway = Figure(
    name="Overturned Fallaway Throwaway",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=OpenPosition,
)

# TODO variations - Hesitation, Point, Kick
BallChange = Figure(
    name="Ball Change",
    start_foot=LFFlick,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

ShoulderSpin = Figure(
    name="Shoulder Spin",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

ToeHeelSwivels = Figure(
    name="Toe Heel Swivels",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=ClosedPosition,
    next_position=PromenadePosition,
)

Chugging = Figure(
    name="Chugging",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

ChickenWalks = Figure(
    name="Chicken Walks",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
)

Catapult = Figure(
    name="Catapult",
    start_foot=LFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
)

StalkerWalksFlicksAndBreak = Figure(
    name="Stalker Walks and Flicks",
    start_foot=LFBwd,
    next_foot=LFAny,
    next_position=PromenadePosition,
)
