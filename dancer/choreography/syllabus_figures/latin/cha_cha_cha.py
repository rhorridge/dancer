"""Exports figures that are typically from the Cha-Cha-Chá syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    LFAny,
    LFClose,
    LFBwd,
    LFLeft,
    LFLatinCrossFwd,
    RFRight,
    RFFwd,
    RFBwd,
    RFAny,
    RFClose,
    RFLatinCrossBwd,
    OpenPosition,
    FanPosition,
    RightShadowPosition,
    ContactPosition,
)


BasicMovementClosedL = Figure(
    name="Basic Movement - Closed - LF",
    start_foot=LFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

BasicMovementInPlaceL = Figure(
    name="Basic Movement - In Place - LF",
    start_foot=LFClose,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

BasicMovementOpenL = Figure(
    name="Basic Movement - Open - LF",
    start_foot=LFClose,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

BasicMovementRShadowL = Figure(
    name="Basic Movement - R Shadow - LF",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.2,
)

# TODO initialise these through:
# BasicMovementClosedR = BasicMovementClosedL.natural_opposite()
BasicMovementClosedR = Figure(
    name="Basic Movement - Closed - RF",
    start_foot=RFBwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

BasicMovementInPlaceR = Figure(
    name="Basic Movement - In Place - RF",
    start_foot=RFClose,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

BasicMovementOpenR = Figure(
    name="Basic Movement - Open - RF",
    start_foot=RFClose,
    next_foot=RFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

BasicMovementRShadowR = Figure(
    name="Basic Movement - R Shadow - RF",
    start_foot=RFBwd,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.2,
)

ChaChaChaChasseToLeftClosed = Figure(
    name="Cha Cha Cha Chasse to Left - Closed",
    start_foot=LFLeft,
    next_foot=RFAny,
    # rhythm=Beat.4,
)

ChaChaChaChasseToLeftOpen = Figure(
    name="Cha Cha Cha Chasse to Left - Open",
    start_foot=LFLeft,
    next_foot=RFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

ChaChaChaChasseToLeftRShadow = Figure(
    name="Cha Cha Cha Chasse to Left - R Shadow",
    start_foot=LFLeft,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.4,
)

ChaChaChaChasseToRightClosed = Figure(
    name="Cha Cha Cha Chasse to Right - Closed",
    start_foot=RFRight,
    next_foot=LFAny,
    # rhythm=Beat.4,
)

ChaChaChaChasseToRightOpen = Figure(
    name="Cha Cha Cha Chasse to Right - Open",
    start_foot=RFRight,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

ChaChaChaChasseToRightRShadow = Figure(
    name="Cha Cha Cha Chasse to Right - R Shadow",
    start_foot=RFRight,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.4,
)

# TODO add open basic, spot turn, 3CCC, time step, cuban break for R Shadow

ChaChaChaLockForwardClosed = Figure(
    name="Cha Cha Cha Lock Forward - Closed",
    start_foot=RFFwd,
    next_foot=LFFwd,
    # rhythm=Beat.4,
)

ChaChaChaLockForwardOpen = Figure(
    name="Cha Cha Cha Lock Forward - Open",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

ChaChaChaLockBackwardClosed = Figure(
    name="Cha Cha Cha Lock Backward - Closed",
    start_foot=LFBwd,
    next_foot=RFBwd,
    # rhythm=Beat.4,
)

ChaChaChaLockBackwardOpen = Figure(
    name="Cha Cha Cha Lock Backward - Open",
    start_foot=LFBwd,
    next_foot=RFBwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

TimeStepL = Figure(
    name="Time Step - LF",
    start_foot=LFClose,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

TimeStepR = Figure(
    name="Time Step - RF",
    start_foot=RFClose,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

# TODO handle Open PP/CPP
NewYorkL = Figure(
    name="New York - Left",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

NewYorkR = Figure(
    name="New York - R",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

SpotTurnL = Figure(
    name="Spot Turn to Left",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

SpotTurnR = Figure(
    name="Spot Turn to Right",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

SwitchTurnL = Figure(
    name="Switch Turn to Left",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

SwitchTurnR = Figure(
    name="Switch Turn to Right",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

UnderarmTurnL = Figure(
    name="Underarm Turn to Left",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

UnderarmTurnR = Figure(
    name="Underarm Turn to Right",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

# TODO handle different positions better
ShoulderToShoulderL = Figure(
    name="Shoulder to Shoulder to Left Side",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

ShoulderToShoulderR = Figure(
    name="Shoulder to Shoulder to Right Side",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

HandToHandR = Figure(
    name="Hand to Hand to Right Side Position",
    start_foot=LFBwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

HandToHandL = Figure(
    name="Hand to Hand to Left Side Position",
    start_foot=RFBwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

TheeChaChaChasForward = Figure(
    name="Three Cha Cha Cha's Forward",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

TheeChaChaChasBackward = Figure(
    name="Three Cha Cha Cha's Backward",
    start_foot=LFBwd,
    next_foot=RFBwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.4,
)

SideStepToLeft = Figure(
    name="Side Step to Left",
    start_foot=LFLeft,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

SideStepToRight = Figure(
    name="Side Step to Right",
    start_foot=RFRight,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

ThereAndBack = Figure(
    name="There and Back",
    start_foot=LFClose,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

Fan = Figure(
    name="Fan",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

HockeyStick = Figure(
    name="Hockey Stick",
    start_foot=LFClose,
    next_foot=LFFwd,
    start_position=FanPosition,
    # rhythm=Beat.2,
)

Alemana = Figure(
    name="Alemana",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=FanPosition,
    # rhythm=Beat.2,
)

# TODO handle different hand holds
AlemanaFromOpenPosition = Figure(
    name="Alemana from Open Position",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    # rhythm=Beat.2,
)

# TODO add alternate ending alemanas

NaturalTop = Figure(
    name="Natural Top",
    start_foot=RFLatinCrossBwd,
    next_foot=LFFwd,
    # rhythm=Beat.2,
)

# TODO add alternate ending for Natural Top

ClosedHipTwist = Figure(
    name="Closed Hip Twist",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

ClosedHipTwistRShadow = Figure(
    name="Closed Hip Twist - Start in R Shadow",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=RightShadowPosition,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

OpenHipTwist = Figure(
    name="Open Hip Twist",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

ReverseTop = Figure(
    name="Reverse Top",
    start_foot=RFRight,
    next_foot=LFFwd,
    start_position=ContactPosition,
    # rhythm=Beat.2,
)

ReverseTop1_10 = ReverseTop.copy()
ReverseTop1_10.name = "1-10 of Reverse Top"
ReverseTop1_10.next_foot = RFRight
ReverseTop1_10.next_position = ContactPosition

OpeningOutFromReverseTop = Figure(
    name="Opening out from Reverse Top",
    start_foot=RFRight,
    next_foot=LFFwd,
    start_position=ContactPosition,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

Aida = Figure(
    name="Aida",
    start_foot=RFBwd,
    next_foot=RFFwd,
    # rhythm=Beat.2,
)

Spiral = Figure(
    name="Spiral",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

Curl = Figure(
    name="Curl",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

RopeSpinning = Figure(
    name="Rope Spinning",
    start_foot=LFLeft,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

CrossBasic = Figure(
    name="Cross Basic",
    start_foot=LFLatinCrossFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

CubanBreaksL = Figure(
    name="Cuban Breaks - LF",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

CubanBreaksR = Figure(
    name="Cuban Breaks - RF",
    start_foot=RFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

SplitCubanBreaksL = Figure(
    name="Split Cuban Breaks - LF",
    start_foot=LFFwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

SplitCubanBreaksR = Figure(
    name="Split Cuban Breaks - RF",
    start_foot=RFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

CubanBreaksOpenL = CubanBreaksL.copy()
CubanBreaksOpenL.name = "Cuban Breaks - Open Position - LF"
CubanBreaksOpenL.start_position = OpenPosition
CubanBreaksOpenL.next_position = OpenPosition

CubanBreaksOpenR = CubanBreaksR.copy()
CubanBreaksOpenR.name = "Cuban Breaks - Open Position - RF"
CubanBreaksOpenR.start_position = OpenPosition
CubanBreaksOpenR.next_position = OpenPosition

SplitCubanBreaksOpenL = SplitCubanBreaksL.copy()
SplitCubanBreaksOpenL.name = "Split Cuban Breaks - Open Position - LF"
SplitCubanBreaksOpenL.start_position = OpenPosition
SplitCubanBreaksOpenL.next_position = OpenPosition

SplitCubanBreaksOpenR = SplitCubanBreaksR.copy()
SplitCubanBreaksOpenR.name = "Split Cuban Breaks - Open Position - RF"
SplitCubanBreaksOpenR.start_position = OpenPosition
SplitCubanBreaksOpenR.next_position = OpenPosition

# TODO add fast new yorks here

Chase = Figure(
    name="Chase",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

RondeChasse = Figure(
    name="Ronde Chasse",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

HipTwistChasse = Figure(
    name="Hip Twist Chasse",
    start_foot=RFBwd,
    next_foot=LFAny,
    # rhythm=Beat.2,
)

SlipCloseChasse = Figure(
    name="Slip Close Chasse",
    start_foot=LFFwd,
    next_foot=RFAny,
    # rhythm=Beat.2,
)

# TODO: Open CPP figures

# TODO Alemana checked to Open CPP

FootChange1 = Figure(
    name="Foot Change 1 - Open Position to Right Shadow Position",
    start_foot=RFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.2,
)

FootChange2 = Figure(
    name="Foot Change 2 - Right Shadow Position to Open Position",
    start_foot=RFBwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=OpenPosition,
)

FootChange3 = Figure(
    name="Foot Change 3 - Open Position to Right Shadow Position",
    start_foot=RFBwd,
    next_foot=LFAny,
    start_position=OpenPosition,
    next_position=RightShadowPosition,
    # rhythm=Beat.2,
)

FootChange4 = Figure(
    name="Foot Change 4 - Right Shadow Position to Open Position",
    start_foot=RFBwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

# TODO Advanced hip twist

TurkishTowel = Figure(
    name="Turkish Towel",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

Sweetheart = Figure(
    name="Sweetheart",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=FanPosition,
    # rhythm=Beat.2,
)

FollowMyLeader = Figure(
    name="Follow my Leader",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)

# TODO guapacha timing

ClosedHipTwistSpiral = Figure(
    name="Closed Hip Twist Spiral",
    start_foot=LFFwd,
    next_foot=LFFwd,
    # rhythm=Beat.2,
)

OpenHipTwist1_5 = OpenHipTwist.copy()
OpenHipTwist1_5.name = "1-5 of Open Hip Twist"
OpenHipTwist1_5.next_foot = RFBwd
OpenHipTwist1_5.next_position = OpenPosition

OpenHipTwistSpiral = Figure(
    name="Open Hip Twist Spiral",
    start_foot=RFBwd,
    next_foot=LFFwd,
    start_position=OpenPosition,
    next_position=OpenPosition,
    # rhythm=Beat.2,
)
