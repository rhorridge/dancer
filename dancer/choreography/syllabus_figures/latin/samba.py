"""Exports figures that are typically from the Samba syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    LFAny,
    LFClose,
    LFBwd,
    LFLeft,
    LFLatinCrossFwd,
    LFFlick,
    RFRight,
    RFFwd,
    RFBwd,
    RFAny,
    RFClose,
    RFLatinCrossFwd,
    RFLatinCrossBwd,
    RFFlick,
    ClosedPosition,
    OpenPosition,
    LeftContraPosition,
    RightContraPosition,
    RightSidePosition,
    RightShadowPosition,
    PromenadePosition,
    OpenPromenadePosition,
    OpenCounterPromenadePosition,
)

BasicMovementNatural = Figure(
    name="Basic Movement - Natural",
    start_foot=RFFwd,
    next_foot=RFAny,
)

BasicMovementReverse = Figure(
    name="Basic Movement - Reverse",
    start_foot=LFFwd,
    next_foot=LFAny,
)

BasicMovementSideL = Figure(
    name="Basic Movement - Side - L",
    start_foot=LFLeft,
    next_foot=RFAny,
)

BasicMovementSideR = Figure(
    name="Basic Movement - Side - R",
    start_foot=RFRight,
    next_foot=LFAny,
)

BasicMovementProgressive = Figure(
    name="Basic Movement - Progressive",
    start_foot=RFFwd,
    next_foot=RFAny,
)

SambaWhiskToL = Figure(
    name="Samba Whisk to L",
    start_foot=LFLeft,
    next_foot=RFAny,
)

SambaWhiskToR = Figure(
    name="Samba Whisk to R",
    start_foot=RFRight,
    next_foot=LFAny,
)

# TODO with follower's underarm turn

SambaWalksPromenadeLF = Figure(
    name="Samba Walks - Promenade - LF",
    start_foot=LFFwd,
    next_foot=RFAny,
    start_position=PromenadePosition,
    next_position=PromenadePosition,
)

SambaWalksPromenadeRF = Figure(
    name="Samba Walks - Promenade - LR",
    start_foot=RFFwd,
    next_foot=LFAny,
    start_position=PromenadePosition,
    next_position=PromenadePosition,
)

SambaWalksSide = Figure(
    name="Samba Walks - Side",
    start_foot=RFFwd,
    next_foot=LFAny,
    start_position=PromenadePosition,
    next_position=PromenadePosition,
)

SambaWalksStationaryLF = Figure(
    name="Samba Walks - Stationary - LF",
    start_foot=LFClose,
    next_foot=RFAny,
)

SambaWalksStationaryRF = Figure(
    name="Samba Walks - Stationary - RF",
    start_foot=RFClose,
    next_foot=LFAny,
)

# TODO Volta Movements
# TODO Rhythm bounce

RhythmBounceRF = Figure(
    name="Rhythm Bounce - RF",
    start_foot=RFAny,
    next_foot=RFAny,
)

RhythmBounceLF = Figure(
    name="Rhythm Bounce - LF",
    start_foot=LFAny,
    next_foot=LFAny,
)

BotaFogosTravellingForward = Figure(
    name="Bota Fogos Travelling Forward",
    start_foot=LFFwd,
    next_foot=RFAny,
)

BotaFogosTravellingForward = Figure(
    name="Bota Fogos Travelling Back",
    start_foot=RFBwd,
    next_foot=RFAny,
)

# Also called Criss Cross Boto Fogos
PromenadeBotaFogos = Figure(
    name="Promenade Botafogo",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=OpenPromenadePosition,
    next_position=OpenPromenadePosition,
)

# Reverse Turn is in Ballroom - WaltzSyllabus

# Also called Travelling Voltas to R and L
CrissCrossVoltas = Figure(
    name="Criss Cross Voltas",
    start_foot=LFFwd,
    next_foot=LFAny,
    start_position=OpenPromenadePosition,
    next_position=OpenPromenadePosition,
)

SoloSpotVoltaLF = Figure(
    name="Solo Spot Volta - LF",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
)

SoloSpotVoltaRF = Figure(
    name="Solo Spot Volta - RF",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
)

# TODO Volta Spot Turn to R and L
# TODO Follower step?

ContinuousVoltaSpotTurnToRight = Figure(
    name="Volta Spot Turns to Right",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
)

ContinuousVoltaSpotTurnToLeft = Figure(
    name="Volta Spot Turns to Left",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
)

SambaFootChange1RF = Figure(
    name="Samba - Foot Change - 1 - RF",
    start_foot=LFFlick,
    next_foot=LFAny,
    next_position=RightShadowPosition,
)

SambaFootChange1LF = Figure(
    name="Samba - Foot Change - 1 - LF",
    start_foot=RFFlick,
    next_foot=RFAny,
    next_position=RightShadowPosition,
)

SambaFootChange2RF = Figure(
    name="Samba - Foot Change - 2 - RF",
    start_foot=LFFlick,
    next_foot=LFAny,
    start_position=RightShadowPosition,
)

SambaFootChange2LF = Figure(
    name="Samba - Foot Change - 2 - LF",
    start_foot=RFFlick,
    next_foot=RFAny,
    start_position=RightShadowPosition,
)

# Side chasses

ShadowTravellingVoltasLF = Figure(
    name="Shadow Travelling Voltas - LF",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)

ShadowTravellingVoltasRF = Figure(
    name="Shadow Travelling Voltas - RF",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)

CortaJaca = Figure(
    name="Corta Jaca",
    start_foot=RFFwd,
    next_foot=RFAny,
)

ClosedRocks = Figure(
    name="Closed Rocks",
    start_foot=RFFwd,
    next_foot=RFAny,
)

NaturalRoll = Figure(
    name="Natural Roll",
    start_foot=RFFwd,
    next_foot=RFAny,
)

OpenRocks = Figure(
    name="Open Rocks",
    start_foot=RFFwd,
    next_foot=RFAny,
)

BackwardRocksLF = Figure(
    name="Backward Rocks on LF",
    start_foot=RFBwd,
    next_foot=LFAny,
)

BackwardRocksRF = Figure(
    name="Backward Rocks on RF",
    start_foot=LFBwd,
    next_foot=RFAny,
)

PlaitRF = Figure(
    name="Plait - RF",
    start_foot=RFBwd,
    next_foot=LFBwd,
)

PlaitLF = Figure(
    name="Plait - LF",
    start_foot=LFBwd,
    next_foot=RFBwd,
)

# TODO this should probably have RSP
RollingOffTheArm = Figure(
    name="Rolling off the Arm",
    start_foot=LFLeft,
    next_foot=LFAny,
    start_position=RightSidePosition,
    next_position=RightSidePosition,
)

RollingOffTheArmSpinEnding1 = RollingOffTheArm.copy()
RollingOffTheArmSpinEnding1.name = f"{RollingOffTheArm.name} - Spin Ending 1"
RollingOffTheArmSpinEnding1.next_position = ClosedPosition
RollingOffTheArmSpinEnding1.next_foot = LFFwd

# TODO alternate endings for Rolling off the Arm

ArgentineCrosses = Figure(
    name="Argentine Crosses",
    start_foot=LFLeft,
    next_foot=LFAny,
)

MaypoleTurningL = Figure(
    name="Maypole Turning L",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
)

MaypoleTurningR = Figure(
    name="Maypole Turning R",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
)

ShadowCircularVoltaLF = Figure(
    name="Shadow Circular Volta - LF",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)

ShadowCircularVoltaRF = Figure(
    name="Shadow Circular Volta - RF",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)

ContraBotaFogos = Figure(
    name="Contra Bota Fogos",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=RightContraPosition,
    next_position=RightContraPosition,
)

RoundaboutToR = Figure(
    name="Roundabout to R",
    start_foot=RFLatinCrossFwd,
    next_foot=LFAny,
    start_position=RightContraPosition,
    next_position=LeftContraPosition,
)

RoundaboutToL = Figure(
    name="Roundabout to L",
    start_foot=LFLatinCrossFwd,
    next_foot=RFAny,
    start_position=LeftContraPosition,
    next_position=RightContraPosition,
)

ReverseRoll = Figure(
    name="Reverse Roll",
    start_foot=LFFwd,
    next_foot=LFAny,
)

PromenadeAndCounterPromenadeRuns = Figure(
    name="Promenade & Counter Promenade Runs",
    start_foot=RFFwd,
    next_foot=LFAny,
    next_position=OpenPromenadePosition,
)

SambaFootChange3RFPromenadePosition = Figure(
    name="Samba - Foot Change - 3 - RF - Promenade Position",
    start_foot=LFFlick,
    next_foot=LFAny,
    start_position=PromenadePosition,
    next_position=RightShadowPosition,
)

SambaFootChange3RFOpenPromenadePosition = Figure(
    name="Samba - Foot Change - 3 - RF - Open Promenade Position",
    start_foot=LFFlick,
    next_foot=LFAny,
    start_position=OpenPromenadePosition,
    next_position=RightShadowPosition,
)

SambaFootChange3LFPromenadePosition = Figure(
    name="Samba - Foot Change - 3 - LF - Promenade Position",
    start_foot=RFFlick,
    next_foot=RFAny,
    start_position=PromenadePosition,
    next_position=RightShadowPosition,
)

SambaFootChange3LFOpenPromenadePosition = Figure(
    name="Samba - Foot Change - 3 - LF - Open Promenade Position",
    start_foot=RFFlick,
    next_foot=RFAny,
    start_position=OpenPromenadePosition,
    next_position=RightShadowPosition,
)

SambaFootChange4RFPromenadePosition = Figure(
    name="Samba - Foot Change - 4 - RF - Promenade Position",
    start_foot=LFFlick,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=PromenadePosition,
)

SambaFootChange4LFPromenadePosition = Figure(
    name="Samba - Foot Change - 4 - LF - Promenade Position",
    start_foot=RFFlick,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=PromenadePosition,
)

ThreeStepTurn = Figure(
    name="Three Step Turn",
    start_foot=LFClose,
    next_foot=RFFwd,
    start_position=OpenPromenadePosition,
    next_position=OpenCounterPromenadePosition,
)

SambaLocks = Figure(
    name="Samba Locks",
    start_foot=RFFwd,
    next_foot=RFAny,
    start_position=OpenCounterPromenadePosition,
)

CruzadosWalksAndLocksLF = Figure(
    name="Cruzados Walks and Locks - LF",
    start_foot=LFFwd,
    next_foot=RFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)

CruzadosWalksAndLocksRF = Figure(
    name="Cruzados Walks and Locks - RF",
    start_foot=RFFwd,
    next_foot=LFAny,
    start_position=RightShadowPosition,
    next_position=RightShadowPosition,
)


# TODO Cruzados Locks in Shadow Position

# TODO Drop volta
