"""Unit tests for .quickstep.
"""

import pytest
from dancer.choreography.feet import (
    RFFwd,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
    RFEither,
)
from dancer.choreography.position import (
    ClosedPosition,
    PromenadePosition,
)
from . import (
    quickstep,
    NaturalSpinTurn1_3,
)


def test_natural_spin_turn_progressive_chasse():
    assert NaturalSpinTurn1_3.valid_next(quickstep.ProgressiveChasseToL)


def test_tipplechasses():
    assert quickstep.TippleChasseToL1_4.valid_next(quickstep.TippleChasseToR2_4)


def test_walkrf():
    quickstep.WalkRF.name == "Walk RF"
    quickstep.WalkRF.start_foot == RFFwd
    quickstep.WalkRF.next_foot == LFFwd
    quickstep.WalkRF.start_position == ClosedPosition
    quickstep.WalkRF.next_position == ClosedPosition


def test_walklf():
    quickstep.WalkLF.name == "Walk LF"
    quickstep.WalkLF.start_foot == LFFwd
    quickstep.WalkLF.next_foot == RFFwd
    quickstep.WalkLF.start_position == ClosedPosition
    quickstep.WalkLF.next_position == ClosedPosition


def test_progressivechassetol():
    quickstep.ProgressiveChasseToL.name == "Progressive Chasse"
    quickstep.ProgressiveChasseToL.start_foot == RFEither
    quickstep.ProgressiveChasseToL.next_foot == RFEither
    quickstep.ProgressiveChasseToL.start_position == ClosedPosition
    quickstep.ProgressiveChasseToL.next_position == ClosedPosition


def test_naturalpivotturn():
    quickstep.NaturalPivotTurn.name == "Natural Pivot Turn"
    quickstep.NaturalPivotTurn.start_foot == LFBwd
    quickstep.NaturalPivotTurn.next_foot == RFFwd
    quickstep.NaturalPivotTurn.start_position == ClosedPosition
    quickstep.NaturalPivotTurn.next_position == ClosedPosition


def test_tipplechasetor():
    quickstep.TippleChasseToR.name == "Tipple Chasse to R"
    quickstep.TippleChasseToR.start_foot == LFBwd
    quickstep.TippleChasseToR.next_foot == RFFwd
    quickstep.TippleChasseToR.start_position == ClosedPosition
    quickstep.TippleChasseToR.next_position == ClosedPosition


def test_runningfinish():
    quickstep.RunningFinish.name == "Running Finish"
    quickstep.RunningFinish.start_foot == LFBwd
    quickstep.RunningFinish.next_foot == RFFwd
    quickstep.RunningFinish.start_position == ClosedPosition
    quickstep.RunningFinish.next_position == ClosedPosition


def test_openrunningfinish():
    quickstep.OpenRunningFinish.name == "Open Running Finish"
    quickstep.OpenRunningFinish.start_foot == LFBwd
    quickstep.OpenRunningFinish.next_foot == RFFwd
    quickstep.OpenRunningFinish.start_position == ClosedPosition
    quickstep.OpenRunningFinish.next_position == PromenadePosition


def test_quickopenreverse():
    quickstep.QuickOpenReverse.name == "Quick Open Reverse"
    quickstep.QuickOpenReverse.start_foot == RFFwd
    quickstep.QuickOpenReverse.next_foot == RFBwd
    quickstep.QuickOpenReverse.start_position == ClosedPosition
    quickstep.QuickOpenReverse.next_position == ClosedPosition


def test_zigzag():
    quickstep.ZigZag.name == "Zig-Zag"
    quickstep.ZigZag.start_foot == RFFwd
    quickstep.ZigZag.next_foot == LFFwd
    quickstep.ZigZag.start_position == ClosedPosition
    quickstep.ZigZag.next_position == ClosedPosition


def test_fishtail():
    quickstep.Fishtail.name == "Fishtail"
    quickstep.Fishtail.start_foot == RFFwd
    quickstep.Fishtail.next_foot == RFFwd
    quickstep.Fishtail.start_position == ClosedPosition
    quickstep.Fishtail.next_position == ClosedPosition


def test_runningrightturn():
    quickstep.RunningRightTurn.name == "Running Right Turn"
    quickstep.RunningRightTurn.start_foot == RFFwd
    quickstep.RunningRightTurn.next_foot == RFFwd
    quickstep.RunningRightTurn.start_position == ClosedPosition
    quickstep.RunningRightTurn.next_position == ClosedPosition


def test_fourquickrun():
    quickstep.FourQuickRun.name == "Four Quick Run"
    quickstep.FourQuickRun.start_foot == RFBwd
    quickstep.FourQuickRun.next_foot == RFFwd
    quickstep.FourQuickRun.start_position == ClosedPosition
    quickstep.FourQuickRun.next_position == ClosedPosition


def test_v6():
    quickstep.V6.name == "V-Six"
    quickstep.V6.start_foot == RFBwd
    quickstep.V6.next_foot == RFFwd
    quickstep.V6.start_position == ClosedPosition
    quickstep.V6.next_position == ClosedPosition


def test_tipplechassetol():
    quickstep.TippleChasseToL.name == "Tipple Chasse to L"
    quickstep.TippleChasseToL.start_foot == RFFwd
    quickstep.TippleChasseToL.next_foot == RFBwd
    quickstep.TippleChasseToL.start_position == ClosedPosition
    quickstep.TippleChasseToL.next_position == ClosedPosition


def test_tipplechassetol1_4():
    quickstep.TippleChasseToL1_4.name == "1-4 of Tipple Chasse to L"
    quickstep.TippleChasseToL1_4.start_foot == RFFwd
    quickstep.TippleChasseToL1_4.next_foot == LFBwd
    quickstep.TippleChasseToL1_4.start_position == ClosedPosition
    quickstep.TippleChasseToL1_4.next_position == ClosedPosition


def test_crossswivel():
    quickstep.CrossSwivel.name == "Cross Swivel"
    quickstep.CrossSwivel.start_foot == LFFwd
    quickstep.CrossSwivel.next_foot == RFEither
    quickstep.CrossSwivel.start_position == ClosedPosition
    quickstep.CrossSwivel.next_position == ClosedPosition


def test_runningcrosschasse():
    quickstep.RunningCrossChasse.name == "Running Cross Chasse"
    quickstep.RunningCrossChasse.start_foot == LFFwd
    quickstep.RunningCrossChasse.next_foot == RFFwd
    quickstep.RunningCrossChasse.start_position == ClosedPosition
    quickstep.RunningCrossChasse.next_position == ClosedPosition


def test_passingnaturalturn():
    quickstep.PassingNaturalTurn.name == "Passing Natural Turn"
    quickstep.PassingNaturalTurn.start_foot == RFFwd
    quickstep.PassingNaturalTurn.next_foot == LFBwd
    quickstep.PassingNaturalTurn.start_position == PromenadePosition
    quickstep.PassingNaturalTurn.next_position == ClosedPosition


def test_sixquickrun():
    quickstep.SixQuickRun.name == "Six Quick Run"
    quickstep.SixQuickRun.start_foot == RFBwd
    quickstep.SixQuickRun.next_foot == RFFwd
    quickstep.SixQuickRun.start_position == ClosedPosition
    quickstep.SixQuickRun.next_position == ClosedPosition


def test_rumbacross():
    quickstep.RumbaCross.name == "Rumba Cross"
    quickstep.RumbaCross.start_foot == RFFwd
    quickstep.RumbaCross.next_foot == RFFwd
    quickstep.RumbaCross.start_position == ClosedPosition
    quickstep.RumbaCross.next_position == ClosedPosition


def test_tipsytor():
    quickstep.TipsyToR.name == "Tipsy to R"
    quickstep.TipsyToR.start_foot == LFBwd
    quickstep.TipsyToR.next_foot == LFFwd
    quickstep.TipsyToR.start_position == ClosedPosition
    quickstep.TipsyToR.next_position == ClosedPosition


def test_forwardlockstep2_4():
    quickstep.ForwardLockStep2_4.name == "2-4 of Forward Lock Step"
    quickstep.ForwardLockStep2_4.start_foot == LFFwd
    quickstep.ForwardLockStep2_4.next_foot == RFFwd
    quickstep.ForwardLockStep2_4.start_position == ClosedPosition
    quickstep.ForwardLockStep2_4.next_position == ClosedPosition


def test_backwardlockstep2_4():
    quickstep.BackwardLockStep2_4.name == "2-4 of Backward Lock Step"
    quickstep.BackwardLockStep2_4.start_foot == RFBwd
    quickstep.BackwardLockStep2_4.next_foot == LFBwd
    quickstep.BackwardLockStep2_4.start_position == ClosedPosition
    quickstep.BackwardLockStep2_4.next_position == ClosedPosition


def test_tipsytol():
    quickstep.TipsyToL.name == "Tipsy to L"
    quickstep.TipsyToL.start_foot == RFFwd
    quickstep.TipsyToL.next_foot == RFBwd
    quickstep.TipsyToL.start_position == ClosedPosition
    quickstep.TipsyToL.next_position == ClosedPosition


def test_tipsytol2_4():
    quickstep.TipsyToL2_4.name == "2-4 of Tipsy to L"
    quickstep.TipsyToL2_4.start_foot == LFEither
    quickstep.TipsyToL2_4.next_foot == RFBwd
    quickstep.TipsyToL2_4.start_position == ClosedPosition
    quickstep.TipsyToL2_4.next_position == ClosedPosition
