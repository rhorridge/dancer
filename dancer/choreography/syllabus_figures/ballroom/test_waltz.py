"""Unit tests for .waltz.
"""

from dancer.choreography.feet import (
    RFFwd,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
)
from dancer.choreography.position import (
    ClosedPosition,
    PromenadePosition,
)
from dancer.choreography import ALL_FIGURES
from . import waltz


def test_closedchangerf():
    assert waltz.ClosedChangeRF.name == "Closed Change RF"
    assert waltz.ClosedChangeRF.start_foot == RFFwd
    assert waltz.ClosedChangeRF.next_foot == LFFwd
    assert waltz.ClosedChangeRF.start_position == ClosedPosition
    assert waltz.ClosedChangeRF.next_position == ClosedPosition


def test_closedchangelf():
    assert waltz.ClosedChangeLF.name == "Closed Change LF"
    assert waltz.ClosedChangeLF.start_foot == LFFwd
    assert waltz.ClosedChangeLF.next_foot == RFFwd
    assert waltz.ClosedChangeLF.start_position == ClosedPosition
    assert waltz.ClosedChangeLF.next_position == ClosedPosition
    assert waltz.NaturalTurn1_3.name in set(
        [step.name for step in waltz.ClosedChangeLF.next(figures=ALL_FIGURES)]
    )


def test_naturalturn():
    assert waltz.NaturalTurn.name == "Natural Turn"
    assert waltz.NaturalTurn.start_foot == RFFwd
    assert waltz.NaturalTurn.next_foot == RFFwd
    assert waltz.NaturalTurn.start_position == ClosedPosition
    assert waltz.NaturalTurn.next_position == ClosedPosition


def test_naturalturn1_3():
    assert waltz.NaturalTurn1_3.name == "1-3 of Natural Turn"
    assert waltz.NaturalTurn1_3.start_foot == RFFwd
    assert waltz.NaturalTurn1_3.next_foot == LFBwd
    assert waltz.NaturalTurn1_3.start_position == ClosedPosition
    assert waltz.NaturalTurn1_3.next_position == ClosedPosition
    assert waltz.NaturalSpinTurn1_3.name in set(
        [step.name for step in waltz.NaturalTurn1_3.next(figures=ALL_FIGURES)]
    )


def test_reverseturn():
    assert waltz.ReverseTurn.name == "Reverse Turn"
    assert waltz.ReverseTurn.start_foot == LFFwd
    assert waltz.ReverseTurn.next_foot == LFFwd
    assert waltz.ReverseTurn.start_position == ClosedPosition
    assert waltz.ReverseTurn.next_position == ClosedPosition


def test_naturalspinturn():
    assert waltz.NaturalSpinTurn.name == "Natural Spin Turn"
    assert waltz.NaturalSpinTurn.start_foot == LFBwd
    assert waltz.NaturalSpinTurn.next_foot == LFFwd
    assert waltz.NaturalSpinTurn.start_position == ClosedPosition
    assert waltz.NaturalSpinTurn.next_position == ClosedPosition


def test_naturalspinturn1_3():
    assert waltz.NaturalSpinTurn1_3.name == "1-3 of Natural Spin Turn"
    assert waltz.NaturalSpinTurn1_3.start_foot == LFBwd
    assert waltz.NaturalSpinTurn1_3.next_foot == RFBwd
    assert waltz.NaturalSpinTurn1_3.start_position == ClosedPosition
    assert waltz.NaturalSpinTurn1_3.next_position == ClosedPosition


def test_whisk():
    assert waltz.Whisk.name == "Whisk"
    assert waltz.Whisk.start_foot == LFFwd
    assert waltz.Whisk.next_foot == RFFwd
    assert waltz.Whisk.start_position == ClosedPosition
    assert waltz.Whisk.next_position == PromenadePosition


def test_chassefrompp():
    assert waltz.ChasseFromPP.name == "Chasse from PP"
    assert waltz.ChasseFromPP.start_foot == RFFwd
    assert waltz.ChasseFromPP.next_foot == RFFwd
    assert waltz.ChasseFromPP.start_position == PromenadePosition
    assert waltz.ChasseFromPP.next_position == ClosedPosition


def test_hesitationchange():
    assert waltz.HesitationChange.name == "Hesitation Change"
    assert waltz.HesitationChange.start_foot == LFBwd
    assert waltz.HesitationChange.next_foot == LFFwd
    assert waltz.HesitationChange.start_position == ClosedPosition
    assert waltz.HesitationChange.next_position == ClosedPosition


def test_progressivechassetor():
    assert waltz.ProgressiveChasseToR.name == "Progressive Chasse to R"
    assert waltz.ProgressiveChasseToR.start_foot == LFFwd
    assert waltz.ProgressiveChasseToR.next_foot == LFBwd
    assert waltz.ProgressiveChasseToR.start_position == ClosedPosition
    assert waltz.ProgressiveChasseToR.next_position == ClosedPosition


def test_closedimpetus():
    assert waltz.ClosedImpetus.name == "Closed Impetus"
    assert waltz.ClosedImpetus.start_foot == LFBwd
    assert waltz.ClosedImpetus.next_foot == RFBwd
    assert waltz.ClosedImpetus.start_position == ClosedPosition
    assert waltz.ClosedImpetus.next_position == ClosedPosition


def test_outsidechange():
    assert waltz.OutsideChange.name == "Outside Change"
    assert waltz.OutsideChange.start_foot == LFBwd
    assert waltz.OutsideChange.next_foot == RFFwd
    assert waltz.OutsideChange.start_position == ClosedPosition
    assert waltz.OutsideChange.next_position == ClosedPosition


def test_reversecorte():
    assert waltz.ReverseCorte.name == "Reverse Corte"
    assert waltz.ReverseCorte.start_foot == RFBwd
    assert waltz.ReverseCorte.next_foot == LFEither
    assert waltz.ReverseCorte.start_position == ClosedPosition
    assert waltz.ReverseCorte.next_position == ClosedPosition


def test_backwhisk():
    assert waltz.BackWhisk.name == "Back Whisk"
    assert waltz.BackWhisk.start_foot == LFBwd
    assert waltz.BackWhisk.next_foot == RFFwd
    assert waltz.BackWhisk.start_position == ClosedPosition
    assert waltz.BackWhisk.next_position == PromenadePosition


def test_basicweave():
    assert waltz.BasicWeave.name == "Basic Weave (Waltz)"
    assert waltz.BasicWeave.start_foot == RFBwd
    assert waltz.BasicWeave.next_foot == RFFwd
    assert waltz.BasicWeave.start_position == ClosedPosition
    assert waltz.BasicWeave.next_position == ClosedPosition


def test_doublereversespin():
    assert waltz.DoubleReverseSpin.name == "Double Reverse Spin"
    assert waltz.DoubleReverseSpin.start_foot == LFFwd
    assert waltz.DoubleReverseSpin.next_foot == LFFwd
    assert waltz.DoubleReverseSpin.start_position == ClosedPosition
    assert waltz.DoubleReverseSpin.next_position == ClosedPosition


def test_draghesitation():
    assert waltz.DragHesitation.name == "Drag Hesitation"
    assert waltz.DragHesitation.start_foot == LFFwd
    assert waltz.DragHesitation.next_foot == LFEither
    assert waltz.DragHesitation.start_position == ClosedPosition
    assert waltz.DragHesitation.next_position == ClosedPosition


def test_backwardlockstep():
    assert waltz.BackwardLockStep.name == "Backward Lock Step"
    assert waltz.BackwardLockStep.start_foot == LFBwd
    assert waltz.BackwardLockStep.next_foot == LFBwd
    assert waltz.BackwardLockStep.start_position == ClosedPosition
    assert waltz.BackwardLockStep.next_position == ClosedPosition


def test_forwardlockstep():
    assert waltz.ForwardLockStep.name == "Forward Lock Step"
    assert waltz.ForwardLockStep.start_foot == RFFwd
    assert waltz.ForwardLockStep.next_foot == RFFwd
    assert waltz.ForwardLockStep.start_position == ClosedPosition
    assert waltz.ForwardLockStep.next_position == ClosedPosition


def test_reversepivot():
    assert waltz.ReversePivot.name == "Reverse Pivot"
    assert waltz.ReversePivot.start_foot == RFBwd
    assert waltz.ReversePivot.next_foot == LFFwd
    assert waltz.ReversePivot.start_position == ClosedPosition
    assert waltz.ReversePivot.next_position == ClosedPosition


def test_closedtelemark():
    assert waltz.ClosedTelemark.name == "Closed Telemark"
    assert waltz.ClosedTelemark.start_foot == LFFwd
    assert waltz.ClosedTelemark.next_foot == RFFwd
    assert waltz.ClosedTelemark.start_position == ClosedPosition
    assert waltz.ClosedTelemark.next_position == ClosedPosition


def test_opentelemark():
    assert waltz.OpenTelemark.name == "Open Telemark"
    assert waltz.OpenTelemark.start_foot == LFFwd
    assert waltz.OpenTelemark.next_foot == RFFwd
    assert waltz.OpenTelemark.start_position == ClosedPosition
    assert waltz.OpenTelemark.next_position == PromenadePosition


def test_crosshesitation():
    assert waltz.CrossHesitation.name == "Cross Hesitation"
    assert waltz.CrossHesitation.start_foot == RFFwd
    assert waltz.CrossHesitation.next_foot == RFFwd
    assert waltz.CrossHesitation.start_position == PromenadePosition
    assert waltz.CrossHesitation.next_position == ClosedPosition


def test_wing():
    assert waltz.Wing.name == "Wing"
    assert waltz.Wing.start_foot == RFFwd
    assert waltz.Wing.next_foot == LFFwd
    assert waltz.Wing.start_position == PromenadePosition
    assert waltz.Wing.next_position == ClosedPosition


def test_openimpetus():
    assert waltz.OpenImpetus.name == "Open Impetus"
    assert waltz.OpenImpetus.start_foot == LFBwd
    assert waltz.OpenImpetus.next_foot == RFFwd
    assert waltz.OpenImpetus.start_position == ClosedPosition
    assert waltz.OpenImpetus.next_position == PromenadePosition


def test_weavefrompp():
    assert waltz.WeaveFromPP.name == "Weave from PP (Waltz)"
    assert waltz.WeaveFromPP.start_foot == RFFwd
    assert waltz.WeaveFromPP.next_foot == RFFwd
    assert waltz.WeaveFromPP.start_position == PromenadePosition
    assert waltz.WeaveFromPP.next_position == ClosedPosition


def test_outsidespin():
    assert waltz.OutsideSpin.name == "Outside Spin"
    assert waltz.OutsideSpin.start_foot == LFBwd
    assert waltz.OutsideSpin.next_foot == RFFwd
    assert waltz.OutsideSpin.start_position == ClosedPosition
    assert waltz.OutsideSpin.next_position == ClosedPosition


def test_turninglocktol():
    assert waltz.TurningLockToL.name == "Turning Lock to L"
    assert waltz.TurningLockToL.start_foot == RFBwd
    assert waltz.TurningLockToL.next_foot == RFFwd
    assert waltz.TurningLockToL.start_position == ClosedPosition
    assert waltz.TurningLockToL.next_position == ClosedPosition


def test_turninglocktor():
    assert waltz.TurningLockToR.name == "Turning Lock to R"
    assert waltz.TurningLockToR.start_foot == RFBwd
    assert waltz.TurningLockToR.next_foot == RFFwd
    assert waltz.TurningLockToR.start_position == ClosedPosition
    assert waltz.TurningLockToR.next_position == PromenadePosition


def test_closedwing():
    assert waltz.ClosedWing.name == "Closed Wing"
    assert waltz.ClosedWing.start_foot == RFFwd
    assert waltz.ClosedWing.next_foot == LFFwd
    assert waltz.ClosedWing.start_position == ClosedPosition
    assert waltz.ClosedWing.next_position == ClosedPosition


def test_fallawayreverseslippivot():
    assert waltz.FallawayReverseSlipPivot.name == "Fallaway Reverse & Slip Pivot"
    assert waltz.FallawayReverseSlipPivot.start_foot == LFFwd
    assert waltz.FallawayReverseSlipPivot.next_foot == LFFwd
    assert waltz.FallawayReverseSlipPivot.start_position == ClosedPosition
    assert waltz.FallawayReverseSlipPivot.next_position == ClosedPosition


def test_hovercorte():
    assert waltz.HoverCorte.name == "Hover Corte"
    assert waltz.HoverCorte.start_foot == RFBwd
    assert waltz.HoverCorte.next_foot == LFBwd
    assert waltz.HoverCorte.start_position == ClosedPosition
    assert waltz.HoverCorte.next_position == ClosedPosition


def test_fallawaynaturalturn():
    assert waltz.FallawayNaturalTurn.name == "Fallaway Natural Turn"
    assert waltz.FallawayNaturalTurn.start_foot == RFFwd
    assert waltz.FallawayNaturalTurn.next_foot == RFFwd
    assert waltz.FallawayNaturalTurn.start_position == PromenadePosition
    assert waltz.FallawayNaturalTurn.next_position == ClosedPosition


def test_runningspinturn():
    assert waltz.RunningSpinTurn.name == "Running Spin Turn"
    assert waltz.RunningSpinTurn.start_foot == LFBwd
    assert waltz.RunningSpinTurn.next_foot == LFBwd
    assert waltz.RunningSpinTurn.start_position == ClosedPosition
    assert waltz.RunningSpinTurn.next_position == ClosedPosition


def test_fallawaywhisk():
    assert waltz.FallawayWhisk.name == "Fallaway Whisk"
    assert waltz.FallawayWhisk.start_foot == LFBwd
    assert waltz.FallawayWhisk.next_foot == RFFwd
    assert waltz.FallawayWhisk.start_position == ClosedPosition
    assert waltz.FallawayWhisk.next_position == PromenadePosition
