"""Unit tests for .foxtrot.
"""

from dancer.choreography.feet import (
    RFFwd,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
)
from dancer.choreography.position import (
    ClosedPosition,
    PromenadePosition,
)
from . import foxtrot


def test_featherstep():
    assert foxtrot.FeatherStep.name == "Feather Step"
    assert foxtrot.FeatherStep.start_foot == RFFwd
    assert foxtrot.FeatherStep.next_foot == LFFwd
    assert foxtrot.FeatherStep.start_position == ClosedPosition
    assert foxtrot.FeatherStep.next_position == ClosedPosition


def test_threestep():
    assert foxtrot.ThreeStep.name == "Three Step"
    assert foxtrot.ThreeStep.start_foot == LFFwd
    assert foxtrot.ThreeStep.next_foot == RFFwd
    assert foxtrot.ThreeStep.start_position == ClosedPosition
    assert foxtrot.ThreeStep.next_position == ClosedPosition


def test_changeofdirection():
    assert foxtrot.ChangeOfDirection.name == "Change of Direction"
    assert foxtrot.ChangeOfDirection.start_foot == LFFwd
    assert foxtrot.ChangeOfDirection.next_foot == LFFwd
    assert foxtrot.ChangeOfDirection.start_position == ClosedPosition
    assert foxtrot.ChangeOfDirection.next_position == ClosedPosition


def test_reverseturn1_3():
    assert foxtrot.ReverseTurn1_3.name == "1-3 of Reverse Turn"
    assert foxtrot.ReverseTurn1_3.start_foot == LFFwd
    assert foxtrot.ReverseTurn1_3.next_foot == RFBwd
    assert foxtrot.ReverseTurn1_3.start_position == ClosedPosition
    assert foxtrot.ReverseTurn1_3.next_position == ClosedPosition


def test_featherfinish():
    assert foxtrot.FeatherFinish.name == "Feather Finish"
    assert foxtrot.FeatherFinish.start_foot == RFBwd
    assert foxtrot.FeatherFinish.next_foot == LFFwd
    assert foxtrot.FeatherFinish.start_position == ClosedPosition
    assert foxtrot.FeatherFinish.next_position == ClosedPosition


def test_naturalweave():
    assert foxtrot.NaturalWeave.name == "Natural Weave"
    assert foxtrot.NaturalWeave.start_foot == RFFwd
    assert foxtrot.NaturalWeave.next_foot == LFFwd
    assert foxtrot.NaturalWeave.start_position == ClosedPosition
    assert foxtrot.NaturalWeave.next_position == ClosedPosition


def test_basicweave2():
    assert foxtrot.BasicWeave2.name == "Basic Weave (Foxtrot)"
    assert foxtrot.BasicWeave2.start_foot == LFFwd
    assert foxtrot.BasicWeave2.next_foot == LFFwd
    assert foxtrot.BasicWeave2.start_position == ClosedPosition
    assert foxtrot.BasicWeave2.next_position == ClosedPosition


def test_reversewave():
    assert foxtrot.ReverseWave.name == "Reverse Wave"
    assert foxtrot.ReverseWave.start_foot == LFFwd
    assert foxtrot.ReverseWave.next_foot == RFFwd
    assert foxtrot.ReverseWave.start_position == ClosedPosition
    assert foxtrot.ReverseWave.next_position == ClosedPosition


def test_reversewave1_4():
    assert foxtrot.ReverseWave1_4.name == "1-4 of Reverse Wave"
    assert foxtrot.ReverseWave1_4.start_foot == LFFwd
    assert foxtrot.ReverseWave1_4.next_foot == LFEither
    assert foxtrot.ReverseWave1_4.start_position == ClosedPosition
    assert foxtrot.ReverseWave1_4.next_position == ClosedPosition


def test_featherending():
    assert foxtrot.FeatherEnding.name == "Feather Ending"
    assert foxtrot.FeatherEnding.start_foot == RFFwd
    assert foxtrot.FeatherEnding.next_foot == LFFwd
    assert foxtrot.FeatherEnding.start_position == PromenadePosition
    assert foxtrot.FeatherEnding.next_position == ClosedPosition


def test_topspin():
    assert foxtrot.TopSpin.name == "Top Spin"
    assert foxtrot.TopSpin.start_foot == LFBwd
    assert foxtrot.TopSpin.next_foot == LFFwd
    assert foxtrot.TopSpin.start_position == ClosedPosition
    assert foxtrot.TopSpin.next_position == ClosedPosition


def test_hoverfeather():
    assert foxtrot.HoverFeather.name == "Hover Feather"
    assert foxtrot.HoverFeather.start_foot == LFFwd
    assert foxtrot.HoverFeather.next_foot == LFFwd
    assert foxtrot.HoverFeather.start_position == ClosedPosition
    assert foxtrot.HoverFeather.next_position == ClosedPosition


def test_hovertelemark():
    assert foxtrot.HoverTelemark.name == "Hover Telemark"
    assert foxtrot.HoverTelemark.start_foot == LFFwd
    assert foxtrot.HoverTelemark.next_foot == RFFwd
    assert foxtrot.HoverTelemark.start_position == ClosedPosition
    assert foxtrot.HoverTelemark.next_position == ClosedPosition


def test_naturaltelemark():
    assert foxtrot.NaturalTelemark.name == "Natural Telemark"
    assert foxtrot.NaturalTelemark.start_foot == RFFwd
    assert foxtrot.NaturalTelemark.next_foot == LFFwd
    assert foxtrot.NaturalTelemark.start_position == ClosedPosition
    assert foxtrot.NaturalTelemark.next_position == ClosedPosition


def test_hovercross():
    assert foxtrot.HoverCross.name == "Hover Cross"
    assert foxtrot.HoverCross.start_foot == RFFwd
    assert foxtrot.HoverCross.next_foot == LFFwd
    assert foxtrot.HoverCross.start_position == ClosedPosition
    assert foxtrot.HoverCross.next_position == ClosedPosition


def test_outsideswivel():
    assert foxtrot.OutsideSwivel.name == "Outside Swivel (Foxtrot)"
    assert foxtrot.OutsideSwivel.start_foot == LFBwd
    assert foxtrot.OutsideSwivel.next_foot == RFFwd
    assert foxtrot.OutsideSwivel.start_position == ClosedPosition
    assert foxtrot.OutsideSwivel.next_position == PromenadePosition


def test_weavefrompp2():
    assert foxtrot.WeaveFromPP2.name == "Weave from PP (Foxtrot)"
    assert foxtrot.WeaveFromPP2.start_foot == RFFwd
    assert foxtrot.WeaveFromPP2.next_foot == LFFwd
    assert foxtrot.WeaveFromPP2.start_position == PromenadePosition
    assert foxtrot.WeaveFromPP2.next_position == ClosedPosition


def test_naturaltwistturn():
    assert foxtrot.NaturalTwistTurn.name == "Natural Twist Turn"
    assert foxtrot.NaturalTwistTurn.start_foot == RFFwd
    assert foxtrot.NaturalTwistTurn.next_foot == LFFwd
    assert foxtrot.NaturalTwistTurn.start_position == ClosedPosition
    assert foxtrot.NaturalTwistTurn.next_position == ClosedPosition


def test_quicknaturalweavefrompp():
    assert foxtrot.QuickNaturalWeaveFromPP.name == "Quick Natural Weave from PP"
    assert foxtrot.QuickNaturalWeaveFromPP.start_foot == RFFwd
    assert foxtrot.QuickNaturalWeaveFromPP.next_foot == LFFwd
    assert foxtrot.QuickNaturalWeaveFromPP.start_position == PromenadePosition
    assert foxtrot.QuickNaturalWeaveFromPP.next_position == ClosedPosition


def test_curvedfeather():
    assert foxtrot.CurvedFeather.name == "Curved Feather"
    assert foxtrot.CurvedFeather.start_foot == RFFwd
    assert foxtrot.CurvedFeather.next_foot == LFBwd
    assert foxtrot.CurvedFeather.start_position == ClosedPosition
    assert foxtrot.CurvedFeather.next_position == ClosedPosition


def test_backfeather():
    assert foxtrot.BackFeather.name == "Back Feather"
    assert foxtrot.BackFeather.start_foot == LFBwd
    assert foxtrot.BackFeather.next_foot == RFBwd
    assert foxtrot.BackFeather.start_position == ClosedPosition
    assert foxtrot.BackFeather.next_position == ClosedPosition


def test_naturalzigzagfrompp():
    assert foxtrot.NaturalZigZagFromPP.name == "Natural Zig-Zag from PP"
    assert foxtrot.NaturalZigZagFromPP.start_foot == RFFwd
    assert foxtrot.NaturalZigZagFromPP.next_foot == LFFwd
    assert foxtrot.NaturalZigZagFromPP.start_position == PromenadePosition
    assert foxtrot.NaturalZigZagFromPP.next_position == ClosedPosition


def test_naturalhovertelemark():
    assert foxtrot.NaturalHoverTelemark.name == "Natural Hover Telemark"
    assert foxtrot.NaturalHoverTelemark.start_foot == RFFwd
    assert foxtrot.NaturalHoverTelemark.next_foot == LFFwd
    assert foxtrot.NaturalHoverTelemark.start_position == ClosedPosition
    assert foxtrot.NaturalHoverTelemark.next_position == ClosedPosition


def test_bouncefallawaywithweaveending():
    assert (
        foxtrot.BounceFallawayWithWeaveEnding.name
        == "Bounce Fallaway with Weave Ending"
    )
    assert foxtrot.BounceFallawayWithWeaveEnding.start_foot == LFFwd
    assert foxtrot.BounceFallawayWithWeaveEnding.next_foot == LFFwd
    assert foxtrot.BounceFallawayWithWeaveEnding.start_position == ClosedPosition
    assert foxtrot.BounceFallawayWithWeaveEnding.next_position == ClosedPosition


def test_extendedreversewave():
    assert foxtrot.ExtendedReverseWave.name == "Extended Reverse Wave"
    assert foxtrot.ExtendedReverseWave.start_foot == LFFwd
    assert foxtrot.ExtendedReverseWave.next_foot == RFFwd
    assert foxtrot.ExtendedReverseWave.start_position == ClosedPosition
    assert foxtrot.ExtendedReverseWave.next_position == ClosedPosition


def test_curvedthreestep():
    assert foxtrot.CurvedThreeStep.name == "Curved Three Step"
    assert foxtrot.CurvedThreeStep.start_foot == LFFwd
    assert foxtrot.CurvedThreeStep.next_foot == RFFwd
    assert foxtrot.CurvedThreeStep.start_position == ClosedPosition
    assert foxtrot.CurvedThreeStep.next_position == ClosedPosition
