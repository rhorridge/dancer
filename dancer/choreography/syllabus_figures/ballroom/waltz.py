"""Exports figures that are typically from the Waltz syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    RFFwd,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
    ClosedPosition,
    PromenadePosition,
)


ClosedChangeRF = Figure(
    name="Closed Change RF",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=ClosedPosition,
)
# (NaturalFigure, StartClosed, FinishClosed):

ClosedChangeLF = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Closed Change LF",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

NaturalTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Turn",
    start_foot=RFFwd,
    next_foot=RFFwd,
)

NaturalTurn1_3 = NaturalTurn.copy()
NaturalTurn1_3.name = "1-3 of Natural Turn"
NaturalTurn1_3.next_foot = LFBwd

ReverseTurn = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Reverse Turn",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

NaturalSpinTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Spin Turn",
    start_foot=LFBwd,
    next_foot=LFFwd,
)

NaturalSpinTurn1_3 = NaturalSpinTurn.copy()
NaturalSpinTurn1_3.name = "1-3 of Natural Spin Turn"
NaturalSpinTurn1_3.next_foot = RFBwd

Whisk = Figure(
    # (ReverseFigure, StartClosed, FinishPP):
    name="Whisk",
    start_foot=LFFwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

ChasseFromPP = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Chasse from PP",
    start_foot=RFFwd,
    next_foot=RFFwd,
    start_position=PromenadePosition,
)

HesitationChange = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Hesitation Change",
    start_foot=LFBwd,
    next_foot=LFFwd,
)

ProgressiveChasseToR = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Progressive Chasse to R",
    start_foot=LFFwd,
    next_foot=LFBwd,
)

ClosedImpetus = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Closed Impetus",
    start_foot=LFBwd,
    next_foot=RFBwd,
)

OutsideChange = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Outside Change",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

ReverseCorte = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Reverse Corte",
    start_foot=RFBwd,
    next_foot=LFEither,
)

BackWhisk = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Back Whisk",
    start_foot=LFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

BasicWeave = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Basic Weave (Waltz)",
    start_foot=RFBwd,
    next_foot=RFFwd,
)

DoubleReverseSpin = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Double Reverse Spin",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

DragHesitation = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Drag Hesitation",
    start_foot=LFFwd,
    next_foot=LFEither,
)

BackwardLockStep = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Backward Lock Step",
    start_foot=LFBwd,
    next_foot=LFBwd,
)

ForwardLockStep = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Forward Lock Step",
    start_foot=RFFwd,
    next_foot=RFFwd,
)

ReversePivot = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Reverse Pivot",
    start_foot=RFBwd,
    next_foot=LFFwd,
)

ClosedTelemark = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Closed Telemark",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

OpenTelemark = Figure(
    # (ReverseFigure, StartClosed, FinishPP):
    name="Open Telemark",
    start_foot=LFFwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

CrossHesitation = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Cross Hesitation",
    start_foot=RFFwd,
    next_foot=RFFwd,
    start_position=PromenadePosition,
)

Wing = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Wing",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
)

OpenImpetus = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Open Impetus",
    start_foot=LFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

WeaveFromPP = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Weave from PP (Waltz)",
    start_foot=RFFwd,
    next_foot=RFFwd,
    start_position=PromenadePosition,
)

OutsideSpin = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Outside Spin",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

TurningLockToL = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Turning Lock to L",
    start_foot=RFBwd,
    next_foot=RFFwd,
)

TurningLockToR = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Turning Lock to R",
    start_foot=RFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

ClosedWing = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Closed Wing",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

FallawayReverseSlipPivot = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Fallaway Reverse & Slip Pivot",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

HoverCorte = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Hover Corte",
    start_foot=RFBwd,
    next_foot=LFBwd,
)

FallawayNaturalTurn = Figure(
    # (NaturalFigure, StartPP, FinishClosed):
    name="Fallaway Natural Turn",
    start_foot=RFFwd,
    next_foot=RFFwd,
    start_position=PromenadePosition,
)

RunningSpinTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Running Spin Turn",
    start_foot=LFBwd,
    next_foot=LFBwd,
)

FallawayWhisk = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Fallaway Whisk",
    start_foot=LFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)
