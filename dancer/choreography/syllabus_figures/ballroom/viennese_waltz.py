"""Exports figures that are typically from the Viennese Waltz syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    RFFwd,
    LFBwd,
    RFBwd,
    RFEither,
    LFEither,
)
from .waltz import ReverseTurn


BackwardChangeRF = Figure(
    # This is equivalent to Backward Change - Reverse to Natural
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Backward Change RF",
    start_foot=RFBwd,
    next_foot=LFBwd,
)

BackwardChangeLF = Figure(
    # This is equivalent to Backward Change - Natural to Reverse
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Backward Change LF",
    start_foot=LFBwd,
    next_foot=RFBwd,
)

ContraCheck = Figure(
    # Thinking of Viennese Waltz
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Contra Check",
    start_foot=LFFwd,
    next_foot=RFBwd,
)

ReverseFleckerl = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Reverse Fleckerl",
    start_foot=LFFwd,
    next_foot=LFEither,
)

NaturalFleckerl = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Fleckerl",
    start_foot=RFBwd,
    next_foot=RFEither,
)

NaturalTurn4_6 = Figure(
    # (NaturalTurn):
    name="4-6 of Natural Turn",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

ReverseTurn4_6 = ReverseTurn.copy()
ReverseTurn4_6.name = "4-6 of Reverse Turn"
ReverseTurn4_6.start_foot = RFBwd
