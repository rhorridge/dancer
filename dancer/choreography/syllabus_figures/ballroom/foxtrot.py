"""Exports figures that are typically from the Foxtrot syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    RFFwd,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
    PromenadePosition,
)

FeatherStep = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Feather Step",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

ThreeStep = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Three Step",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

ChangeOfDirection = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Change of Direction",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

ReverseTurn1_3 = Figure(
    # (ReverseTurn):
    name="1-3 of Reverse Turn",
    start_foot=LFFwd,
    next_foot=RFBwd,
)

FeatherFinish = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Feather Finish",
    start_foot=RFBwd,
    next_foot=LFFwd,
)

NaturalWeave = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Weave",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

BasicWeave2 = Figure(
    # A second basic weave? slightly different start / end
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Basic Weave (Foxtrot)",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

ReverseWave = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Reverse Wave",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

ReverseWave1_4 = ReverseWave.copy()
ReverseWave1_4.name = "1-4 of Reverse Wave"
ReverseWave1_4.next_foot = LFEither

FeatherEnding = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Feather Ending",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
)

TopSpin = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Top Spin",
    start_foot=LFBwd,
    next_foot=LFFwd,
)

HoverFeather = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Hover Feather",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

HoverTelemark = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Hover Telemark",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

NaturalTelemark = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Telemark",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

HoverCross = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Hover Cross",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

OutsideSwivel = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Outside Swivel (Foxtrot)",
    start_foot=LFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

WeaveFromPP2 = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Weave from PP (Foxtrot)",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
)

NaturalTwistTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Twist Turn",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

QuickNaturalWeaveFromPP = Figure(
    # (NaturalFigure, StartPP, FinishClosed):
    name="Quick Natural Weave from PP",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
)

CurvedFeather = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Curved Feather",
    start_foot=RFFwd,
    next_foot=LFBwd,
)

BackFeather = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Back Feather",
    start_foot=LFBwd,
    next_foot=RFBwd,
)

NaturalZigZagFromPP = Figure(
    # (NaturalFigure, StartPP, FinishClosed):
    name="Natural Zig-Zag from PP",
    start_foot=RFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
)

NaturalHoverTelemark = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Hover Telemark",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

BounceFallawayWithWeaveEnding = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Bounce Fallaway with Weave Ending",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

ExtendedReverseWave = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Extended Reverse Wave",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

CurvedThreeStep = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Curved Three Step",
    start_foot=LFFwd,
    next_foot=RFFwd,
)
