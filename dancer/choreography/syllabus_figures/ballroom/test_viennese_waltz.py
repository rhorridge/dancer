"""Unit tests for .viennese_waltz.
"""

from dancer.choreography.feet import (
    LFFwd,
    LFBwd,
    LFEither,
    RFBwd,
    RFFwd,
    RFEither,
)
from dancer.choreography.position import ClosedPosition
from . import viennese_waltz


def test_backwardchangerf():
    assert viennese_waltz.BackwardChangeRF.name == "Backward Change RF"
    assert viennese_waltz.BackwardChangeRF.start_foot == RFBwd
    assert viennese_waltz.BackwardChangeRF.next_foot == LFBwd
    assert viennese_waltz.BackwardChangeRF.start_position == ClosedPosition
    assert viennese_waltz.BackwardChangeRF.next_position == ClosedPosition


def test_backwardchangelf():
    assert viennese_waltz.BackwardChangeLF.name == "Backward Change LF"
    assert viennese_waltz.BackwardChangeLF.start_foot == LFBwd
    assert viennese_waltz.BackwardChangeLF.next_foot == RFBwd
    assert viennese_waltz.BackwardChangeLF.start_position == ClosedPosition
    assert viennese_waltz.BackwardChangeLF.next_position == ClosedPosition


def test_contracheck():
    assert viennese_waltz.ContraCheck.name == "Contra Check"
    assert viennese_waltz.ContraCheck.start_foot == LFFwd
    assert viennese_waltz.ContraCheck.next_foot == RFBwd
    assert viennese_waltz.ContraCheck.start_position == ClosedPosition
    assert viennese_waltz.ContraCheck.next_position == ClosedPosition


def test_reversefleckerl():
    assert viennese_waltz.ReverseFleckerl.name == "Reverse Fleckerl"
    assert viennese_waltz.ReverseFleckerl.start_foot == LFFwd
    assert viennese_waltz.ReverseFleckerl.next_foot == LFEither
    assert viennese_waltz.ReverseFleckerl.start_position == ClosedPosition
    assert viennese_waltz.ReverseFleckerl.next_position == ClosedPosition


def test_naturalfleckerl():
    assert viennese_waltz.NaturalFleckerl.name == "Natural Fleckerl"
    assert viennese_waltz.NaturalFleckerl.start_foot == RFBwd
    assert viennese_waltz.NaturalFleckerl.next_foot == RFEither
    assert viennese_waltz.NaturalFleckerl.start_position == ClosedPosition
    assert viennese_waltz.NaturalFleckerl.next_position == ClosedPosition


def test_naturalturn4_6():
    assert viennese_waltz.NaturalTurn4_6.name == "4-6 of Natural Turn"
    assert viennese_waltz.NaturalTurn4_6.start_foot == LFBwd
    assert viennese_waltz.NaturalTurn4_6.next_foot == RFFwd
    assert viennese_waltz.NaturalTurn4_6.start_position == ClosedPosition
    assert viennese_waltz.NaturalTurn4_6.next_position == ClosedPosition


def test_reverseturn4_6():
    assert viennese_waltz.ReverseTurn4_6.name == "4-6 of Reverse Turn"
    assert viennese_waltz.ReverseTurn4_6.start_foot == RFBwd
    assert viennese_waltz.ReverseTurn4_6.next_foot == LFFwd
    assert viennese_waltz.ReverseTurn4_6.start_position == ClosedPosition
    assert viennese_waltz.ReverseTurn4_6.next_position == ClosedPosition
