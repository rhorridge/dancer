"""Unit tests for .tango.
"""

from dancer.choreography.feet import (
    LFFwd,
    LFBwd,
    LFEither,
    RFBwd,
    RFEither,
)
from dancer.choreography.position import (
    ClosedPosition,
    PromenadePosition,
)
from . import tango


def test_progressivesidestep():
    assert tango.ProgressiveSideStep.name == "Progressive Side Step"
    assert tango.ProgressiveSideStep.start_foot == LFFwd
    assert tango.ProgressiveSideStep.next_foot == RFEither
    assert tango.ProgressiveSideStep.start_position == ClosedPosition
    assert tango.ProgressiveSideStep.next_position == ClosedPosition


def test_progressivelink():
    assert tango.ProgressiveLink.name == "Progressive Link"
    assert tango.ProgressiveLink.start_foot == LFFwd
    assert tango.ProgressiveLink.next_foot == LFFwd
    assert tango.ProgressiveLink.start_position == ClosedPosition
    assert tango.ProgressiveLink.next_position == PromenadePosition


def test_closedpromenade():
    assert tango.ClosedPromenade.name == "Closed Promenade"
    assert tango.ClosedPromenade.start_foot == LFFwd
    assert tango.ClosedPromenade.next_foot == LFEither
    assert tango.ClosedPromenade.start_position == PromenadePosition
    assert tango.ClosedPromenade.next_position == ClosedPosition


def test_openpromenade():
    assert tango.OpenPromenade.name == "Open Promenade"
    assert tango.OpenPromenade.start_foot == LFFwd
    assert tango.OpenPromenade.next_foot == LFEither
    assert tango.OpenPromenade.start_position == PromenadePosition
    assert tango.OpenPromenade.next_position == ClosedPosition


def test_naturalrockturn():
    assert tango.NaturalRockTurn.name == "Natural Rock Turn"
    assert tango.NaturalRockTurn.start_foot == LFFwd
    assert tango.NaturalRockTurn.next_foot == RFBwd
    assert tango.NaturalRockTurn.start_position == PromenadePosition
    assert tango.NaturalRockTurn.next_position == ClosedPosition


def test_backcorte():
    assert tango.BackCorte.name == "Back Corte"
    assert tango.BackCorte.start_foot == RFBwd
    assert tango.BackCorte.next_foot == LFEither
    assert tango.BackCorte.start_position == ClosedPosition
    assert tango.BackCorte.next_position == ClosedPosition


def test_progressivesidestepreverseturn():
    assert (
        tango.ProgressiveSideStepReverseTurn.name
        == "Progressive Side Step Reverse Turn"
    )
    assert tango.ProgressiveSideStepReverseTurn.start_foot == LFFwd
    assert tango.ProgressiveSideStepReverseTurn.next_foot == LFEither
    assert tango.ProgressiveSideStepReverseTurn.start_position == ClosedPosition
    assert tango.ProgressiveSideStepReverseTurn.next_position == ClosedPosition


def test_rockonlf():
    assert tango.RockOnLF.name == "Rock on LF"
    assert tango.RockOnLF.start_foot == LFEither
    assert tango.RockOnLF.next_foot == RFEither
    assert tango.RockOnLF.start_position == ClosedPosition
    assert tango.RockOnLF.next_position == ClosedPosition


def test_rockonrf():
    assert tango.RockOnRF.name == "Rock on RF"
    assert tango.RockOnRF.start_foot == RFEither
    assert tango.RockOnRF.next_foot == LFEither
    assert tango.RockOnRF.start_position == ClosedPosition
    assert tango.RockOnRF.next_position == ClosedPosition


def test_naturalpromenadeturn():
    assert tango.NaturalPromenadeTurn.name == "Natural Promenade Turn"
    assert tango.NaturalPromenadeTurn.start_foot == LFFwd
    assert tango.NaturalPromenadeTurn.next_foot == LFFwd
    assert tango.NaturalPromenadeTurn.start_position == PromenadePosition
    assert tango.NaturalPromenadeTurn.next_position == PromenadePosition


def test_promenadelink():
    assert tango.PromenadeLink.name == "Promenade Link"
    assert tango.PromenadeLink.start_foot == RFBwd
    assert tango.PromenadeLink.next_foot == LFFwd
    assert tango.PromenadeLink.start_position == ClosedPosition
    assert tango.PromenadeLink.next_position == PromenadePosition


def test_fourstep():
    assert tango.FourStep.name == "Four Step"
    assert tango.FourStep.start_foot == LFFwd
    assert tango.FourStep.next_foot == LFEither
    assert tango.FourStep.start_position == ClosedPosition
    assert tango.FourStep.next_position == ClosedPosition


def test_backopenpromenade():
    assert tango.BackOpenPromenade.name == "Back Open Promenade"
    assert tango.BackOpenPromenade.start_foot == LFBwd
    assert tango.BackOpenPromenade.next_foot == LFFwd
    assert tango.BackOpenPromenade.start_position == ClosedPosition
    assert tango.BackOpenPromenade.next_position == PromenadePosition


def test_fourstepchange():
    assert tango.FourStepChange.name == "Four Step Change"
    assert tango.FourStepChange.start_foot == LFFwd
    assert tango.FourStepChange.next_foot == LFFwd
    assert tango.FourStepChange.start_position == ClosedPosition
    assert tango.FourStepChange.next_position == ClosedPosition


def test_brushtap():
    assert tango.BrushTap.name == "Brush Tap"
    assert tango.BrushTap.start_foot == LFEither
    assert tango.BrushTap.next_foot == LFEither
    assert tango.BrushTap.start_position == ClosedPosition
    assert tango.BrushTap.next_position == ClosedPosition


def test_fallawayfourstep():
    assert tango.FallawayFourStep.name == "Fallaway Four Step"
    assert tango.FallawayFourStep.start_foot == LFFwd
    assert tango.FallawayFourStep.next_foot == LFEither
    assert tango.FallawayFourStep.start_position == ClosedPosition
    assert tango.FallawayFourStep.next_position == ClosedPosition


def test_thechase():
    assert tango.TheChase.name == "The Chase"
    assert tango.TheChase.start_foot == LFFwd
    assert tango.TheChase.next_foot == LFEither
    assert tango.TheChase.start_position == PromenadePosition
    assert tango.TheChase.next_position == ClosedPosition


def test_opentopromenade():
    assert tango.OpenToPromenade.name == "Open to Promenade"
    assert tango.OpenToPromenade.start_foot == LFEither
    assert tango.OpenToPromenade.next_foot == LFFwd
    assert tango.OpenToPromenade.start_position == ClosedPosition
    assert tango.OpenToPromenade.next_position == PromenadePosition


def test_fivestep():
    assert tango.FiveStep.name == "Five Step"
    assert tango.FiveStep.start_foot == LFFwd
    assert tango.FiveStep.next_foot == LFFwd
    assert tango.FiveStep.start_position == ClosedPosition
    assert tango.FiveStep.next_position == PromenadePosition


def test_minifivestep():
    assert tango.MiniFiveStep.name == "Mini Five Step"
    assert tango.FiveStep.start_foot == LFFwd
    assert tango.FiveStep.next_foot == LFFwd
    assert tango.FiveStep.start_position == ClosedPosition
    assert tango.FiveStep.next_position == PromenadePosition


def test_outsideswivel2():
    assert tango.OutsideSwivel2.name == "Outside Swivel (Tango)"
    assert tango.OutsideSwivel2.start_foot == LFBwd
    assert tango.OutsideSwivel2.next_foot == LFFwd
    assert tango.OutsideSwivel2.start_position == ClosedPosition
    assert tango.OutsideSwivel2.next_position == PromenadePosition
