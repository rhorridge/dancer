"""Exports figures that are typically from the Tango syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    LFFwd,
    RFBwd,
    LFBwd,
    RFEither,
    LFEither,
    PromenadePosition,
)

ProgressiveSideStep = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Progressive Side Step",
    start_foot=LFFwd,
    next_foot=RFEither,
)

ProgressiveLink = Figure(
    # (ReverseFigure, StartClosed, FinishPP):
    name="Progressive Link",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

ClosedPromenade = Figure(
    # (ReverseFigure, StartPP, FinishClosed):
    name="Closed Promenade",
    start_foot=LFFwd,
    next_foot=LFEither,
    start_position=PromenadePosition,
)

OpenPromenade = Figure(
    # We need to add an open position
    # (ReverseFigure, StartPP, FinishClosed):
    name="Open Promenade",
    start_foot=LFFwd,
    next_foot=LFEither,
    start_position=PromenadePosition,
)

NaturalRockTurn = Figure(
    # (NaturalFigure, StartPP, FinishClosed):
    name="Natural Rock Turn",
    start_foot=LFFwd,
    next_foot=RFBwd,
    start_position=PromenadePosition,
)

BackCorte = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Back Corte",
    start_foot=RFBwd,
    next_foot=LFEither,
)

ProgressiveSideStepReverseTurn = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Progressive Side Step Reverse Turn",
    start_foot=LFFwd,
    next_foot=LFEither,
)

RockOnLF = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Rock on LF",
    start_foot=LFEither,
    next_foot=RFEither,
)

RockOnRF = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Rock on RF",
    start_foot=RFEither,
    next_foot=LFEither,
)

NaturalPromenadeTurn = Figure(
    # (NaturalFigure, StartPP, FinishPP):
    name="Natural Promenade Turn",
    start_foot=LFFwd,
    next_foot=LFFwd,
    start_position=PromenadePosition,
    next_position=PromenadePosition,
)

PromenadeLink = Figure(
    # TODO this is probably wrong
    # (ReverseFigure, StartClosed, FinishPP):
    name="Promenade Link",
    start_foot=RFBwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

FourStep = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Four Step",
    start_foot=LFFwd,
    next_foot=LFEither,
)

BackOpenPromenade = Figure(
    # TODO idk
    # (ReverseFigure, StartClosed, FinishPP):
    name="Back Open Promenade",
    start_foot=LFBwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

FallawayPromenade = Figure(
    # TODO idk
    # (ReverseFigure, StartClosed, FinishPP):
    name="Back Open Promenade",
    start_foot=LFBwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

FourStepChange = Figure(
    # TODO is this a four step basically?
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Four Step Change",
    start_foot=LFFwd,
    next_foot=LFFwd,
)

BrushTap = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Brush Tap",
    start_foot=LFEither,
    next_foot=LFEither,
)

FallawayFourStep = Figure(
    # TODO check footwork
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Fallaway Four Step",
    start_foot=LFFwd,
    next_foot=LFEither,
)

TheChase = Figure(
    # TODO check footwork
    # (ReverseFigure, StartPP, FinishClosed):
    name="The Chase",
    start_foot=LFFwd,
    next_foot=LFEither,
    start_position=PromenadePosition,
)

OpenToPromenade = Figure(
    # """Open to PP from closed."""
    # (NaturalFigure, StartClosed, FinishPP):
    name="Open to Promenade",
    start_foot=LFEither,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

FiveStep = Figure(
    # (ReverseFigure, StartClosed, FinishPP):
    name="Five Step",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

MiniFiveStep = Figure(
    # (ReverseFigure, StartClosed, FinishPP):
    name="Mini Five Step",
    start_foot=LFFwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)

OutsideSwivel2 = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Outside Swivel (Tango)",
    start_foot=LFBwd,
    next_foot=LFFwd,
    next_position=PromenadePosition,
)
