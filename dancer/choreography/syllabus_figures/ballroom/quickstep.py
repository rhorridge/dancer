"""Exports figures that are typically from the Quickstep syllabus.
"""

from dancer.steps import Figure
from dancer.choreography import (
    RFFwd,
    RFRight,
    RFAny,
    LFFwd,
    RFBwd,
    LFBwd,
    LFEither,
    RFEither,
    PromenadePosition,
)
from .waltz import (
    ForwardLockStep,
    BackwardLockStep,
)

WalkRF = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Walk RF",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

WalkLF = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Walk LF",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

ProgressiveChasseToL = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Progressive Chasse",
    start_foot=RFEither,
    next_foot=RFEither,
)

NaturalPivotTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Natural Pivot Turn",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

TippleChasseToR = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Tipple Chasse to R",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

TippleChasseToR1_4 = TippleChasseToR.copy()
TippleChasseToR1_4.name = "1-4 of Tipple Chasse to R"
TippleChasseToR1_4.next_foot = LFEither

TippleChasseToR2_4 = TippleChasseToR1_4.copy()
TippleChasseToR2_4.name = "2-4 of Tipple Chasse to R"
TippleChasseToR2_4.start_foot = RFRight

RunningFinish = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Running Finish",
    start_foot=LFBwd,
    next_foot=RFFwd,
)

OpenRunningFinish = Figure(
    # (NaturalFigure, StartClosed, FinishPP):
    name="Open Running Finish",
    start_foot=LFBwd,
    next_foot=RFFwd,
    next_position=PromenadePosition,
)

QuickOpenReverse = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Quick Open Reverse",
    start_foot=RFFwd,
    next_foot=RFBwd,
)

ZigZag = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Zig-Zag",
    start_foot=RFFwd,
    next_foot=LFFwd,
)

Fishtail = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Fishtail",
    start_foot=RFFwd,
    next_foot=RFFwd,
)

RunningRightTurn = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Running Right Turn",
    start_foot=RFFwd,
    next_foot=RFFwd,
)

FourQuickRun = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Four Quick Run",
    start_foot=RFBwd,
    next_foot=RFFwd,
)

V6 = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="V-Six",
    start_foot=RFBwd,
    next_foot=RFFwd,
)

TippleChasseToL = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    # TODO
    name="Tipple Chasse to L",
    start_foot=RFFwd,
    next_foot=LFBwd,
)

TippleChasseToL1_4 = TippleChasseToL.copy()
TippleChasseToL1_4.name = "1-4 of Tipple Chasse to L"
TippleChasseToL1_4.next_foot = RFAny


CrossSwivel = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Cross Swivel",
    start_foot=LFFwd,
    next_foot=RFEither,
)

RunningCrossChasse = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Running Cross Chasse",
    start_foot=LFFwd,
    next_foot=RFFwd,
)

PassingNaturalTurn = Figure(
    # (NaturalFigure, StartPP, FinishClosed):
    name="Passing Natural Turn",
    start_foot=RFFwd,
    next_foot=LFBwd,
    start_position=PromenadePosition,
)

SixQuickRun = Figure(
    # (ReverseFigure, StartClosed, FinishClosed):
    name="Six Quick Run",
    start_foot=RFBwd,
    next_foot=RFFwd,
)

RumbaCross = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Rumba Cross",
    start_foot=RFFwd,
    next_foot=RFFwd,
)

TipsyToR = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Tipsy to R",
    start_foot=LFBwd,
    next_foot=LFFwd,
)

ForwardLockStep2_4 = ForwardLockStep.copy()
ForwardLockStep2_4.name = "2-4 of Forward Lock Step"
ForwardLockStep2_4.start_foot = LFFwd

BackwardLockStep2_4 = BackwardLockStep.copy()
BackwardLockStep2_4.name = "2-4 of Backward Lock Step"
BackwardLockStep2_4.start_foot = RFBwd

TipsyToL = Figure(
    # (NaturalFigure, StartClosed, FinishClosed):
    name="Tipsy to L",
    start_foot=RFFwd,
    next_foot=RFBwd,
)

TipsyToL2_4 = TipsyToL.copy()
TipsyToL2_4.name = "2-4 of Tipsy to L"
TipsyToL2_4.start_foot = LFEither
