"""Exports different positions that may be used.
"""

from dancer.steps import Position


ClosedPosition = Position.CLOSED_POSITION
PromenadePosition = Position.PROMENADE_POSITION
OpenPromenadePosition = Position.OPEN_PROMENADE_POSITION
OpenCounterPromenadePosition = Position.OPEN_COUNTER_PROMENADE_POSITION
OpenPosition = Position.OPEN_POSITION
FanPosition = Position.FAN_POSITION
LeftContraPosition = Position.CONTRA_POSITION_L
RightContraPosition = Position.CONTRA_POSITION_R
RightSidePosition = Position.SIDE_POSITION_R
RightShadowPosition = Position.SHADOW_POSITION_R
ContactPosition = Position.CONTACT_POSITION
InvertedPromenadePosition = Position.INVERTED_PROMENADE_POSITION
InvertedCounterPromenadePosition = Position.INVERTED_COUNTER_PROMENADE_POSITION
