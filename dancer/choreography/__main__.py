#!/usr/bin/env python3
"""Print all figures as JSON.
"""

from pydantic import BaseModel
from . import Figure, ALL_FIGURES


class FigureGroup(BaseModel):
    __root__: list[Figure]


if __name__ == "__main__":
    group = FigureGroup(__root__=ALL_FIGURES)
    print(group.json(indent=4, exclude_none=True))
