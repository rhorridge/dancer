"""Unit tests for syllabus.
"""

from . import syllabus
import dancer.choreography as stp


class TestWaltzSyllabus:
    def test_steps(self):
        assert stp.waltz.ClosedChangeRF in syllabus.WaltzSyllabus.figures
        assert stp.waltz.BasicWeave in syllabus.WaltzSyllabus.figures
        assert stp.waltz.DoubleReverseSpin in syllabus.WaltzSyllabus.figures
        assert stp.waltz.Whisk in syllabus.WaltzSyllabus.figures

    def test_routine(self):
        s = syllabus.WaltzSyllabus
        r = s.routine()
        assert len(r) == 10
        for elem in r:
            assert elem in s.figures


class TestQuickstepSyllabus:
    def test_steps(self):
        assert stp.quickstep.QuickOpenReverse in syllabus.QuickstepSyllabus.figures
        assert stp.quickstep.RunningFinish in syllabus.QuickstepSyllabus.figures
        assert stp.waltz.NaturalSpinTurn in syllabus.QuickstepSyllabus.figures

    def test_routine(self):
        s = syllabus.QuickstepSyllabus
        r = s.routine()
        assert len(r) == 10
        for elem in r:
            assert elem in s.figures


class TestFoxtrotSyllabus:
    def test_steps(self):
        assert stp.quickstep.QuickOpenReverse in syllabus.FoxtrotSyllabus.figures
        assert stp.foxtrot.FeatherStep in syllabus.FoxtrotSyllabus.figures
        assert stp.foxtrot.ThreeStep in syllabus.FoxtrotSyllabus.figures

    def test_routine(self):
        s = syllabus.FoxtrotSyllabus
        r = s.routine()
        assert len(r) == 10
        for elem in r:
            assert elem in s.figures


class TestTangoSyllabus:
    def test_steps(self):
        assert stp.tango.FourStep in syllabus.TangoSyllabus.figures
        assert stp.tango.TheChase in syllabus.TangoSyllabus.figures
        assert stp.tango.ProgressiveSideStep in syllabus.TangoSyllabus.figures
        assert stp.tango.ProgressiveLink in syllabus.TangoSyllabus.figures

    def test_routine(self):
        s = syllabus.TangoSyllabus
        r = s.routine()
        assert len(r) == 10
        for elem in r:
            assert elem in s.figures
