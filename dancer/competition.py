"""Represents a dance competition.

This module exports various classes that can be used
together to represent the running of a dance competition:

- Name, venue and date of the dancesport competition
- Competitors
- Adjudicators
- Events and their constituent dances
- Entries to said events
- Marks awarded to said competitors in each dance in each event by each adjudicator
- Whether a competitor couple is recalled to the next round
- Placings awarded to competitors in a final by each judge
- Reporting the overall placing in an individual event

In combination with `dancer.scrutineer`, the marks awarded may
be transformed via the Skating System rules 1-11 to independently
report the overall competition performance.
"""

import datetime
import logging
from pydantic import BaseModel, validate_arguments, HttpUrl
from typing import List, Dict, Optional
from dancer.couple import Couple
from dancer.dance import Dance
from .league import League
from .age_restriction import AgeRestriction


LOG = logging.getLogger(__name__)


class Adjudicator(BaseModel):
    name: str

    def to_sqlalchemy(self):
        return self.dict()


class Marks(BaseModel):
    __root__: str

    def __len__(self) -> int:
        return len(self.__root__)

    def __str__(self) -> str:
        return self.__root__


@validate_arguments
def create_marks(marks: str):
    """Create a marks instance from a string of who has marked."""

    return Marks(__root__=marks)


class CoupleRound(BaseModel):
    marks: List[Marks] = []
    recalled: bool = False

    def fmt_marks(self, judges) -> str:
        """Format marks, for the number of judges shown."""

        lst = []
        for mark in self.marks:
            strn = ""
            for judge in judges:
                if str(judge) in str(mark):
                    strn += "%s " % "✘"
                else:
                    strn += "  "
            lst.append(strn[:-1])
        return lst

    def fmt_recall(self) -> str:
        """Format the recall given to this couple."""

        if not self.recalled:
            return " "
        return "✓"


class Round(BaseModel):
    couples: Dict[int, CoupleRound]

    def __len__(self) -> int:
        """Return the number of couples in this round."""

        return len(self.couples)

    def marks(self) -> dict[int, list[str]]:
        """Return the marks from this round, for input into dancer.scrutineer.recall."""

        return {
            key: [str(mark) for mark in val.marks] for key, val in self.couples.items()
        }

    def __eq__(self, other) -> bool:
        """Determine if two rounds are equal."""

        if len(self) != len(other):
            LOG.debug("%d != %d", len(self), len(other))
            return False
        for key, val in self.couples.items():
            oth = other.couples[key]
            for dance1, dance2 in zip(val.marks, oth.marks):
                if set(dance1) != set(dance2):
                    LOG.debug(
                        "%d marks: %s %s",
                        key,
                        dance1,
                        dance2,
                    )
                    return False
            if val.recalled != oth.recalled:
                LOG.debug(
                    "%d recall: %s %s",
                    key,
                    val.recalled,
                    other.couples[key].recalled,
                )
                return False
        return True

    def couples_recalled(self) -> set[int]:
        """Return a set of couples recalled."""

        return set([couple for couple, marks in self.couples.items() if marks.recalled])

    def __str__(self) -> str:
        return "Recalled %d from %d" % (len(self.couples_recalled()), len(self))


class Placings(BaseModel):
    __root__: dict[str, int]

    def fmt(self) -> str:
        """Format the placings for printing out."""

        return " ".join(["%s" % val for val in self.__root__.values()])


class CoupleFinal(BaseModel):
    placings: List[Placings] = []
    final_place: Optional[int] = None

    def add_placing(self, placing: Placings):
        """Add a placing for a single dance."""

        self.placings.append(placing)

    def add_final_place(self, place: int):
        """Add a final place."""

        if self.final_place:
            raise ValueError("This couple already has a place: %d" % self.final_place)
        self.final_place = place

    def fmt_placings(self) -> List[str]:
        """Format the placings for this final."""

        return [plc.fmt() for plc in self.placings]

    def fmt_final_place(self) -> str:
        """Format the final placing given to this couple."""

        if not self.final_place:
            return " "
        return str(self.final_place)


class Final(BaseModel):
    couples: Dict[int, CoupleFinal]

    def __len__(self) -> int:
        """Return the number of couples in this final."""

        return len(self.couples)

    def __str__(self) -> str:
        """Format the couples in the final."""

        couples = ", ".join([str(key) for key in sorted(self.couples.keys())])
        return f"{len(self.couples)} in final: {couples}"

    def list_couples(self) -> list[int]:
        """Return a list of all couples in this final."""

        return [key for key in sorted(self.couples.keys())]

    def placings_table(self) -> str:
        """Return the placings as a table."""

        return "\n".join(
            [
                "%3d: %s -> %s"
                % (
                    key,
                    "\t".join(
                        [
                            " ".join(
                                [str(at) for _, at in sorted(row.__root__.items())]
                            )
                            for row in val.placings
                        ],
                    ),
                    val.final_place,
                )
                for key, val in sorted(self.couples.items())
            ]
        )

    def placings(self) -> dict[int, list[dict[str, int]]]:
        """Return the placings for each couple for each dance by each judge as a dictionary."""

        return {
            key: [row.__root__ for row in val.placings]
            for key, val in self.couples.items()
        }

    def final_placings(self) -> dict[int, int]:
        """Return the final placings for the final as a dictionary.

        Returns:
             a dictionary of {couple: place}.
        """

        return {
            number: couple.final_place
            for number, couple in self.couples.items()
            if couple.final_place
        }


class Category(BaseModel):
    name: str
    dances: List[Dance]
    restricted: bool = False
    league: League = League.UNIVERSITY_CIRCUIT
    age_restriction: Optional[AgeRestriction] = None

    def __str__(self) -> str:
        """Return a human-readable representation."""

        dances = "".join([dance.abbr for dance in self.dances])
        restricted = ""
        if self.restricted:
            restricted = " ⛡"
        strn = f"{self.name} [{dances}]{restricted}"
        if self.age_restriction:
            strn = f"{str(self.age_restriction)} {strn}"
        return strn


class Event(BaseModel):
    dance: Category
    entries: List[int] = []
    rounds: Dict[int, Round] = {}
    final: Optional[Final]

    def __len__(self):
        return len(self.dance.dances)

    def fmt_dances(self) -> str:
        """Format the dances as a string."""

        return ", ".join([dance.name for dance in self.dance.dances])


# TODO merge these two classes
class CompetitionDetails(BaseModel):
    name: str
    venue: Optional[str]
    date: datetime.date
    date_finish: Optional[datetime.date]
    url: Optional[HttpUrl]

    @property
    def id(self) -> str:
        """Return a unique string identifier for this event."""

        return f"{self.date}:{self.name}"

    def __str__(self):
        fmteddate = self.date.strftime("%a")
        return f"{fmteddate} {self.date} {self.name}"

    def scrape(self, headers: dict) -> None:
        """Scrape the competition details for this
        competition from the configured URL.
        """

        with requests.Session() as sess:
            res = sess.get(
                self.url,
                headers=headers,
            )
            print(len(res.text))

    def full_location(self) -> str:
        """Return the full location as a string."""

        if self.venue:
            return self.venue
        return None

    def html_description(self) -> str:
        """Return basic HTML description."""

        return "Dancesport Competition"


class Competition(BaseModel):
    name: str
    venue: Optional[str] = None
    date: Optional[datetime.date] = None
    adjudicators: Dict[str, Adjudicator] = {}
    events: Dict[int, Event] = {}
    competitors: Dict[int, Couple] = {}

    class Config:
        validate_assignment = True

    def __str__(self):
        """Nice human-readable format of this object."""

        if self.date:
            return f"{self.name} - {self.date}"
        return self.name

    def to_sqlalchemy(self) -> Dict:
        """Convert to an SQLAlchemy dict model."""

        return {
            "name": self.name,
            "venue": self.venue,
            "date": self.date,
        }

    def merge_competitors(self, competitors: dict[int, Couple]) -> None:
        """Merge competitors into the current competitiors file. Errors
        on existing competitor with different name!
        """

        for num, couple in competitors.items():
            if existing_couple := self.competitors.get(num):
                if couple != existing_couple:
                    raise RuntimeError(
                        "Existing couple %s is different to the new couple %s!"
                        % (
                            existing_couple,
                            couple,
                        )
                    )
                else:
                    continue
            self.competitors[num] = couple

    def competitor(self, number: int) -> Optional[Couple]:
        """Return a given couple by their number."""

        return self.competitors.get(number)

    def event(self, number: int) -> Optional[Event]:
        """Return a given event by its number."""

        return self.events.get(number)

    def adjudicator(self, letter: str) -> Optional[Adjudicator]:
        """Return a given adjudicator by their letter."""

        return self.adjudicators.get(letter)

    def fmt_adjudicators(self) -> str:
        """Format adjudicators for display."""

        return " ".join([str(judge) for judge in self.adjudicators.keys()])

    def adjudicator_string(self) -> str:
        """Format adjudictors for internal use."""

        return "".join(self.adjudicators.keys())

    def print_competitors(self):
        """Print a list of competitors."""

        print("* Competitors")
        print()
        depth = " "
        for number in sorted(self.competitors.keys()):
            print(
                "%s%4d. %s"
                % (
                    depth,
                    number,
                    str(self.competitor(number)),
                )
            )
        print()

    def print_adjudicators(self) -> str:
        """Print all adjudicators for this competition, in order."""

        print("* Adjudicators")
        print()
        depth = "  "
        for letter in sorted(self.adjudicators.keys()):
            print(
                "%s%s. %s"
                % (
                    depth,
                    letter,
                    self.adjudicator(letter).name,
                )
            )
        print()

    def print_events(self):
        """Print all events for this competition, in order."""

        print("* Events")
        print()
        depth = "  "
        for num in sorted(self.events.keys()):
            if num < 10:
                pad = "  "
            else:
                pad = " "
            print(
                "%s%d.%s%s"
                % (
                    depth,
                    num,
                    pad,
                    str(self.event(num).dance),
                )
            )
        print()

    def print_event(self, number: int):
        """Print a table showing the event information."""

        event = self.event(number)
        if not event:
            LOG.error("Event %d not found!", number)
            return
        num_dances = len(event)
        num_judges = len(self.adjudicators)
        depth = "  "
        print(
            "* Event %d :: %s :: %d entries"
            % (
                number,
                event.dance.name,
                len(event.entries),
            )
        )
        tmpfmt = ""
        if num_dances >= 2:
            tmpfmt = "s"
        print(
            "%sEvent has %d dance%s - %s"
            % (
                depth,
                num_dances,
                tmpfmt,
                event.fmt_dances(),
            )
        )
        print()

        print("* Entries")
        print()
        depth = " "
        for entry in event.entries:
            person = self.competitor(entry)
            if person:
                fmtperson = person.fmt()
            else:
                fmtperson = "NOT FOUND"
            print(
                "%s%4d. %s"
                % (
                    depth,
                    entry,
                    fmtperson,
                )
            )
        print()

        print("* Rounds")
        print()
        for num, rnd in event.rounds.items():
            print("** Round %d" % num)
            print()
            depth = " "
            title = "%s   #   " % depth
            title += "   ".join(
                ["%s" % (self.fmt_adjudicators()) for event in event.dance.dances]
            )
            title += "  R"
            print(title)
            for cplnum, couple in rnd.couples.items():
                line = "%s%4d.  " % (depth, cplnum)
                plcs = "   ".join(
                    ["%s" % plc for plc in couple.fmt_marks(self.adjudicator_string())]
                )
                if plcs == "":
                    plcs = "   ".join(
                        [
                            " ".join([" " for _ in range(num_judges)])
                            for event in event.dance.dances
                        ]
                    )
                line += plcs
                line += "  %s" % couple.fmt_recall()
                line += "  ┇ %s" % (self.competitor(cplnum) or "?")
                print(line)
            print()
        print()

        print("* Final")
        print()
        depth = " "
        title = "%s   #   " % depth
        title += "   ".join(
            ["%s" % (self.fmt_adjudicators()) for event in event.dance.dances]
        )
        title += "  P"
        print(title)
        for cplnum, couple in event.final.couples.items():
            line = "%s%4d.  " % (depth, cplnum)
            plcs = "   ".join(["%s" % plc for plc in couple.fmt_placings()])
            if plcs == "":
                plcs = "   ".join(
                    [
                        " ".join([" " for _ in range(num_judges)])
                        for event in event.dance.dances
                    ]
                )
            line += plcs
            line += "  %s" % couple.fmt_final_place()
            line += "  ┇ %s" % self.competitor(cplnum)
            print(line)
        print()

        print("* Placings")
        print()
        depth = " "
        title = "%s   #   " % depth
        title += "   ".join(
            ["%s" % (self.fmt_adjudicators()) for event in event.dance.dances]
        )
        title += "  P"
        print(title)
        for cplnum, couple in sorted(
            event.final.couples.items(), key=lambda val: val[1].final_place
        ):
            line = "%s%4d.  " % (depth, cplnum)
            plcs = "   ".join(["%s" % plc for plc in couple.fmt_placings()])
            if plcs == "":
                plcs = "   ".join(
                    [
                        " ".join([" " for _ in range(num_judges)])
                        for event in event.dance.dances
                    ]
                )
            line += plcs
            line += "  %s" % couple.fmt_final_place()
            line += "  ┇ %s" % self.competitor(cplnum)
            print(line)
        print()

        print(
            "* Event %d :: %s :: %d finalists"
            % (
                number,
                event.dance.name,
                len(event.final.couples),
            )
        )


if __name__ == "__main__":
    print("# Import the module.")
    print(
        "from dancer.competition import Competition, Adjudicator, Couple, Event, Round, Final"
    )
    print()
    print("# Declare a competition.")
    print('competition = Competition(name="Test Competition")')
    print()
    print("# Declare an adjudicator.")
    print('adjudicator = Adjudicator(name="A. Judge")')
    print()
    print("# Assign the adjudicator to the competition.")
    print('competition.adjudicators["A"] = adjudicator')
    print()
    print("# Set the competition venue.")
    print('competition.venue = "Local Sports Hall"')
    print()
    print("# Set the competition date.")
    print('competition.date = "2022-05-25"')
    print()
    print(
        "# Set up an event. We will import an existing category. It will be event number 1."
    )
    print("from dancer.competition_events import OpenF  # Open Foxtrot")
    print("event = Event(dance=OpenF)")
    print("competition.events[1] = event")
    print()
    print("# Enter a couple. They will have a number of 1.")
    print('couple = Couple(leader="A. Leader", follower="A. Follower")')
    print("competition.competitors[1] = couple")
    print()
    print("# Enter couple 1 into event 1.")
    print("competition.events[1].entries.append(1)")
    print()
    print("# Run a first round.")
    print(
        'round = Round(couples={1: {"marks": [{"marked_by": "A"}], "recalled": True}})'
    )
    print("competition.events[1].rounds[1] = round")
    print()
    print("# Run a final.")
    print(
        'final = Final(couples={1: {"placings": [{"placed_by": {"A": 1}}], "final_place": 1}})'
    )
    print("competition.events[1].final = final")
    print()
    print("# Display the adjudicators.")
    print("competition.print_adjudicators()")
    print()
    print("# Display the competitors.")
    print("competition.print_competitors()")
    print()
    print("# Display the events.")
    print("competition.print_events()")
    print()
    print("# Display event 1.")
    print("competition.print_event(1)")
