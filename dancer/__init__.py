"""Top level imports, including main program.
"""

import logging
import os
import argparse
from dancer.scrutineer.io.placings import PlacingsFile
from dancer.competition import Competition
from dancer import main


LOG = logging.getLogger(__name__)


def _comp(args: object):
    """Handle the `comp` program options."""

    if args.filename:
        # handle an input filename
        competition = Competition.parse_file(args.filename)
        comp_name = competition.name
    else:
        if args.download and args.name:
            if not args.easycomp_url:
                LOG.error(
                    "Cannot download results for %s; set --easycomp-url first!",
                    args.name,
                )
                return
            main.download(args.name, url=args.easycomp_url)
            main.preprocess(args.name)
            competition = main.parse(args.name)
        elif args.name:
            # try parsing the competition
            competition = main.parse(args.name)
        else:
            LOG.error("Require both --name and --download to be set!")
            return

    if args.print:
        # First - check specific print options
        if number := args.event_number:
            event = competition.event(number)
            if not event:
                LOG.error(
                    "Event %d not found for %s!",
                    number,
                    comp_name,
                )
                return
            if args.print_fmt == "json":
                print(event.json(indent=4, sort_keys=True))
                return
            elif args.print_fmt == "txt":
                # TODO this is actually org mode
                competition.print_event(number)
                return
            else:
                LOG.error("Unsupported output format: %s", args.print_fmt)
                return

        if args.print_fmt == "json":
            print(competition.json(indent=4, sort_keys=True))
        elif args.print_fmt == "txt":
            # TODO this is actually org mode
            competition.print_events()
            competition.print_adjudicators()
            competition.print_competitors()


def _scrut(args: object):
    """Handle the `scrut` program options."""

    if args.filename:
        placings = PlacingsFile(filename=args.filename)
        placings.validate()
        res = placings.place()
        if args.print:
            print(res.placings_table())


def environ_or_required(key: str) -> dict:
    """Retrieve a value from an environment variable, if it is present.

    Args:
        key: an environment variable name. Will be converted to upper case.

    Returns:
        keyword arguments that can be passed to `argparse.Parser.add_argument`.
    """

    key = key.upper()
    return (
        {"default": os.environ.get(key)} if os.environ.get(key) else {"required": True}
    )


def main(*args: list[str]) -> None:
    """Main program. Handle arguments passed."""

    parser = argparse.ArgumentParser(
        description="All-in-one dancesport tool",
        prog="dancer",
    )
    subparsers = parser.add_subparsers()
    parser.add_argument(
        "--print",
        action=argparse.BooleanOptionalAction,
        default=True,
        help="Print output to stdout",
    )
    parser.add_argument(
        "--print-fmt",
        type=str,
        choices=["json", "txt", "csv", "xml", "html", "org", "sql"],
        help="Format to be used for stdout output",
        default="json",
    )

    scrut = subparsers.add_parser("scrut", help="Run the scrutineering functionality")
    scrut.add_argument(
        "--filename",
        "-f",
        type=str,
        help="Filename containing marks",
    )
    scrut.set_defaults(func=_scrut)

    comp = subparsers.add_parser("comp", help="Download and report competition results")
    comp.add_argument(
        "--easycomp-url",
        type=str,
        help="URL to download Easycomp results from - required for online functionality",
        default=os.environ.get("EASYCOMP_URL"),
    )
    comp.add_argument(
        "--name",
        "-n",
        type=str,
        help="Name of the competition to download",
    )
    comp.add_argument(
        "--filename",
        "-f",
        type=str,
        help="Path to a filename containing competition results",
    )
    comp.add_argument(
        "--download",
        "-d",
        action=argparse.BooleanOptionalAction,
        default=False,
        help="Whether to attempt download the results from Easycomp",
    )
    comp.add_argument(
        "--event-number",
        "-e",
        type=int,
        help="Event number to be queried directly",
    )
    comp.add_argument(
        "--couple-number",
        "-c",
        type=int,
        help="Couple number to be queried directly",
    )
    comp.add_argument(
        "--adjudicator-letter",
        "-a",
        type=str,
        help="Adjudicator letter to be queried directly",
    )
    comp.set_defaults(func=_comp)

    args = parser.parse_args(args)
    args.func(args)
