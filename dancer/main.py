#!/usr/bin/env python3

"""This is a dance project.
"""

import out
import json
import os
import logging
import sys
import subprocess
from dancer import parse as _parse
from dancer.web import easycomp
from dancer.competition import Competition
from dancer.database.sqlite import SessionScope
from dancer.database import models
from typing import Optional

out.configure(
    level=os.environ.get("LOGLEVEL", "warning"),
    default_level="info",
)

LOG = logging.getLogger(__name__)


def fmt(jsn: str):
    return json.dumps(json.loads(jsn), indent=4)


def download(
    name: str,
    url: Optional[str] = None,
    directory: str = "data",
):
    """Download results for competition with given name from given URL, saving
    to the given directory.
    """

    if not url:
        url = os.environ.get("EASYCOMP_URL")
    if not url:
        LOG.error("EASYCOMP_URL is not set!")
        return
    easycomp.download_comp_results(url, name, directory)


def preprocess(name: str, directory: str = "data"):
    """Preprocess PDF files in the downloaded directory."""

    data_dir = os.path.join(directory, name)

    res = subprocess.run(
        [
            "./scripts/convert_pdf_in_dir.sh",
            data_dir,
        ],
        capture_output=True,
    )
    if res.stdout:
        for row in res.stdout.decode("utf").split("\n"):
            LOG.debug(row)
    if res.stderr:
        for row in res.stderr.decode("utf").split("\n"):
            LOG.error(row)
    res = subprocess.run(
        [
            "find",
            data_dir,
            "-type",
            "f",
            "-name",
            "Competitors.pdf",
            "-exec",
            "pdftotext",
            "{}",
            "+",
        ]
    )
    if res.stdout:
        for row in res.stdout.decode("utf").split("\n"):
            LOG.debug(row)
    if res.stderr:
        for row in res.stderr.decode("utf").split("\n"):
            LOG.error(row)


def parse(name: str, directory: str = "data") -> Competition:
    """
    Parse a given competition directory. Will raise exception if directory
    doesn't exist.
    """

    data_dir = os.path.join(directory, name)

    comp = Competition(name=name)
    comp.competitors = _parse.competitors(data_dir)
    comp.events = _parse.events(data_dir)
    comp.adjudicators = _parse.judges(data_dir)
    for event in comp.events.keys():
        comp.events[event].rounds = _parse.rounds(data_dir, event)
        comp.events[event].final = _parse.final(data_dir, event)
        if comp.events[event].final:
            for couple in comp.events[event].final.list_couples():
                if couple not in comp.competitors:
                    comp.merge_competitors(_parse.competitors_in_final(data_dir, event))

    return comp


def main(*args):
    if len(args) < 2:
        LOG.error("Expected competition name as first argument! %s")
        sys.exit(1)
    comp_name = args[1]

    if comp_name.endswith(".json"):
        comp = Competition.parse_file(comp_name)
        comp_name = comp.name
    else:
        download(comp_name)
        preprocess(comp_name)
        comp = parse(comp_name)

    if len(args) > 2:
        if args[2] == "json":
            print(comp.json())
        elif args[2] == "print":
            comp.print_events()
            comp.print_adjudicators()
            comp.print_competitors()
        else:
            comp.print_event(int(args[2]))

    # TODO copy this out
    with SessionScope() as sess:
        comp_ = models.Competition(**comp.to_sqlalchemy())
        judges_ = {
            key: models.Adjudicator(**judge.to_sqlalchemy())
            for key, judge in comp.adjudicators.items()
        }
        comp_judges_ = [
            models.CompetitionAdjudicator(letter=key) for key in judges_.keys()
        ]
        for judge, comp_judge in zip(judges_.values(), comp_judges_):
            comp_.competition_adjudicator.append(comp_judge)
            comp_judge.adjudicator = judge

        competitors_ = {
            number: models.Couple(**couple.to_sqlalchemy())
            for number, couple in comp.competitors.items()
        }
        dancers_ = {}
        comp_entries_ = {}
        for entry, num, couple in zip(
            competitors_.values(), comp.competitors.keys(), comp.competitors.values()
        ):
            already = set(dancers_.keys())
            if couple.leader not in already:
                leader = models.Dancer(name=couple.leader)
                dancers_[couple.leader] = leader
            else:
                leader = dancers_[couple.leader]
            if couple.follower not in already:
                follower = models.Dancer(name=couple.follower)
                dancers_[couple.follower] = follower
            else:
                follower = dancers_[couple.follower]
            entry.leader.append(leader)
            entry.follower.append(follower)

            comp_entries_[num] = models.CompetitionEntry(number=num)
            comp_entries_[num].couple = entry
            comp_.entry.append(comp_entries_[num])


if __name__ == "__main__":
    main(*sys.argv)
