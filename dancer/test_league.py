"""Unit tests for dancer.league.
"""

from dancer import league


def test_league():
    """Test for converting league from string."""

    obj = league.League
    assert obj.from_str('NL') == obj.OPEN_CIRCUIT_NATIONAL_LEAGUE
    assert obj.from_str('SL') == obj.OPEN_CIRCUIT_SUPER_LEAGUE
    assert obj.from_str('uni') == obj.UNKNOWN
    assert obj.from_str('foobar') == obj.UNKNOWN
