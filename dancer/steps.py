"""Dance steps.

LF = Left Foot
RF = Right Foot
"""

import random
from enum import Enum
from typing import Optional, Iterable
import functools
from pydantic import BaseModel, validate_arguments

# TODO: automatically keep track of all figures that inherit from
# NaturalFigure and ReverseFigure


class RoutineError(Exception):
    """Represents an error with a routine."""

    pass


class Position(str, Enum):
    """Define all possible body positions wrt partner."""

    CLOSED_POSITION = "Closed"
    PROMENADE_POSITION = "PP"
    COUNTER_PROMENADE_POSITION = "CPP"
    OPEN_POSITION = "Open"
    OPEN_PROMENADE_POSITION = "OpenPP"
    OPEN_COUNTER_PROMENADE_POSITION = "OpenCPP"
    SHADOW_POSITION_R = "R Shadow"
    SHADOW_POSITION_L = "L Shadow"
    FAN_POSITION = "Fan"
    CONTACT_POSITION = "Contact"
    SIDE_POSITION_R = "RSP"
    CONTRA_POSITION_R = "ContraR"
    CONTRA_POSITION_L = "ContraL"
    INVERTED_PROMENADE_POSITION = "Inv.PP"
    INVERTED_COUNTER_PROMENADE_POSITION = "Inv.CPP"


class Direction(str, Enum):
    """Define all possible directions."""

    FORWARD = "Fwd"
    BACKWARD = "Bwd"
    LEFT = "Left"
    RIGHT = "Right"
    FORWARD_OR_BACKWARD = "Fwd/Bwd"
    ANY = "Any"
    LATIN_CROSS = "LatX"
    LATIN_CROSS_FWD = "LatXFwd"
    FLICK = "Flick"
    APPEL = "Appel"
    SLIP_APPEL = "Sp.Appel"

    def compatible_with(self, other) -> bool:
        """Return True if this option is compatible with the other option e.g.
        FORWARD with FORWARD_OR_BACKWARD.
        """

        if self == other:
            return True
        if self == self.FORWARD_OR_BACKWARD:
            if other == self.FORWARD:
                return True
            if other == self.BACKWARD:
                return True
        if other == self.FORWARD_OR_BACKWARD:
            if self == self.FORWARD:
                return True
            if self == self.BACKWARD:
                return True
        if self == self.ANY:
            return True
        if other == self.ANY:
            return True
        return False


class FootOptions(str, Enum):
    """Define all possible directions."""

    LEFT = "L"
    RIGHT = "R"


class Foot(BaseModel):
    """
    Represents a foot (either L or R) at the start or end of a figure.

    PP implies that the next foot will be RF fwd, with the previous
    being LF.
    """

    direction: Direction
    foot: FootOptions

    def __eq__(self, other) -> bool:
        """Compare these."""

        return self.foot == other.foot and self.direction == other.direction

    def __hash__(self) -> bytes:
        """Uniquely hash this object."""

        return hash((self.foot, self.direction))

    def samefoot(self, other):
        """Determine if this is compatible with another foot."""

        if other == self:
            return True
        if other.foot == self.foot:
            if self.direction.compatible_with(other.direction):
                return True
        return False


class Figure(BaseModel):
    """
    Generic class for a dance figure.
    """

    name: str
    start_foot: Foot
    start_position: Position = Position.CLOSED_POSITION
    next_foot: Foot
    next_position: Position = Position.CLOSED_POSITION

    def __eq__(self, other) -> bool:
        """Very that these steps are equivalent."""

        return (
            self.start_foot == other.start_foot
            and self.start_position == other.start_position
            and self.name == other.name
            and self.next_foot == other.next_foot
            and self.next_position == other.next_position
        )

    def __hash__(self) -> bytes:
        """Uniquely hash this object."""

        return hash(
            (
                self.name,
                self.start_foot,
                self.start_position,
                self.next_foot,
                self.next_position,
            )
        )

    def valid_next(self, other) -> bool:
        """Return True if the `other` move is valid proceeding from this one."""

        if other.start_foot.samefoot(self.next_foot):
            if other.start_position == self.next_position:
                return True
        return False

    def next(self, *, figures: list[object]) -> list[object]:
        """Return all possible next figures after this one."""

        return [
            fig
            for fig in figures
            if fig.start_foot.samefoot(self.next_foot)
            and fig.start_position == self.next_position
        ]

    def prev(self, *, figures: list[object]) -> list[object]:
        """Return all possible previous figures before this one."""

        return [
            fig
            for fig in figures
            if fig.next_foot.samefoot(self.start_foot)
            and fig.next_position == self.start_position
        ]


@validate_arguments
def random_figure(*, figures: list[Figure]) -> Figure:
    """Generate a random figure."""

    return random.choice(figures)


def main():
    """Run this to test."""

    # Import is here to avoid circular import
    from dancer.choreography import ALL_FIGURES

    step = random_figure(figures=ALL_FIGURES)
    print("Random step: %s" % step.name)
    print()
    routine = []
    for _ in range(10):
        routine.append(step)
        possible = step.next(figures=ALL_FIGURES)
        step = random.choice(possible)

    start = routine[0]

    print("* Routine starting with %s" % start.name)
    for i, elem in enumerate(routine):
        print("%2d. %s" % (i + 1, elem.name))

    print()
    print("* All possible preceding %s" % start.name)
    for step in sorted([step.name for step in start.prev(figures=ALL_FIGURES)]):
        print("- %s" % step)

    print()
    print("* All possible proceeding from %s" % start.name)
    for step in sorted([step.name for step in start.next(figures=ALL_FIGURES)]):
        print("- %s" % step)


if __name__ == "__main__":
    main()
