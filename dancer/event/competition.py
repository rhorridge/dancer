"""Represents more details about a competition than is necessary
for scrutineering and results purposes, such as organiser contact
details, locations etc.
"""

import hashlib
from typing import Optional, Union
from enum import Enum
from pydantic import BaseModel, HttpUrl, EmailStr
from dancer.competition import CompetitionDetails


class Address(BaseModel):
    """Represents an address."""

    street_address: str
    locality: str
    postcode: str
    country: str
    map_url: Optional[HttpUrl]

    def full_location(self) -> str:
        """Return the full location as a string."""

        addr = self.street_address.split(",")
        addr = [row.strip() for row in addr]
        locality = self.locality.split(",")
        locality = [row.strip() for row in locality]
        postcode = [self.postcode.strip()]
        country = [self.country.strip()]

        return ", ".join(addr + locality + postcode + country)


class ContactDetails(BaseModel):
    """Represents contact details."""

    name: Optional[str]
    telephone: Optional[str]
    email: Union[list[EmailStr], EmailStr, None]
    url: Optional[HttpUrl]


class EventCategory(str, Enum):
    BALLROOM_AND_LATIN = "Ballroom and Latin"


class CompetitionCalendarDetails(CompetitionDetails):
    """Includes extra details that may appear on a calendar."""

    event_category: EventCategory
    event_url: Optional[HttpUrl]
    venue_address: Address
    venue_telephone: Optional[str]
    venue_url: Optional[HttpUrl]
    organiser: ContactDetails

    @property
    def id(self) -> str:
        """Return a unique identifier for this competition."""

        return hashlib.sha1(self.json(exclude_none=True))

    def full_location(self) -> str:
        """Return the full location as a string."""

        if self.venue:
            return f"{self.venue.strip()}, {self.venue_address.full_location()}"
        return self.venue_address.full_location()

    def html_description(self) -> str:
        """Return full HTML description."""

        string = f"<h4>{self.name} - {self.venue.strip()} - {self.date.strftime('%A %d %B %Y')} - {self.event_category}</h4>\n"
        string += "<ul>\n"
        if self.venue_url:
            string += f'<li>Venue Information: <a href="{self.venue_url}">{self.venue_url}</a></li>\n'
        if self.organiser.url:
            string += f'<li>Organiser Information: <a href="{self.organiser.url}">{self.organiser.url}</a></li>\n'
        if self.url:
            string += f'<li>DPA Details: <a href="{self.url}">{self.url}</a></li>\n'
        if self.venue_address.map_url:
            string += f'<li>Google Maps link: <a href="{self.venue_address.map_url}">{self.venue_address.map_url}</a></li>\n'
        string += "</ul>\n"
        return string
