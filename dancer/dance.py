"""Represents a different type of dance.
"""

import sys
from pydantic import BaseModel
from typing import List, Optional


class Dance(BaseModel):
    name: str
    abbr: str

    def __eq__(self, other: object) -> bool:
        """Compare dances."""

        return self.name == other.name

    def __hash__(self) -> bytes:
        """Uniquely hash this object."""

        return hash(self.name)


Waltz = Dance(name="Waltz", abbr="W")
Quickstep = Dance(name="Quickstep", abbr="Q")
Tango = Dance(name="Tango", abbr="T")
VienneseWaltz = Dance(name="Viennese Waltz", abbr="V")
Foxtrot = Dance(name="Foxtrot", abbr="F")

ChaChaCha = Dance(name="Cha-Cha-Cha", abbr="C")
Jive = Dance(name="Jive", abbr="J")
Rumba = Dance(name="Rumba", abbr="R")
Samba = Dance(name="Samba", abbr="S")
PasoDoble = Dance(name="Paso Doble", abbr="P")

Salsa = Dance(name="Salsa", abbr="Salsa")
AcroRockNRoll = Dance(name="Acrobatic Rock 'n' Roll", abbr="ARnR")
NonAcroRockNRoll = Dance(name="Non-acrobatic Rock 'n' Roll", abbr="RnR")

FiveDanceBallroom = [Waltz, Tango, VienneseWaltz, Foxtrot, Quickstep]
FiveDanceLatin = [ChaChaCha, Samba, Rumba, PasoDoble, Jive]

TeamMatch = [Waltz, ChaChaCha, Quickstep, Jive]

ClassicalSequence = Dance(name="Classical Sequence", abbr="Class.Seq.")


def get_dance(option: str) -> Optional[Dance]:
    """Return a dance, given an option."""

    opt = option.lower().replace("'", "").replace("-", " ").replace(" ", "_").lower()

    match opt:
        case "waltz":
            return Waltz
        case "quickstep":
            return Quickstep
        case "foxtrot":
            return Foxtrot
        case "tango":
            return Tango
        case "viennese_waltz":
            return VienneseWaltz
        case "cha_cha_cha":
            return ChaChaCha
        case "jive":
            return Jive
        case "rumba":
            return Rumba
        case "samba":
            return Samba
        case "paso_doble":
            return PasoDoble
        case "salsa":
            return Salsa
        case "acrobatic_rock_n_roll":
            return AcroRockNRoll
        case "non_acrobatic_rock_n_roll":
            return NonAcroRockNRoll


if __name__ == "__main__":
    if len(sys.argv) <= 1:
        sys.stderr.write("This script expects at least one argument!\n")
        sys.exit(1)

    dance = get_dance(sys.argv[1])
    print(dance.json(indent=4, exclude_none=True))
