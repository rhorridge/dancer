"""Unit tests for .couple.
"""

from . import couple
from .university import university


def test_couple_str():
    """Test for parsing couple."""

    fn = couple.parse_couple_str

    assert fn("Leader A Follower B Bristol") == couple.Couple(
        leader="Leader A",
        follower="Follower B",
        university=university("Bristol"),
    )
    assert fn("Leader A Follower B C Bristol") == couple.Couple(
        leader="Leader A",
        follower="Follower B C",
        university=university("Bristol"),
    )

    assert fn("Leader A B Follower B C Bristol") == couple.Couple(
        leader="Leader A",
        follower="B Follower B C",
        university=university("Bristol"),
    )
