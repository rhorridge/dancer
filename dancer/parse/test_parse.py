"""Unit tests for dancer.parse.
"""

import pytest
from dancer import parse


def test_fmt():
    """Test for formatting JSON string."""

    assert parse.fmt("{}") == "{}"


def test_competitors(tmpdir):
    """Test for parsing competitors."""

    filename = tmpdir / "competitors.txt"

    with filename.open("w", encoding="utf-8") as fp_:
        fp_.write("1 Competitor 1 & Competitor 2\n")
        fp_.write("2 Competitor 3 and Competitor 4\n")

    res = parse.competitors(tmpdir)
    assert res[1].leader == "Competitor 1"
    assert res[1].follower == "Competitor 2"
    assert res[2].leader == "Competitor 3"
    assert res[2].follower == "Competitor 4"


def test_round_1_events(tmpdir):
    """Test parsing of round 1 events."""

    filename = tmpdir / "NL1 - Waltz - Round 1.txt"

    with filename.open("w", encoding="utf-8") as fp_:
        fp_.write("\n")

    res = parse.round_1_events(tmpdir)
    print(res)
    assert res[0].group(4) == "Waltz"


def test_final_events(tmpdir):
    """Test parsing of final events."""

    filename = tmpdir / "NL1 - Waltz - Final.txt"

    with filename.open("w", encoding="utf-8") as fp_:
        fp_.write("\n")

    res = parse.final_events(tmpdir)
    assert res[0].group(4) == "Waltz"


def test_events(tmpdir):
    """Test parsing of events."""

    filename1 = tmpdir / "NL1 - Team Match - Final.txt"
    filename2 = tmpdir / "NL2 - Team Match - Final.txt"

    filename1.open("w", encoding="utf-8").write("\n")
    filename2.open("w", encoding="utf-8").write("\n")

    assert len(parse.events(tmpdir)) == 2


@pytest.mark.xfail
def test_event_entries():
    """Test parsing of events."""

    raise NotImplementedError("Integration Test - needs file on filesystem")


@pytest.mark.xfail
def test_final_entries():
    """Test parsing of finals."""

    raise NotImplementedError("Integration Test - needs file on filesystem")


@pytest.mark.xfail
def test_judges():
    """Test parsing of judges."""

    raise NotImplementedError("Integration Test - needs filesystem directory")


@pytest.mark.xfail
def test_rounds():
    """Test parsing of rounds."""

    raise NotImplementedError("Integration Test - needs filesystem directory")


@pytest.mark.xfail
def test_final():
    """Test parsing of event final."""

    raise NotImplementedError("Integration Test - needs filesystem directory")


@pytest.mark.xfail
def test_competitors_in_final():
    """Test parsing of competitors from an event final."""

    raise NotImplementedError("Integration Test - needs filesystem directory")
