"""Unit tests for ..university.
"""

import pytest
from . import university


def test_university():
    """Test for parsing university name."""

    fn = university.university_name
    a, b = fn("A B C D Aberystwyth")
    assert a == "A B C D"
    assert b.display_name == "Aberystwyth"

    a, b = fn("Leader A Follower B Bristol")
    assert a == "Leader A Follower B"
    assert b.display_name == "Bristol"

    a, b = fn("Leader A Follower B C Bristol")
    assert a == "Leader A Follower B C"
    assert b.display_name == "Bristol"

    a, b = fn("A. Follower BRISTOL")
    assert a == "A. Follower"
    assert b.display_name == "Bristol"

    a, b = fn("A. Follower ROYAL HOLLOWAY")
    assert a == "A. Follower"
    assert b.display_name == "Royal Holloway"
