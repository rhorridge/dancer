"""figure out event categories.
"""

import logging
from copy import deepcopy
from dancer.competition import Category
from dancer import competition_events as ev
from .open_circuit import national_league, super_league, unknown

LOG = logging.getLogger(__name__)


def event_category(event_name: str) -> Category:
    """Determine the event category from a name."""

    LOG.debug("Matching %s", event_name)
    match event_name:
        case "Beginner Waltz" | "Beginners Waltz" | "Beginner W" | "Beg Waltz" | "Beg W":
            return ev.BeginnerW
        case "Beginner Quickstep" | "Beginners Quickstep" | "Beginner Q" | "Beg Quickstep" | "Beg Q":
            return ev.BeginnerQ
        case "Beginners Waltz Quickstep" | "Beginner Waltz Quickstep" | "Beginner W Q" | "Beg  WQ":
            return ev.BeginnerWQ
        case "Novice Waltz" | "Nov Waltz" | "Nov W" | "Novice W":
            return ev.NoviceW
        case "Novice Quickstep" | "Nov Quickstep" | "Nov Q" | "Novice Q":
            return ev.NoviceQ
        case "Beg Novice V":
            event = deepcopy(ev.NoviceV)
            event.name = "Beginner/" + event.name
            return ev.NoviceV
        case "Novice Ballroom" | "Nov Ballroom" | "Nov Waltz Quickstep" | "Novice Waltz Quickstep" | "Novice WQ" | "Nov WQ" | "Novice W Q":
            return ev.NoviceWQ
        case "Novice 1 Ballroom" | "Nov 1 Ballroom" | "Novice Waltz and Quickstep Qualifier A" | "Novice Waltz and Quickstep Qualifier" | "Novice Waltz Quickstep Qualifier A" | "Novice Waltz Quickstep Qualifier" | "Novice WQ A":
            event = deepcopy(ev.NoviceWQ)
            event.name = event.name + " - Division 1"
            return event
        case "Novice W V Q" | "Novice WVQ" | "Nov WVQ" | "Nov W V Q":
            return ev.NoviceWVQ
        case "Novice 2 Ballroom" | "Novice 2 WQ" | "First Year Novice WQ" | "Nov 2 Ballroom" | "Novice WQ B":
            event = deepcopy(ev.NoviceWQ)
            event.name = event.name + " - Division 2"
            return event
        case "Novice Waltz and Quickstep Qualifier B" | "Novice Waltz Quickstep Qualifier B":
            event = deepcopy(ev.NoviceWQ)
            event.name = event.name + " - Division 2"
            return event
        case "BeginnerNovice Same-Sex 2-Dance (WQ)" | "Same Sex BegNov WQ" | "BegNov Same Sex WQ" | "BegNov Same-Sex 2-Dance (WQ)" | "Same-Sex BegNov WQ" | "SameSex BegNov WQ" | "BegNov SameSex WQ" | "BegNov Same-Sex WQ":
            event = deepcopy(ev.NoviceWQ)
            event.name = "Same-Sex Beginner/" + event.name
            return event
        case "BeginnerNovice Tango" | "Beginners Novice Tango" | "Beginner Novice Tango" | "BegNov Tango" | "BegNov T":
            event = deepcopy(ev.NoviceT)
            event.name = "Beginner/" + event.name
            return event
        case "BeginnerNovice Foxtrot" | "BegNov  F" | "BegNov Foxtrot" | "Beginner Novice Foxtrot" | "Beginners Novice Foxtrot" | "BegNov F" | "Beg Nov Foxtrot":
            event = deepcopy(ev.NoviceF)
            event.name = "Beginner/" + event.name
            return event
        case "Ex-Student Novice Ballroom" | "Ex Student Novice WQ" | "Ex Student Novice Ballroom" | "Ex-Student Novice WQ" | "Ex-Student Nov Ballroom" | "Ex Student Nov Ballroom" | "Ex-Student Nov WQ" | "Ex Student Nov WQ" | "Ex student Novice WQ":
            event = deepcopy(ev.NoviceWQ)
            event.name = "Ex-Student " + event.name
            return event
        case "Pre-Intermediate Ballroom" | "Pre-Intermediate 2-Dance (WQ)" | "Pre-Int WQ" | "Preintermediate WQ" | "Preinter WQ" | "Pre inter WQ":
            return ev.PreIntermediateWQ
        case "Intermediate Ballroom" | "Inter W T Q":
            return ev.IntermediateWTQ
        case "Intermediate WFQ" | "Intermediate W FQ":
            return ev.IntermediateWFQ
        case "Intermediate 2 WFQ":
            event = deepcopy(ev.IntermediateWFQ)
            event.name = event.name + " - Division 2"
            return event
        case "Inter Adv - B - WTQ" | "B Inter Advanced Waltz Tango Quickstep":
            event = deepcopy(ev.AdvancedWTQ)
            event.name = "Intermediate/" + event.name + " - Division 2"
            return event
        case "Int  Adv WFQ":
            event = deepcopy(ev.AdvancedWFQ)
            event.name = "Intermediate/" + event.name + " - Division 1"
            return event
        case "Int  Adv WFQ B":
            event = deepcopy(ev.AdvancedWFQ)
            event.name = "Intermediate/" + event.name + " - Division 2"
            return event
        case "Ex-Student Intermediate Ballroom" | "Ex Student Intermediate Ballroom" | "Ex-Student Inter Ballroom" | "Ex Student Inter Ballroom":
            event = deepcopy(ev.IntermediateWTQ)
            event.name = "Ex-Student " + event.name
            return event
        case "Same-Sex Ballroom" | "Same Sex Ballroom" | "SameSex Ballroom" | "SS Ballroom":
            event = deepcopy(ev.OpenWTQ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same Sex WQ" | "SS WQ" | "Same sex WQ":
            event = deepcopy(ev.OpenWQ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same Sex WTQ" | "Same Sex Waltz Tango Quickstep" | "Same-Sex WTQ" | "SameSex WTQ" | "Same-Sex Waltz Tango Quickstep" | "SameSex Waltz Tango Quickstep" | "SS WTQ":
            event = deepcopy(ev.OpenWTQ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same-sex W V Q" | "Same Sex WVQ" | "Same-Sex WVQ" | "SameSex WVQ" | "Same Sex Waltz Viennese Quickstep" | "Same-Sex Waltz Viennese Quickstep" | "SameSex Waltz Viennese Quickstep" | "SS WVQ":
            event = deepcopy(ev.OpenWVQ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same-Sex IntAdv WQ" | "Same Sex IntAdv WQ" | "SameSex IntAdv WQ" | "Same Sex IntAdv Waltz Quickstep" | "Same-Sex IntAdv Waltz Quickstep" | "SameSex IntAdv Waltz Quickstep" | "SS IntAdv WQ":
            event = deepcopy(ev.OpenWQ)
            event.name = "Same-Sex " + event.name
            return event
        case "Ex Student WTQ" | "Ex-Student WTQ":
            event = deepcopy(ev.OpenWTQ)
            event.name = "Ex-Student " + event.name
            return event
        case "Ex student WFQ":
            event = deepcopy(ev.OpenWFQ)
            event.name = "Ex-Student " + event.name
            return event
        case "Ex Student Open WF" | "Ex-Student WF":
            event = deepcopy(ev.OpenWF)
            event.name = "Ex-Student " + event.name
            return event
        case "Adv Int- QualifierWQ" | "IntAdv Qualifier WQ":
            event = deepcopy(ev.AdvancedWQ)
            event.name = "Intermediate/" + event.name
            return event
        case "Intermediate-Advanced WQT" | "Intermediate-Advanced WTQ" | "IntermediateAdv WTQ" | "IntAdv WTQ" | "Inter Adv Waltz Tango Quickstep":
            event = deepcopy(ev.AdvancedWTQ)
            event.name = "Intermediate/" + event.name
            return event
        case "IntermediateAdv WFQ" | "Intermediate-Advanced WFQ" | "IntAdv WFQ":
            event = deepcopy(ev.AdvancedWFQ)
            event.name = "Intermediate/" + event.name
            return event
        case "Inter  Adv - A - W T Q" | "InterAdv - A - WTQ" | "Int  Adv WFQ A":
            event = deepcopy(ev.AdvancedWTQ)
            event.name = "Intermediate/" + event.name + " - Division 1"
            return event
        case "Intermediate W T V Q" | "Intermediate WTVQ" | "Inter WTVQ" | "Int WTVQ":
            return ev.IntermediateWTVQ
        case "Advanced Ballroom" | "Advanced W T V Q" | "Advanced WTVQ" | "Adv WTVQ":
            return ev.AdvancedWTVQ
        case "Advanced W T F Q":
            return ev.AdvancedWTFQ
        case "Advanced WFQ" | "Adv WFQ":
            return ev.AdvancedWFQ
        case "Open Foxtrot" | "Open Fox" | "Open F":
            return ev.OpenF
        case "Intermediate Advanced Foxtrot" | "InterAdvanced Foxtrot" | "IntAdv Foxtrot":
            event = deepcopy(ev.AdvancedF)
            event.name = "Intermediate/" + event.name
            return event
        case "Open Viennese Waltz" | "Open Viennese" | "Open VW" | "OpenViennese Waltz" | "Open Vienesse Waltz" | "Open V" | "Open Vienese Waltz" | "Open Vienesse":
            return ev.OpenV
        case "Open Tango" | "Open T":
            return ev.OpenT
        case "Open 5 Dance Ballroom WTVFQ" | "Open 5 Dance Ballroom" | "Open WTVFQ":
            return ev.OpenWTVFQ

        case "Beginner Cha" | "Beginner Cha Cha" | "Beginners CCC" | "Beginner C" | "Beginners C" | "Beg CCC" | "Beginners Cha" | "Beg Cha" | "Beg C" | "Beginner ChaCha" | "Beginners ChaCha" | "Beg ChaCha" | "Beginners Cha Cha" | "Beg Cha Cha" | "Beginner Cha-Cha" | "Beginners Cha-Cha" | "Beg Cha-Cha" | "Beginner Cha-Cha-Cha" | "Beginners Cha-Cha-Cha" | "Beg Cha-Cha-Cha" | "Beginner Cha Cha Cha" | "Beginners Cha Cha Cha" | "Beg Cha Cha Cha" | "Beginner CCC":
            return ev.BeginnerC
        case "Beginner Jive" | "Beginners Jive" | "Beginner J" | "Beginners J" | "Beg Jive" | "Beg J":
            return ev.BeginnerJ
        case "Beginner Cha Jive" | "Beginner C J" | "Beginner Cha Cha Jive" | "Beginner CJ" | "Beg CJ":
            return ev.BeginnerCJ
        case "Novice Cha Cha" | "Novice C":
            return ev.NoviceC
        case "Novice Jive" | "Novice J":
            return ev.NoviceJ
        case "Novice Latin" | "Novice Cha Jive" | "Novice CJ" | "Novice Cha Cha Jive" | "Nov CJ" | "Novice C J":
            return ev.NoviceCJ
        case "Novice C S J" | "Novice CSJ":
            return ev.NoviceCSJ
        case "Novice 1 Latin":
            event = deepcopy(ev.NoviceCJ)
            event.name = event.name + " - Division 1"
            return event
        case "Novice 2 Latin" | "Novice 2 CJ" | "First Year Novice CJ" | "B Novice B CJ":
            event = deepcopy(ev.NoviceCJ)
            event.name = event.name + " - Division 2"
            return event
        case "Novice CJ Qualifier":
            event = deepcopy(ev.NoviceCJ)
            event.name = event.name + " - Division 1"
            return event
        case "Novice CJ Qualifier A":
            event = deepcopy(ev.NoviceCJ)
            event.name = event.name + " - Division 1"
            return event
        case "Novice CJ Qualifier B":
            event = deepcopy(ev.NoviceCJ)
            event.name = event.name + " - Division 2"
            return event
        case "BeginnerNovice Same-sex 2-Dance (CJ)" | "Same-Sex BegNov CJ" | "BegNov Same Sex CJ" | "BegNov SameSex CJ":
            event = deepcopy(ev.NoviceCJ)
            event.name = "Same-Sex Beginner/" + event.name
            return event
        case "BeginnerNovice Rumba" | "Beginner Novice Basic Rumba" | "Beginner Novice Rumba" | "BeginnerNovice R" | "Beg Nov Rumba" | "BegNov R" | "Beg Novice Rumba":
            event = deepcopy(ev.NoviceR)
            event.name = "Beginner/" + event.name
            return event
        case "Beg Nov Samba" | "BegNov S" | "Beginner Novice Samba" | "BegNov Samba" | "BeginnerNovice Samba" | "Beginner Novice Basic Samba" | "BeginnerNovice S":
            event = deepcopy(ev.NoviceS)
            event.name = "Beginner/" + event.name
            return event
        case "Ex-Student Novice Latin" | "Ex Student Novice CJ" | "Ex Student Novice Latin" | "Ex-Student Novice CJ" | "Ex student Novice CJ":
            event = deepcopy(ev.NoviceCJ)
            event.name = "Ex-Student " + event.name
            return event
        case "Ex Student Open CS" | "Ex-Student Open CS":
            event = deepcopy(ev.OpenCS)
            event.name = "Ex-Student " + event.name
            return event
        case "Pre-Intermediate Latin" | "Pre-Intermediate 2-Dance (CJ)" | "Pre-Int CJ" | "Pre inter CJ":
            return ev.PreIntermediateCJ
        case "Intermediate Latin" | "Inter CRJ" | "Inter C R J":
            return ev.IntermediateCRJ
        case "Intermediate CSJ":
            return ev.IntermediateCSJ
        case "Intermediate 2 CSJ" | "B Inter Advanced Cha Cha Samba Jive":
            event = deepcopy(ev.AdvancedCSJ)
            event.name = "Intermediate/" + event.name + " - Division 2"
            return event
        case "Intermediate C S R J" | "Intermediate CSRJ":
            return ev.IntermediateCSRJ
        case "Ex-Student Intermediate Latin":
            event = deepcopy(ev.IntermediateCRJ)
            event.name = "Ex-Student " + event.name
            return event
        case "Int-Adv Qualifier Round 1 CJ":
            event = deepcopy(ev.AdvancedCJ)
            event.name = "Intermediate/" + event.name
            return event
        case "Advanced 3-Dance (CRJ)":
            return ev.AdvancedCRJ
        case "Inter adv V":
            event = deepcopy(ev.AdvancedV)
            event.name = "Intermediate/" + event.name
            return event
        case "Intermediate-Advanced CJR" | "Intermediate-Advanced CRJ":
            event = deepcopy(ev.AdvancedCRJ)
            event.name = "Intermediate/" + event.name
            return event
        case "Inter Adv  - A - CSJ" | "Inter Advanced Cha Cha Samba Jive" | "Int  Adv  CSJ A" | "Int  Adv  CSJ":
            event = deepcopy(ev.AdvancedCSJ)
            event.name = "Intermediate/" + event.name + " - Division 1"
            return event
        case "Int Adv - B - CSJ" | "Int  Adv  CSJ B":
            event = deepcopy(ev.AdvancedCSJ)
            event.name = "Intermediate/" + event.name + " - Division 2"
            return event
        case "Advanced CSJ":
            return ev.AdvancedCSJ
        case "Advanced Latin":
            return ev.AdvancedCRPJ
        case "Advanced C S R J" | "Advanced CSRJ":
            return ev.AdvancedCSRJ
        case "Adv C R P J":
            return ev.AdvancedCRPJ
        case "Ex-Student Advanced Latin" | "Ex Student Advanced Latin":
            event = deepcopy(ev.AdvancedCRPJ)
            event.name = "Ex-Student " + event.name
            return event
        case "Same Sex CJ" | "SS CJ" | "Same sex CJ":
            event = deepcopy(ev.OpenCJ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same-Sex IntAdv CJ":
            event = deepcopy(ev.OpenCJ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same-Sex Latin":
            event = deepcopy(ev.OpenCRJ)
            event.name = "Same-Sex " + event.name
            return event
        case "Same Sex C S J" | "Same-sex C S J" | "Same Sex Cha Cha Samba Jive" | "Same-Sex C S J" | "Same sex C S J" | "Same Sex CSJ" | "Same-Sex CSJ" | "Same sex CSJ" | "Same-sex CSJ" | "Same-sex Cha Cha Samba Jive" | "Same-Sex Cha Cha Samba Jive" | "Same sex Cha Cha Samba Jive":
            event = deepcopy(ev.OpenCSJ)
            event.name = "Same-Sex " + event.name
            return event
        case "Ex Student CRJ":
            event = deepcopy(ev.OpenCRJ)
            event.name = "Ex-Student " + event.name
            return event
        case "Ex student CSJ":
            event = deepcopy(ev.OpenCSJ)
            event.name = "Ex-Student " + event.name
            return event
        case "Open Samba" | "Open S":
            return ev.OpenS
        case "Open Paso Doble" | "Open Paso" | "Open P":
            return ev.OpenP
        case "Intermediate Advanced Paso" | "IntermediateAdvanced Paso" | "InterAdvanced Paso" | "IntAdv Paso" | "IntAdv P":
            event = deepcopy(ev.AdvancedP)
            event.name = "Intermediate/" + event.name
            return event
        case "Inter Adv Samba":
            event = deepcopy(ev.AdvancedS)
            event.name = "Intermediate/" + event.name
            return event
        case "Open Rumba" | "Open R":
            return ev.OpenR
        case "Open 5 Dance Latin CSRPJ" | "Open 5 Dance Latin" | "Open CSRPJ":
            return ev.OpenCSRPJ

        case "Open Salsa" | "Salsa":
            return ev.OpenSalsa
        case "Non-acrobatic Rock n Roll" | "Non-Acro RnR" | "Non Acrobatic RnR" | "Non-acro RnR" | "Non-acro Rock n Roll" | "Non-Acrobatic Rock n Roll" | "Non-Acro Rock n Roll" | "Non-acrobatic RnR" | "Non-Acrobatic RnR" | "Rock & Roll":
            return ev.NonAcroRnR
        case "Rock n Roll":
            return ev.NonAcroRnR
        case "Acro RnR" | "Acrobatic RnR":
            return ev.AcroRnR

        case "Mayfair Quickstep":
            event = deepcopy(ev.ClassicalSequence)
            event.name = event.name + " - Mayfair Quickstep"
            return event

        case "Sindy Swing":
            event = deepcopy(ev.ClassicalSequence)
            event.name = event.name + " - Sindy Swing"
            return event

        case "Team Match" | "Team match":
            return ev.TeamMatch

        case "Team match A":
            event = deepcopy(ev.TeamMatch)
            event.name = event.name + " - Division 1"
            return ev.TeamMatch

        case "Team match B":
            event = deepcopy(ev.TeamMatch)
            event.name = event.name + " - Division 2"
            return ev.TeamMatch

        case _:
            try:
                event = (
                    national_league(event_name)
                    or super_league(event_name)
                    or unknown(event_name)
                )
            except RuntimeError as err:
                LOG.error(err)
                return None
            if event:
                return event
            LOG.error("Unknown/unsupported event: %s", event_name)
            return None
