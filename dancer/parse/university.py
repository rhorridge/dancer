"""Parsing for Universities, where present.
"""

from typing import Optional
from pydantic import BaseModel
from dancer.university import University


UNIVERSITY_SYSTEM_NAMES = {
    "aberystwyth": University(
        system_name="abersytwyth",
        display_name="Aberystwyth",
    ),
    "birmingham": University(
        system_name="birmingham",
        display_name="Birmingham",
    ),
    "bristol": University(
        system_name="bristol",
        display_name="Bristol",
    ),
    "bath": University(
        system_name="bath",
        display_name="Bath",
    ),
    "bournemouth": University(
        system_name="bournemouth",
        display_name="Bournemouth",
    ),
    "cardiff": University(
        system_name="cardiff",
        display_name="Cardiff",
    ),
    "durham": University(
        system_name="durham",
        display_name="Durham",
    ),
    "exeter": University(
        system_name="exeter",
        display_name="Exeter",
    ),
    "glasgow": University(
        system_name="glasgow",
        display_name="Glasgow",
    ),
    "hull": University(
        system_name="hull",
        display_name="Hull",
    ),
    "imperial": University(
        system_name="imperial",
        display_name="Imperial",
    ),
    "lancaster": University(
        system_name="lancaster",
        display_name="Lancaster",
    ),
    "leicester": University(
        system_name="leicester",
        display_name="Leicester",
    ),
    "london": University(
        system_name="london",
        display_name="London",
    ),
    "manchester": University(
        system_name="manchester",
        display_name="Manchester",
    ),
    "oxford": University(
        system_name="oxford",
        display_name="Oxford",
    ),
    "royal holloway": University(
        system_name="royal holloway",
        display_name="Royal Holloway",
    ),
    "southampton": University(
        system_name="southampton",
        display_name="Southampton",
    ),
    "surrey": University(
        system_name="surrey",
        display_name="Surrey",
    ),
    "warwick": University(
        system_name="warwick",
        display_name="Warwick",
    ),
}

UNIVERSITY_NAMES_MAPPING = {
    "Aberystwyth",
    "Birmingham",
    "Bristol",
    "Durham",
    "Glasgow",
    "Hull",
    "Imperial",
    "Imperial London",
    "Lancaster",
    "Leeds",
    "Leicester",
    "London",
    "Manchester",
    "Nottingham",
    "Oxford",
    "Royal Holloway",
    "Sheffield",
    "Southampton",
    "St Andrews",
    "Surrey",
    "Uea",
    "Warwick",
    "York",
}


def university(university: str) -> Optional[University]:
    """Return a University, or None."""

    return UNIVERSITY_SYSTEM_NAMES.get(university.lower())


def university_name(follower: str) -> (str, Optional[str]):
    """Return the University name, if relevant, from the follower.

    Returned as a tuple, with the first item the modified follower
    name.

    If a University is not found, the follower name is unchanged.
    """

    splits = follower.split()
    if len(splits) < 3:
        # We keep the follower surname
        return follower, None
    university = splits[-1].lower()
    if university in UNIVERSITY_SYSTEM_NAMES:
        return (" ".join(splits[:-1]), UNIVERSITY_SYSTEM_NAMES[university])
    # trying second name
    university = " ".join(splits[-2:]).lower()
    if university in UNIVERSITY_SYSTEM_NAMES:
        return (" ".join(splits[:-2]), UNIVERSITY_SYSTEM_NAMES[university])
    return follower, None
