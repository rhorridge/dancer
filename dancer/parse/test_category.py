"""Unit tests for dancer.parse.category.
"""

from copy import deepcopy
import pytest
from dancer.parse import category


def make_div_1(obj) -> object:
    itm = deepcopy(obj)
    itm.name = itm.name + " - Division 1"
    return itm


def make_div_2(obj) -> object:
    itm = deepcopy(obj)
    itm.name = itm.name + " - Division 2"
    return itm


def make_same_sex(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Same-Sex " + itm.name
    return itm


def make_same_sex_beg_nov(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Same-Sex Beginner/" + itm.name
    return itm


def make_same_sex_int_adv(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Same-Sex Intermediate/" + itm.name
    return itm


def make_beg_nov(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Beginner/" + itm.name
    return itm


def make_int_adv(obj) -> object:
    print(obj)
    itm = deepcopy(obj)
    itm.name = "Intermediate/" + itm.name
    print(repr(itm))
    return itm


def make_int_adv_div_1(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Intermediate/" + itm.name + " - Division 1"
    return itm


def make_int_adv_div_2(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Intermediate/" + itm.name + " - Division 2"
    return itm


def make_ex_student(obj) -> object:
    itm = deepcopy(obj)
    itm.name = "Ex-Student " + itm.name
    return itm


def test_event_category():
    """Test for parsing of event categories."""

    fn = category.event_category
    obj = category.Category
    ev = category.ev

    itm = ev.BeginnerW
    assert fn("Beginner Waltz") == itm
    assert fn("Beginners Waltz") == itm
    assert fn("Beg Waltz") == itm
    assert fn("Beginner W") == itm
    assert fn("Beg W") == itm

    itm = ev.BeginnerQ
    assert fn("Beginner Quickstep") == itm
    assert fn("Beginners Quickstep") == itm
    assert fn("Beg Quickstep") == itm
    assert fn("Beginner Q") == itm
    assert fn("Beg Q") == itm

    itm = ev.BeginnerWQ
    assert fn("Beginners Waltz Quickstep") == itm
    assert fn("Beginner Waltz Quickstep") == itm
    assert fn("Beginner W Q") == itm

    itm = ev.NoviceW
    assert fn("Novice Waltz") == itm
    assert fn("Novice Waltz") == itm
    assert fn("Nov Waltz") == itm
    assert fn("Novice W") == itm
    assert fn("Nov W") == itm

    itm = ev.NoviceQ
    assert fn("Novice Quickstep") == itm
    assert fn("Novice Quickstep") == itm
    assert fn("Nov Quickstep") == itm
    assert fn("Novice Q") == itm
    assert fn("Nov Q") == itm

    itm = ev.NoviceWQ
    assert fn("Novice Ballroom") == itm
    assert fn("Nov Ballroom") == itm
    assert fn("Novice Waltz Quickstep") == itm
    assert fn("Novice WQ") == itm
    assert fn("Nov Waltz Quickstep") == itm
    assert fn("Nov WQ") == itm

    itm = make_div_1(ev.NoviceWQ)
    assert fn("Novice 1 Ballroom") == itm
    assert fn("Nov 1 Ballroom") == itm
    assert fn("Novice Waltz Quickstep Qualifier A") == itm
    assert fn("Novice Waltz and Quickstep Qualifier A") == itm
    assert fn("Novice Waltz Quickstep Qualifier") == itm

    itm = ev.NoviceWVQ
    assert fn("Novice WVQ") == itm
    assert fn("Novice W V Q") == itm
    assert fn("Nov WVQ") == itm
    assert fn("Nov W V Q") == itm

    itm = make_div_2(ev.NoviceWQ)
    assert fn("Novice 2 Ballroom") == itm
    assert fn("Nov 2 Ballroom") == itm
    assert fn("Novice Waltz Quickstep Qualifier B") == itm
    assert fn("Novice Waltz and Quickstep Qualifier B") == itm

    itm = make_same_sex_beg_nov(ev.NoviceWQ)
    assert fn("BeginnerNovice Same-Sex 2-Dance (WQ)") == itm
    assert fn("BegNov Same-Sex 2-Dance (WQ)") == itm
    assert fn("Same Sex BegNov WQ") == itm
    assert fn("Same-Sex BegNov WQ") == itm
    assert fn("SameSex BegNov WQ") == itm
    assert fn("BegNov SameSex WQ") == itm
    assert fn("BegNov Same-Sex WQ") == itm
    assert fn("BegNov Same Sex WQ") == itm

    itm = make_beg_nov(ev.NoviceT)
    assert fn("BeginnerNovice Tango") == itm
    assert fn("Beginner Novice Tango") == itm
    assert fn("Beginners Novice Tango") == itm
    assert fn("BegNov Tango") == itm
    assert fn("BegNov T") == itm

    itm = make_beg_nov(ev.NoviceF)
    assert fn("BeginnerNovice Foxtrot") == itm
    assert fn("Beginner Novice Foxtrot") == itm
    assert fn("Beginners Novice Foxtrot") == itm
    assert fn("BegNov Foxtrot") == itm
    assert fn("BegNov F") == itm

    itm = make_ex_student(ev.NoviceWQ)
    assert fn("Ex-Student Novice Ballroom") == itm
    assert fn("Ex Student Novice Ballroom") == itm
    assert fn("Ex-Student Novice WQ") == itm
    assert fn("Ex Student Novice WQ") == itm
    assert fn("Ex-Student Nov Ballroom") == itm
    assert fn("Ex Student Nov Ballroom") == itm
    assert fn("Ex-Student Nov WQ") == itm
    assert fn("Ex Student Nov WQ") == itm

    itm = ev.PreIntermediateWQ
    assert fn("Pre-Intermediate Ballroom") == itm
    assert fn("Pre-Intermediate 2-Dance (WQ)") == itm
    assert fn("Pre-Int WQ") == itm
    assert fn("Preintermediate WQ") == itm
    assert fn("Preinter WQ") == itm

    itm = ev.IntermediateWTQ
    assert fn("Intermediate Ballroom") == itm

    itm = ev.IntermediateWFQ
    assert fn("Intermediate WFQ") == itm

    itm = make_div_2(ev.IntermediateWFQ)
    assert fn("Intermediate 2 WFQ") == itm

    itm = make_ex_student(ev.IntermediateWTQ)
    assert fn("Ex-Student Intermediate Ballroom") == itm
    assert fn("Ex Student Intermediate Ballroom") == itm
    assert fn("Ex-Student Inter Ballroom") == itm
    assert fn("Ex Student Inter Ballroom") == itm

    itm = make_same_sex(ev.OpenWTQ)
    assert fn("Same-Sex Ballroom") == itm
    assert fn("Same Sex Ballroom") == itm
    assert fn("SameSex Ballroom") == itm
    assert fn("SS Ballroom") == itm

    itm = make_same_sex(ev.OpenWQ)
    assert fn("Same Sex WQ") == itm
    assert fn("SS WQ") == itm

    itm = make_same_sex(ev.OpenWTQ)
    assert fn("Same Sex WTQ") == itm
    assert fn("Same-Sex WTQ") == itm
    assert fn("SameSex WTQ") == itm
    assert fn("Same Sex Waltz Tango Quickstep") == itm
    assert fn("Same-Sex Waltz Tango Quickstep") == itm
    assert fn("SameSex Waltz Tango Quickstep") == itm
    assert fn("SS WTQ") == itm

    itm = make_same_sex(ev.OpenWVQ)
    assert fn("Same Sex WVQ") == itm
    assert fn("Same-Sex WVQ") == itm
    assert fn("SameSex WVQ") == itm
    assert fn("Same Sex Waltz Viennese Quickstep") == itm
    assert fn("Same-Sex Waltz Viennese Quickstep") == itm
    assert fn("SameSex Waltz Viennese Quickstep") == itm
    assert fn("SS WVQ") == itm

    itm = make_same_sex(ev.OpenWQ)
    assert fn("Same Sex IntAdv WQ") == itm
    assert fn("Same-Sex IntAdv WQ") == itm
    assert fn("SameSex IntAdv WQ") == itm
    assert fn("Same Sex IntAdv Waltz Quickstep") == itm
    assert fn("Same-Sex IntAdv Waltz Quickstep") == itm
    assert fn("SameSex IntAdv Waltz Quickstep") == itm
    assert fn("SS IntAdv WQ") == itm

    itm = make_ex_student(ev.OpenWTQ)
    assert fn("Ex Student WTQ") == itm
    assert fn("Ex-Student WTQ") == itm

    itm = make_ex_student(ev.OpenWF)
    assert fn("Ex Student Open WF") == itm
    assert fn("Ex-Student WF") == itm

    itm = make_int_adv(ev.AdvancedWQ)
    assert fn("Adv Int- QualifierWQ") == itm
    assert fn("IntAdv Qualifier WQ") == itm

    itm = make_int_adv(ev.AdvancedWTQ)
    assert fn("Intermediate-Advanced WQT") == itm
    assert fn("Intermediate-Advanced WTQ") == itm
    assert fn("IntermediateAdv WTQ") == itm
    assert fn("IntAdv WTQ") == itm
    assert fn("Inter Adv Waltz Tango Quickstep") == itm

    itm = make_int_adv(ev.AdvancedWFQ)
    print(itm)
    assert fn("Intermediate-Advanced WFQ") == itm
    assert fn("IntermediateAdv WFQ") == itm
    assert fn("IntAdv WFQ") == itm

    itm = make_int_adv_div_1(ev.AdvancedWTQ)
    assert fn("Inter  Adv - A - W T Q") == itm
    assert fn("InterAdv - A - WTQ") == itm

    itm = make_int_adv_div_2(ev.AdvancedWTQ)
    assert fn("Inter Adv - B - WTQ") == itm
    assert fn("B Inter Advanced Waltz Tango Quickstep") == itm

    itm = ev.IntermediateWTVQ
    assert fn("Intermediate W T V Q") == itm
    assert fn("Intermediate WTVQ") == itm
    assert fn("Inter WTVQ") == itm
    assert fn("Int WTVQ") == itm

    itm = ev.AdvancedWTVQ
    assert fn("Advanced Ballroom") == itm
    assert fn("Advanced W T V Q") == itm
    assert fn("Advanced WTVQ") == itm
    assert fn("Adv WTVQ") == itm

    itm = ev.AdvancedWFQ
    assert fn("Advanced WFQ") == itm
    assert fn("Adv WFQ") == itm

    itm = ev.OpenF
    assert fn("Open Foxtrot") == itm
    assert fn("Open Fox") == itm
    assert fn("Open F") == itm

    itm = make_int_adv(ev.AdvancedF)
    assert fn("Intermediate Advanced Foxtrot") == itm
    assert fn("InterAdvanced Foxtrot") == itm
    assert fn("IntAdv Foxtrot") == itm

    itm = ev.OpenV
    assert fn("Open Viennese Waltz") == itm
    assert fn("Open Viennese") == itm
    assert fn("Open VW") == itm
    assert fn("Open V") == itm
    assert fn("OpenViennese Waltz") == itm
    assert fn("Open Vienese Waltz") == itm

    itm = ev.OpenT
    assert fn("Open Tango") == itm
    assert fn("Open T") == itm

    itm = ev.OpenWTVFQ
    assert fn("Open 5 Dance Ballroom WTVFQ") == itm
    assert fn("Open 5 Dance Ballroom") == itm
    assert fn("Open WTVFQ") == itm

    itm = ev.BeginnerC
    assert fn("Beginner Cha") == itm
    assert fn("Beginners Cha") == itm
    assert fn("Beg Cha") == itm
    assert fn("Beginner C") == itm
    assert fn("Beg C") == itm
    assert fn("Beginner ChaCha") == itm
    assert fn("Beginners ChaCha") == itm
    assert fn("Beg ChaCha") == itm
    assert fn("Beginner Cha Cha") == itm
    assert fn("Beginners Cha Cha") == itm
    assert fn("Beg Cha Cha") == itm
    assert fn("Beginner Cha-Cha") == itm
    assert fn("Beginners Cha-Cha") == itm
    assert fn("Beg Cha-Cha") == itm
    assert fn("Beginner Cha-Cha-Cha") == itm
    assert fn("Beginners Cha-Cha-Cha") == itm
    assert fn("Beg Cha-Cha-Cha") == itm
    assert fn("Beginner Cha Cha Cha") == itm
    assert fn("Beginners Cha Cha Cha") == itm
    assert fn("Beg Cha Cha Cha") == itm
    assert fn("Beginner CCC") == itm
    assert fn("Beginners CCC") == itm
    assert fn("Beg CCC") == itm

    itm = ev.BeginnerJ
    assert fn("Beginner Jive") == itm
    assert fn("Beginners Jive") == itm
    assert fn("Beg Jive") == itm
    assert fn("Beginner J") == itm
    assert fn("Beg J") == itm

    itm = ev.BeginnerCJ
    assert fn("Beginner Cha Cha Jive") == itm
    assert fn("Beginner Cha Jive") == itm
    assert fn("Beginner C J") == itm
    assert fn("Beginner CJ") == itm

    itm = ev.NoviceC
    assert fn("Novice Cha Cha") == itm
    assert fn("Novice C") == itm

    itm = ev.NoviceJ
    assert fn("Novice Jive") == itm
    assert fn("Novice J") == itm

    itm = ev.NoviceCJ
    assert fn("Novice Latin") == itm
    assert fn("Novice Cha Cha Jive") == itm
    assert fn("Novice Cha Jive") == itm
    assert fn("Novice CJ") == itm
    assert fn("Nov CJ") == itm

    itm = ev.NoviceCSJ
    assert fn("Novice C S J") == itm
    assert fn("Novice CSJ") == itm

    itm = make_div_1(ev.NoviceCJ)
    assert fn("Novice 1 Latin") == itm

    itm = make_div_2(ev.NoviceCJ)
    assert fn("Novice 2 Latin") == itm
    assert fn("Novice 2 CJ") == itm
    assert fn("First Year Novice CJ") == itm
    assert fn("Novice CJ Qualifier B") == itm

    itm = make_div_1(ev.NoviceCJ)
    assert fn("Novice CJ Qualifier") == itm
    assert fn("Novice CJ Qualifier A") == itm

    itm = make_same_sex_beg_nov(ev.NoviceCJ)
    assert fn("BeginnerNovice Same-sex 2-Dance (CJ)") == itm
    assert fn("Same-Sex BegNov CJ") == itm
    assert fn("BegNov SameSex CJ") == itm

    itm = make_beg_nov(ev.NoviceR)
    assert fn("BeginnerNovice Rumba") == itm
    assert fn("Beginner Novice Basic Rumba") == itm
    assert fn("Beginner Novice Rumba") == itm
    assert fn("BeginnerNovice R") == itm
    assert fn("Beg Nov Rumba") == itm
    assert fn("BegNov R") == itm

    itm = make_beg_nov(ev.NoviceS)
    assert fn("BeginnerNovice Samba") == itm
    assert fn("Beginner Novice Basic Samba") == itm
    assert fn("Beginner Novice Samba") == itm
    assert fn("BeginnerNovice S") == itm
    assert fn("Beg Nov Samba") == itm
    assert fn("BegNov S") == itm

    itm = make_ex_student(ev.NoviceCJ)
    assert fn("Ex-Student Novice Latin") == itm
    assert fn("Ex Student Novice Latin") == itm
    assert fn("Ex-Student Novice CJ") == itm
    assert fn("Ex Student Novice CJ") == itm

    itm = make_ex_student(ev.OpenCS)
    assert fn("Ex Student Open CS") == itm
    assert fn("Ex-Student Open CS") == itm

    itm = ev.PreIntermediateCJ
    assert fn("Pre-Intermediate Latin") == itm
    assert fn("Pre-Intermediate 2-Dance (CJ)") == itm
    assert fn("Pre-Int CJ") == itm

    itm = ev.IntermediateCRJ
    assert fn("Intermediate Latin") == itm

    itm = ev.IntermediateCSJ
    assert fn("Intermediate CSJ") == itm

    itm = make_int_adv_div_2(ev.AdvancedCSJ)
    assert fn("Intermediate 2 CSJ") == itm
    assert fn("B Inter Advanced Cha Cha Samba Jive") == itm

    itm = ev.IntermediateCSRJ
    assert fn("Intermediate C S R J") == itm
    assert fn("Intermediate CSRJ") == itm

    itm = make_ex_student(ev.IntermediateCRJ)
    assert fn("Ex-Student Intermediate Latin") == itm

    itm = make_int_adv(ev.AdvancedCJ)
    assert fn("Int-Adv Qualifier Round 1 CJ") == itm

    itm = ev.AdvancedCRJ
    assert fn("Advanced 3-Dance (CRJ)") == itm

    itm = make_int_adv(ev.AdvancedCRJ)
    assert fn("Intermediate-Advanced CJR") == itm
    assert fn("Intermediate-Advanced CRJ") == itm

    itm = make_int_adv_div_1(ev.AdvancedCSJ)
    assert fn("Inter Adv  - A - CSJ") == itm
    assert fn("Inter Advanced Cha Cha Samba Jive") == itm

    itm = make_int_adv_div_2(ev.AdvancedCSJ)
    assert fn("Int Adv - B - CSJ") == itm

    itm = ev.AdvancedCSJ
    assert fn("Advanced CSJ") == itm

    itm = ev.AdvancedCRPJ
    assert fn("Advanced Latin") == itm

    itm = ev.AdvancedCSRJ
    assert fn("Advanced C S R J") == itm
    assert fn("Advanced CSRJ") == itm

    itm = make_ex_student(ev.AdvancedCRPJ)
    assert fn("Ex-Student Advanced Latin") == itm
    assert fn("Ex Student Advanced Latin") == itm

    itm = make_same_sex(ev.OpenCJ)
    assert fn("Same Sex CJ") == itm
    assert fn("SS CJ") == itm

    itm = make_same_sex(ev.OpenCJ)
    assert fn("Same-Sex IntAdv CJ") == itm

    itm = make_same_sex(ev.OpenCRJ)
    assert fn("Same-Sex Latin") == itm

    itm = make_same_sex(ev.OpenCSJ)
    assert fn("Same Sex C S J") == itm
    assert fn("Same-Sex C S J") == itm
    assert fn("Same sex C S J") == itm
    assert fn("Same-sex C S J") == itm
    assert fn("Same Sex CSJ") == itm
    assert fn("Same-Sex CSJ") == itm
    assert fn("Same sex CSJ") == itm
    assert fn("Same-sex CSJ") == itm
    assert fn("Same Sex Cha Cha Samba Jive") == itm
    assert fn("Same-Sex Cha Cha Samba Jive") == itm
    assert fn("Same sex Cha Cha Samba Jive") == itm
    assert fn("Same-sex Cha Cha Samba Jive") == itm

    itm = make_ex_student(ev.OpenCRJ)
    assert fn("Ex Student CRJ") == itm

    itm = ev.OpenS
    assert fn("Open Samba") == itm
    assert fn("Open S") == itm

    itm = ev.OpenP
    assert fn("Open Paso Doble") == itm
    assert fn("Open Paso") == itm
    assert fn("Open P") == itm

    itm = make_int_adv(ev.AdvancedP)
    assert fn("Intermediate Advanced Paso") == itm
    assert fn("IntermediateAdvanced Paso") == itm
    assert fn("InterAdvanced Paso") == itm
    assert fn("IntAdv Paso") == itm
    assert fn("IntAdv P") == itm

    itm = ev.OpenR
    assert fn("Open Rumba") == itm
    assert fn("Open R") == itm

    itm = ev.OpenCSRPJ
    assert fn("Open 5 Dance Latin CSRPJ") == itm
    assert fn("Open 5 Dance Latin") == itm
    assert fn("Open CSRPJ") == itm

    itm = ev.OpenSalsa
    assert fn("Open Salsa") == itm
    assert fn("Salsa") == itm

    itm = ev.NonAcroRnR
    assert fn("Non-acrobatic Rock n Roll") == itm
    assert fn("Non-acro Rock n Roll") == itm
    assert fn("Non-Acrobatic Rock n Roll") == itm
    assert fn("Non-Acro Rock n Roll") == itm
    assert fn("Non-acrobatic RnR") == itm
    assert fn("Non-acro RnR") == itm
    assert fn("Non-Acrobatic RnR") == itm
    assert fn("Non-Acro RnR") == itm
    assert fn("Rock n Roll") == itm

    itm = ev.AcroRnR
    assert fn("Acro RnR") == itm
    assert fn("Acrobatic RnR") == itm

    itm = deepcopy(ev.ClassicalSequence)
    itm.name = itm.name + " - Mayfair Quickstep"
    assert fn("Mayfair Quickstep") == itm

    itm = ev.TeamMatch
    assert fn("Team Match") == itm

    assert fn("NL Under 35s Intermediate Latin")
    assert fn("SL Foo Juvenile Ballroom")
