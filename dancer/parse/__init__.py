"""Parse input files.
"""

import json
import os
import re
import logging
from pydantic import FilePath, DirectoryPath, validate_arguments
from dancer.couple import Couple
from dancer.competition import (
    Event,
    Category,
    Adjudicator,
    Round,
    Marks,
    CoupleRound,
    Final,
    Placings,
    CoupleFinal,
)
from dancer import competition_events as ev
from typing import Dict, List, Optional
from .category import event_category
from .couple import parse_couple_str


LOG = logging.getLogger(__name__)


def fmt(jsn: str):
    return json.dumps(json.loads(jsn), indent=4)


def _competitors(filename: str) -> Dict[int, Couple]:
    """Read competitors from file."""

    retval = {}
    with open(filename, "r", encoding="utf-8") as fil:
        regexp = re.compile(r"^[0-9]+")
        for line in fil:
            res = regexp.match(line)
            if not res:
                continue
            num = res.group()
            retval[int(num)] = parse_couple_str(line[res.span()[-1] :])

    return retval


def competitors(directory: str) -> Dict[int, Couple]:
    """Read competitors from directory."""

    files = os.listdir(directory)
    regexp = re.compile(r"^[C|c]ompetitors.txt")

    files = [regexp.match(fil) for fil in files]
    files = [fil for fil in files if fil]

    if len(files) >= 1:
        return _competitors(os.path.join(directory, files[0].group(0)))
    LOG.error("No competitors file found.")
    return {}


def round_1_events(directory: str) -> List[re.Match]:
    """Return a list of round 1 events."""

    files = os.listdir(directory)
    regexp = re.compile(r"^([A-Z]+)?([0-9]+)( - )?(.*?)( - )?(Round 1) ?(.*)?.txt")

    round_1_files = [regexp.match(fil) for fil in files]
    round_1_files = [fil for fil in round_1_files if fil]
    return round_1_files


def final_events(directory: str) -> List[re.Match]:
    """Return a list of final events."""

    files = os.listdir(directory)
    regexp = re.compile(r"^([A-Z]+)?([0-9]+)( - )?(.*?)( - )?(Final) ?(.*)?.txt")

    files = [regexp.match(fil) for fil in files]
    files = [fil for fil in files if fil]
    return files


def events(directory: str) -> Dict[int, Event]:
    """
    Search a directory for 'Round 1' txt files and 'Final' files
    and import them as events,
    with the competitors as well.

    This ensures that all competitors are imported correctly, as there will
    either be a first round and a final or a final.
    """

    round_1_files = round_1_events(directory)
    # we parse finals as well so we can get straight finals
    finals = final_events(directory)
    retval = {}

    for event in round_1_files:
        _league = event.group(1)
        num = int(event.group(2))
        category = event_category(event.group(4).strip())
        entries = event_entries("{0}/{1}".format(directory, event.group()))
        print(category)
        print(entries)
        if category:
            retval[num] = Event(dance=category, entries=entries)

    for event in finals:
        # if we haven't got it we should parse it
        num = int(event.group(2))
        category = event_category(event.group(4).strip())
        entries = final_entries("{0}/{1}".format(directory, event.group()))
        if num not in retval:
            if category:
                retval[num] = Event(dance=category, entries=entries)

    print(retval)

    return retval


def event_entries(filename: str) -> List[int]:
    """Import a 'Round 1' file and create a competitor list from it."""

    retval = []
    with open(filename, "r", encoding="utf-8") as fil:
        regexp1 = r"TotalRecall"
        regexp2 = r"^\s*$"
        regexp3 = r"^ *([0-9]+)"
        ready = False
        for line in fil:
            if "Recall" in line:
                # we have reached the start of numbers / heats
                ready = True
                continue
            if not ready:
                continue
            res = re.match(regexp3, line)
            if not res:
                # most likely describes a heat
                continue
            num = res.group(1)
            retval.append(int(num))
    return retval


def final_entries(filename: str) -> List[int]:
    """Import a 'Final' file and create a competitor list from it."""

    retval = []
    with open(filename, "r", encoding="utf-8") as fil:
        regexp1 = r"TotalResult"
        regexp2 = r"^\s*$"
        regexp3 = r"^\s*([0-9]+)"
        ready = False
        for line in fil:
            if "TotalResult" in line:
                # we have reached the start of numbers / heats
                ready = True
                continue
            if not ready:
                continue
            if re.match(regexp2, line):
                # we have reached the end
                break
            res = re.match(regexp3, line)
            if not res:
                # most likely describes a heat
                continue
            num = res.group(1)
            retval.append(int(num))
    return retval


def judges(directory: str) -> Dict[str, Adjudicator]:
    """Parse directory for judges."""

    round_1_files = round_1_events(directory)
    # TODO handle multiple events having different judges
    if len(round_1_files) > 0:
        return _judges("{0}/{1}".format(directory, round_1_files[0].group(0)))

    # try the finals
    finals = final_events(directory)
    if len(finals) > 0:
        return _judges("{0}/{1}".format(directory, finals[0].group(0)))

    LOG.error("No round 1 or final files found - cannot parse judges!")
    return {}


def _judges(filename: str) -> Dict[str, Adjudicator]:
    """Parse file for the names and IDs of adjudicators."""

    retval = {}
    regexp = re.compile(r"^\s*([A-Z]) - (.*)$")
    ready = False
    with open(filename, "r", encoding="utf-8") as fil:
        for line in fil:
            if "Adjudicators" in line:
                ready = True
                continue
            if not ready:
                continue
            res = regexp.match(line)
            if not res:
                break
            retval[res.group(1)] = Adjudicator(name=res.group(2))
    return retval


def _parse_round_marks(string: str, judges: str) -> Marks:
    """Parse a string of X's, the position of which marks their mark
    given to them by the corrresponding judge in judges.
    """

    result = ""
    num_judges = len(judges)

    first_set = string[:num_judges]
    last_set = string[num_judges:]
    if "X" in last_set:
        # there are marks in the last set
        # this is an issue in the parsing of the file
        for i in range(len(string) - 1, -1, -1):
            if string[i] == "X":
                break
        first_set = string[i - 4 : i + 1]
        # assert len(first_set) == num_judges

    # bad loop
    for index, mark in enumerate(first_set):
        if mark == "X":
            # marked
            result += judges[index]
    return Marks(__root__=result)


def _round(filename: str) -> Round:
    """Parse filename as a round."""

    retval = {}
    regexp1 = re.compile(r"([A-Z][A-Z]+)")
    regexp2 = re.compile(r"TotalRecall$")
    regexp3 = re.compile(r"([0-9]+) ? ?([ X]*)?")
    regexp4 = re.compile(r".+(-)$")
    ready = False
    judges = None
    events = None
    with open(filename, "r", encoding="utf-8") as fil:
        for line in fil:
            match = regexp2.match(line)
            # matching the number of judges - minimum 3
            if ready is True and line.strip() == "Recall":
                # handle an edge case
                continue
            if "ABC" in line:
                ready = True
                new_events = regexp1.findall(line)
                if not events:
                    events = new_events
                else:
                    events += new_events
                if not judges:
                    judges = events[0]
                num_judges = len(judges)
                num_events = len(events)
                continue

            if not ready:
                continue

            couple = regexp3.findall(line)
            if not couple:
                # end of results block
                # may be more results later
                ready = False
                continue
            if len(couple) != num_events:
                couple = couple[:-1]
            couple_numbers = {tup[0] for tup in couple}
            if len(couple_numbers) != 1:
                LOG.error(line)
                raise RuntimeError("Appears to be two couples on one line: %s" % line)
            num = int(list(couple_numbers)[0])
            marks = [_parse_round_marks(tup[1], judges) for tup in couple]

            recall = regexp4.match(line)
            recalled = False
            if recall:
                if recall.group(1) == "-":
                    recalled = True
            if retval.get(num):
                # already exists
                retval[num].marks += marks
                retval[num].recalled = recalled
            else:
                retval[num] = CoupleRound(marks=marks, recalled=recalled)
    return Round(couples=retval)


def rounds(directory: str, event: int) -> Dict[int, Round]:
    """Parse all round files in directory for the given event number."""

    retval = []

    files = os.listdir(directory)
    regexp = r"^(0?{}) (.*) - Round ([0-9]+) (.*).txt".format(event)

    files = [re.match(regexp, fil) for fil in files]
    files = [fil for fil in files if fil]
    files = sorted(files, key=lambda x: int(x.group(3)))
    rounds = [int(obj.group(3)) for obj in files]
    LOG.debug("event %d has %d rounds before final: %s", event, len(rounds), rounds)
    if len(rounds) == 0:
        LOG.info("Event %d seems to be a straight final")
        return {}
    if 1 not in rounds:
        raise RuntimeError("Missing first round!")
    prev = 0
    for i in rounds:
        if i - prev != 1:
            if i == prev:
                # we probably have novice 1/2
                rounds[0] = 0
            else:
                raise RuntimeError("Missing at least one round: %s" % rounds)
        prev = i

    retval = {
        rnd: _round(
            "{0}/{1}".format(
                directory,
                obj.group(0),
            )
        )
        for rnd, obj in zip(rounds, files)
    }
    return retval


def _final(filename: str) -> Final:
    """Parse a Final file."""

    retval = {}
    regexp1 = re.compile(r"([A-Z ]+)([0-9 ]+)Placing")
    regexp2 = re.compile(r"\s*([0-9]+)  ([0-9 ]+)")
    regexp3 = re.compile(r"^.*Total\s*Result")
    regexp4 = re.compile(
        r"^\s*([0-9]+)([0-9 \.]+)([0-9])(\s+Rule ([0-9]+))?(\s+Rule ([0-9]+))?$"
    )
    ready = False
    summary = False
    with open(filename, "r", encoding="utf-8") as fil:
        for line in fil:
            match = regexp1.match(line)
            final_ranking = regexp3.match(line)
            if match:
                ready = True
                judges = match.group(1).replace(" ", "")
                num_judges = len(judges)
                continue
            if final_ranking:
                summary = True
                continue
            if summary:
                # parse summary here
                final = regexp4.match(line)
                if not final:
                    # this is probably the end
                    break
                num = int(final.group(1))
                placing = int(final.group(3))
                if not retval.get(num):
                    retval[num] = CoupleFinal()
                retval[num].add_final_place(placing)

            if not ready:
                continue

            couple = re.match(regexp2, line)
            if not couple:
                ready = False
                continue
            num = int(couple.group(1))
            placings = couple.group(2).replace(" ", "")[:num_judges]

            if not retval.get(num):
                # create new result entry
                retval[num] = CoupleFinal()

            ranking = {}
            for judge, place in zip(judges, placings):
                ranking[judge] = int(place)
            retval[num].add_placing(Placings(__root__=ranking))
    return Final(couples=retval)


def final(directory: str, event: int) -> Optional[Final]:
    """Parse the Final for the given event."""

    files = os.listdir(directory)
    regexp = r"^(0?{}) (.*) - Final (.*).txt".format(event)

    files = [re.match(regexp, fil) for fil in files]
    files = [fil for fil in files if fil]
    if len(files) != 1:
        LOG.error("No final found for event %d" % event)
        return None
    return _final("{0}/{1}".format(directory, files[0].group(0)))


@validate_arguments
def _competitors_in_final(filepath: FilePath, event: int) -> dict[int, Couple]:
    """Parse the final file for competitors."""

    retval = {}
    regexp1 = re.compile(r"\s*([1-9])([snrt][tdh])\s+=>\s+([0-9]+)\s+(.*)")

    with filepath.open("r", encoding="utf-8") as fil:
        ready = False
        has_been_two_lines = False
        for line in fil:
            match = regexp1.match(line.strip())
            if match:
                retval[int(match.group(3))] = parse_couple_str(match.group(4))
            if match and not ready:
                LOG.debug(
                    "found competitors in final of event %d",
                    event,
                )
                ready = True
                continue
            if ready and not bool(match):
                if has_been_two_lines:
                    LOG.debug(
                        "finished parsing final of event %d for competitors: %d found",
                        event,
                        len(retval),
                    )
                    break
                # set flag to avoid early break
                has_been_two_lines = True
                continue
            has_been_two_lines = False
    return retval


@validate_arguments
def competitors_in_final(directory: DirectoryPath, event: int) -> dict[int, Couple]:
    """Parse competitors from a final."""

    regexp = r"^(0?{}) (.*) - Final (.*).txt".format(event)
    files = [re.match(regexp, fil.name) for fil in directory.iterdir()]
    files = [fil for fil in files if fil]
    if len(files) != 1:
        LOG.error("No final found for event %d" % event)
        return None
    return _competitors_in_final(directory.joinpath(files[0].group(0)), event)
