"""Parse Open Circuit competition events correctly.
"""

import re
import logging
from typing import Optional
from dancer.competition import Category
from dancer.league import League
from dancer.age_restriction import AgeRestriction


LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)

_NL_REGEXP = re.compile('(NL) +([a-zA-Z]+[ ]+?([a-zB-Z0-9]+)?( 10s)?) *?-? *?([a-zA-Z]+[ ]+?([a-zA-Z]+)?)?([a-zA-Z0-9 ]+)?')
_SL_REGEXP = re.compile('(SL) +([a-zA-Z]+)? ?([a-zA-Z]+ ?[a-zA-Z0-9]+?)? ([a-zA-Z0-9 ]+)')
_UNK_REGEXP = re.compile('([a-zA-Z]+ [a-zA-Z0-9]+) (.*)')


def super_league(name: str) -> Optional[Category]:
    """Optionall create a super league category from this event name."""

    match = _SL_REGEXP.match(name)
    if not match:
        LOG.debug('did not match SL event: %s', name)
        return None

    league = League.from_str(match.group(1))
    age_category = AgeRestriction.from_str(match.group(3))
    return Category(
        league=league,
        age_restriction=age_category,
        dances=[],
        name='%s%s' % (
            '%s ' % match.group(2) if match.group(2) else '',
            match.group(4),
        ),
    )


def national_league(name: str) -> Optional[Category]:
    """Optionall create a national league category from this event name."""

    LOG.info(name)
    match = _NL_REGEXP.match(name)
    if not match:
        LOG.debug('did not match NL event: %s', name)
        return None

    league = League.from_str(match.group(1))
    age_cat_str = match.group(2).strip()
    age_category = AgeRestriction.from_str(age_cat_str)
    if match.group(7):
        name = match.group(7).strip()
    else:
        name = match.group(6) or match.group(3)
        if not name:
            LOG.error('Missing Name for event: %s', name)
            return None
        name = name.strip()
    return Category(
        league=league,
        age_restriction=age_category,
        dances=[],
        name=name,
    )


def unknown(name: str) -> Optional[Category]:
    """Optionally parse a category."""

    match = _UNK_REGEXP.match(name)
    if not match:
        LOG.error('Could not match this event: %s', name)
        return None

    return Category(
        league=League.UNKNOWN,
        age_restriction=AgeRestriction.from_str(match.group(1)),
        dances=[],
        name=match.group(2),
    )
