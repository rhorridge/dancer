"""Parse couple names."""

import logging
from dancer.competition import Couple
from .university import university_name


LOG = logging.getLogger(__name__)


def parse_couple_str(option: str) -> Couple:
    """Parses a string as a Couple."""

    university = None
    names = option.split(" and ")
    if len(names) != 2:
        # try &
        names = option.split("&")

    if len(names) == 1:
        LOG.debug("Trying to parse as Leader Leader Follower Follower University")
        leader_follower, university = university_name(option)
        rests = leader_follower.split()
        LOG.debug("%d names in leader/follower string", len(rests))
        if len(rests) == 4:
            leader = " ".join(rests[:2]).strip()
            follower = " ".join(rests[2:]).strip()
        else:
            LOG.warning(
                "Not possible to easily determine leader/follower from %s", rests
            )
            leader = " ".join(rests[:2]).strip()
            follower = " ".join(rests[2:]).strip()

    elif len(names) != 2:
        LOG.error("Could not parse names for couple %s!", option)
        leader = option
        follower = "?"
    else:
        leader = names[0].strip()
        follower = names[1].strip()
        follower, university = university_name(follower)

    return Couple(
        leader=leader,
        follower=follower,
        university=university,
    )
