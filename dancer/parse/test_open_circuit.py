"""Unit tests for dancer.parse.open_circuit.
"""

from dancer.parse import open_circuit


def test_super_league():
    """Test for super league events."""

    fn = open_circuit.super_league

    res = fn("SL Stars Juvenile Ballroom")
    assert res.name == 'Stars Ballroom'
    assert res.age_restriction.age == 10

    res = fn("SL Stars Junior Ballroom")
    assert res.name == 'Stars Ballroom'
    assert res.age_restriction.age == 14

    res = fn("SL Stars Under 21s Open Ballroom")
    assert res.name == 'Stars Open Ballroom'
    assert res.age_restriction.age == 21

    res = fn("SL Stars Amateur Ballroom")
    assert res.name == 'Stars Ballroom'
    assert res.age_restriction is None

    res = fn("SL Stars Juvenile Latin")
    assert res.name == 'Stars Latin'
    assert res.age_restriction.age == 10

    res = fn("SL Stars Junior Latin")
    assert res.name == 'Stars Latin'
    assert res.age_restriction.age == 14

    res = fn("SL Stars Under 21s Open Latin")
    assert res.name == 'Stars Open Latin'
    assert res.age_restriction.age == 21

    res = fn("SL Stars Amateur Latin")
    assert res.name == 'Stars Latin'
    assert res.age_restriction is None


def test_national_league():
    """Test for national league events."""

    fn = open_circuit.national_league

    res = fn("NL Juvenile - Beginners Ballroom")
    res = fn("NL Juvenile All Girl Ballroom")
    res = fn("NL Juvenile Under 10s Ballroom")
    res = fn("NL Junior Beginners Ballroom")
    res = fn("NL Junior All Girl Ballroom")
    res = fn("NL Under 14s Open Ballroom")
    res = fn("NL Under 35s Beginners Ballroom")
    res = fn("NL Over 35s - Beginners Ballroom")
    res = fn("NL Under 35s  Novice Ballroom")
    res = fn("NL Over 35s  Novice Ballroom")
    res = fn("NL U35s - Intermediate Ballroom")
    res = fn("NL O35s Intermediate Ballroom")
    res = fn("NL U35s  Pre Amateur  Ballroom")
    res = fn("NL O35s Pre Amateur Ballroom")
    res = fn("NL Juvenile Beginners Latin")
    res = fn("NL Juvenile All Girl Latin")
    res = fn("NL Juvenile Under 10s Latin")
    res = fn("NL Junior -Beginners Latin")
    res = fn("NL Junior All Girl Latin")
    res = fn("NL Under 14s Open Latin")
    res = fn("NL Under 35s Beginners Latin")
    res = fn("NL Over 35s Beginners Latin")
    res = fn("NL Under 35s Novice Latin")
    res = fn("NL Over 35s Novice Latin")
    res = fn("NL Under 35s Intermediate Latin")
    res = fn("NL Over 35s Intermediate Latin")
    res = fn("NL Under 35s Pre Amateur Latin")
    res = fn("NL Over 35s Pre Amateur Latin")

    res = fn("NL Foobar")


def test_unknown():
    """Test for unknown events."""

    fn = open_circuit.unknown
    res = fn("Under 35s Latin")
    assert res.age_restriction.age == 35
    res = fn("")
