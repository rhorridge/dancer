"""Represents possible different leagues.
"""

from enum import Enum


class League(str, Enum):
    """Possible leagues e.g. university circuit, open circuit national league
    medallist etc.
    """

    UNKNOWN = 'UNKNOWN'
    OPEN_CIRCUIT_NATIONAL_LEAGUE = 'NL'
    OPEN_CIRCUIT_SUPER_LEAGUE = 'SL'
    UNIVERSITY_CIRCUIT = 'UNI'

    @classmethod
    def from_str(cls, string: str) -> object:
        """Convert this into a string."""

        match string:
            case 'NL':
                return cls.OPEN_CIRCUIT_NATIONAL_LEAGUE
            case 'SL':
                return cls.OPEN_CIRCUIT_SUPER_LEAGUE
        return cls.UNKNOWN
