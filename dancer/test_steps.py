"""Unit tests for steps.
"""

import random
from dancer import choreography as stp


def test_prev_next():
    start = stp.syllabus_figures.ballroom.NaturalTurn
    next_ = random.choice(start.next(figures=stp.ALL_FIGURES))
    assert next_.start_foot.samefoot(start.next_foot)


def test_generate_routine():
    routine = []
    step = stp.syllabus_figures.ballroom.NaturalTurn
    for i in range(20):
        routine.append(step)
        nxt = step.next(figures=stp.ALL_FIGURES)
        step = random.choice(nxt)

    print([step.name for step in routine])
