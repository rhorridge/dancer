from enum import Enum


class Foot(str, Enum):
    """Represents the foot (Left or Right)."""

    LEFT_FOOT = "LF"
    RIGHT_FOOT = "RF"


class FootPlacement(str, Enum):
    """Represents a placement with the foot e.g. Ball of foot."""

    BALL_OF_FOOT = "B"
    HEEL = "H"
    TOE = "T"
    INSIDE_EDGE = "IE"


class Direction(str, Enum):
    """Represents a direction (Left or Right)."""

    LEFT = "L"
    RIGHT = "R"


class BodyPosition(str, Enum):
    """Represents a position with the body e.g. Promenade, CBM."""

    PROMENADE_POSITION = "PP"
    CONTRARY_BODY_MOVEMENT_POSITION = "CBMP"
    CONTRARY_BODY_MOVEMENT = "CBM"
    NO_FOOT_RISE = "NFR"
    OUTSIDE_PARTNER = "OP"


class MovementDirection(str, Enum):
    """Represents a direction around the floor."""

    LINE_OF_DANCE = "LOD"
    DIAGONALLY_TO_WALL = "DW"
    DIAGONALLY_TO_CENTRE = "DC"


class Rhythm(str, Enum):
    """Represents a point in the music."""

    END_OF = "e/o"
    COMMENCE = "Com"
    CONTINUE = "Cont"
    SLOW = "S"
    QUICK = "Q"


class ISTDLicenseType(str, Enum):
    """Represents a type of license with the Imperial Society of
    Teachers of Dancing.
    """

    ASSOCIATE = "A"
    LICENTIATE = "L"
    FELLOW = "F"
