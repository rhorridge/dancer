"""
Represents the syllabus steps for each dance. This uses sets to store
the individual step objects so can easily be checked for membership.
"""

import random
from typing import Optional
from pydantic import BaseModel
import dancer.choreography as stp
from dancer.steps import Figure, RoutineError


class Syllabus(BaseModel):
    """Represents the syllabus steps for a specific dance, or a specific
    syllabus according to an individual organisation.
    """

    name: str
    alt_name: Optional[str] = None
    figures: list[Figure]

    def __eq__(self, other) -> bool:
        return self.name == other.name and self.figures == other.figures

    def __hash__(self) -> bytes:
        return hash((name, figures))

    def routine(self, length=10):
        """
        Generate a random routine using ONLY syllabus steps.

        TODO: add more features e.g. starting with prep step, starting in
        different places, using certain levels of figure, avoiding certain
        figures.
        """

        step = random.choice(list(self.figures))
        routine = []
        for _ in range(length):
            routine.append(step)
            possible = [stp for stp in step.next(figures=self.figures)]
            if not possible:
                raise RoutineError(
                    "No valid next steps from %s in %s" % (step.name, self.name)
                )
            step = random.choice(possible)

        return routine

    def validate(self, routine: object):
        """
        Validate ROUTINE against this syllabus. If any steps are not from
        the syllabus, a RoutineError will be raised.
        """

        for step in routine.steps:
            if step not in self.figures:
                # step does not appear in allowed steps
                raise RoutineError(
                    "%s is not a syllabus %s step" % (step.name, self.name)
                )


"""The syllabus steps for the Waltz, as listed in the BDC rulebook."""
WaltzSyllabus = Syllabus(
    name="Waltz",
    alt_name="Slow Waltz",
    figures=[
        stp.waltz.ClosedChangeRF,
        stp.waltz.ClosedChangeLF,
        stp.waltz.NaturalTurn,
        stp.waltz.NaturalTurn1_3,
        stp.waltz.ReverseTurn,
        stp.waltz.NaturalSpinTurn,
        stp.waltz.NaturalSpinTurn1_3,
        stp.waltz.Whisk,
        stp.waltz.ChasseFromPP,
        stp.waltz.HesitationChange,
        stp.waltz.ProgressiveChasseToR,
        stp.waltz.ClosedImpetus,
        stp.waltz.OutsideChange,
        stp.waltz.ReverseCorte,
        stp.waltz.BackWhisk,
        stp.waltz.BasicWeave,
        stp.waltz.DoubleReverseSpin,
        stp.waltz.DragHesitation,
        stp.waltz.BackwardLockStep,
        stp.waltz.ForwardLockStep,
        stp.waltz.ReversePivot,
        stp.waltz.ClosedTelemark,
        stp.waltz.OpenTelemark,
        stp.waltz.CrossHesitation,
        stp.waltz.Wing,
        stp.waltz.OpenImpetus,
        stp.waltz.WeaveFromPP,
        stp.waltz.OutsideSpin,
        stp.waltz.TurningLockToL,
        stp.waltz.TurningLockToR,
        stp.waltz.ClosedWing,
        stp.waltz.FallawayReverseSlipPivot,
        stp.waltz.HoverCorte,
        stp.waltz.FallawayNaturalTurn,
        stp.waltz.RunningSpinTurn,
        stp.waltz.FallawayWhisk,
    ],
)

"""The syllabus steps for the Quickstep, as listed in the BDC rulebook."""
QuickstepSyllabus = Syllabus(
    name="Quickstep",
    figures=[
        stp.quickstep.WalkLF,
        stp.quickstep.WalkRF,
        stp.quickstep.ProgressiveChasseToL,
        stp.waltz.NaturalTurn,
        stp.waltz.NaturalTurn1_3,
        stp.waltz.HesitationChange,
        stp.quickstep.NaturalPivotTurn,
        stp.waltz.NaturalSpinTurn,
        stp.waltz.NaturalSpinTurn1_3,
        stp.waltz.ProgressiveChasseToR,
        stp.waltz.ForwardLockStep,
        stp.quickstep.ForwardLockStep2_4,
        stp.quickstep.BackwardLockStep,
        stp.quickstep.BackwardLockStep2_4,
        stp.waltz.ReversePivot,
        stp.quickstep.TippleChasseToR,
        stp.quickstep.TippleChasseToR1_4,
        stp.quickstep.RunningFinish,
        stp.quickstep.OpenRunningFinish,
        stp.waltz.DoubleReverseSpin,
        stp.waltz.ClosedImpetus,
        stp.quickstep.QuickOpenReverse,
        stp.quickstep.ZigZag,
        stp.quickstep.Fishtail,
        stp.quickstep.RunningRightTurn,
        stp.quickstep.FourQuickRun,
        stp.quickstep.V6,
        stp.waltz.ClosedTelemark,
        stp.quickstep.TippleChasseToL,
        stp.quickstep.TippleChasseToL1_4,
        stp.quickstep.CrossSwivel,
        stp.quickstep.RunningCrossChasse,
        stp.quickstep.PassingNaturalTurn,
        stp.quickstep.SixQuickRun,
        stp.quickstep.RumbaCross,
        stp.quickstep.TipsyToR,
        stp.quickstep.TipsyToL,
        stp.quickstep.TipsyToL2_4,
        stp.waltz.HoverCorte,
    ],
)


"""The syllabus steps for the Foxtrot, as listed in the BDC rulebook."""
FoxtrotSyllabus = Syllabus(
    name="Foxtrot",
    alt_name="Slow Foxtrot",
    figures=[
        stp.quickstep.WalkLF,
        stp.quickstep.WalkRF,
        stp.foxtrot.FeatherStep,
        stp.foxtrot.ThreeStep,
        stp.waltz.NaturalTurn,
        stp.waltz.NaturalTurn1_3,
        stp.waltz.ReverseTurn,
        stp.foxtrot.ReverseTurn1_3,
        stp.foxtrot.ChangeOfDirection,
        stp.waltz.ClosedImpetus,
        stp.foxtrot.FeatherFinish,
        stp.foxtrot.NaturalWeave,
        stp.foxtrot.BasicWeave2,
        stp.foxtrot.ReverseWave,
        stp.waltz.ClosedTelemark,
        stp.waltz.OpenTelemark,
        stp.foxtrot.FeatherEnding,
        stp.foxtrot.TopSpin,
        stp.foxtrot.HoverFeather,
        stp.foxtrot.HoverTelemark,
        stp.foxtrot.NaturalTelemark,
        stp.foxtrot.HoverCross,
        stp.foxtrot.OutsideSwivel,
        stp.waltz.OpenImpetus,
        stp.foxtrot.WeaveFromPP2,
        stp.foxtrot.NaturalTwistTurn,
        stp.waltz.ReversePivot,
        stp.quickstep.QuickOpenReverse,
        stp.foxtrot.QuickNaturalWeaveFromPP,
        stp.foxtrot.CurvedFeather,
        stp.foxtrot.BackFeather,
        stp.foxtrot.NaturalZigZagFromPP,
        stp.waltz.FallawayReverseSlipPivot,
        stp.foxtrot.NaturalHoverTelemark,
        stp.foxtrot.BounceFallawayWithWeaveEnding,
        stp.foxtrot.ExtendedReverseWave,
        stp.foxtrot.CurvedThreeStep,
    ],
)


"""The syllabus steps for the Tango, as listed in the BDC rulebook."""
TangoSyllabus = Syllabus(
    name="Tango",
    figures=[
        stp.quickstep.WalkLF,
        stp.quickstep.WalkRF,
        stp.tango.ProgressiveSideStep,
        stp.tango.ProgressiveLink,
        stp.tango.ClosedPromenade,
        stp.tango.OpenPromenade,
        stp.tango.NaturalRockTurn,
        stp.tango.BackCorte,
        stp.waltz.ReverseTurn,
        stp.foxtrot.ReverseTurn1_3,
        stp.tango.ProgressiveSideStepReverseTurn,
        stp.tango.RockOnLF,
        stp.tango.RockOnRF,
        stp.foxtrot.NaturalTwistTurn,
        stp.tango.NaturalPromenadeTurn,
        stp.tango.PromenadeLink,
        stp.tango.FourStep,
        stp.tango.BackOpenPromenade,
        stp.tango.OutsideSwivel2,
        stp.tango.FallawayPromenade,
        stp.tango.FourStepChange,
        stp.tango.BrushTap,
        stp.tango.FallawayFourStep,
        stp.tango.TheChase,
        stp.waltz.FallawayReverseSlipPivot,
        stp.tango.FiveStep,
        stp.tango.MiniFiveStep,
        stp.tango.OpenToPromenade,
    ],
)


"""The syllabus steps for the Viennese Waltz, as listed in the BDC rulebook."""
VienneseWaltzSyllabus = Syllabus(
    name="Viennese Waltz",
    figures=[
        stp.viennese_waltz.ReverseTurn,
        stp.foxtrot.ReverseTurn1_3,
        stp.viennese_waltz.ReverseTurn4_6,
        stp.waltz.NaturalTurn,
        stp.waltz.NaturalTurn1_3,
        stp.viennese_waltz.NaturalTurn4_6,
        stp.waltz.ClosedChangeRF,
        stp.waltz.ClosedChangeLF,
        stp.viennese_waltz.BackwardChangeRF,
        stp.viennese_waltz.BackwardChangeLF,
    ],
)

"""The syllabus steps for the Cha-Cha-Cha, as listed in the BDC rulebook."""
ChaChaChaSyllabus = Syllabus(
    name="Cha-Cha-Cha",
    figures=[
        stp.cha_cha_cha.BasicMovementClosedL,
        stp.cha_cha_cha.BasicMovementInPlaceL,
        stp.cha_cha_cha.BasicMovementOpenL,
        stp.cha_cha_cha.BasicMovementRShadowL,
        stp.cha_cha_cha.BasicMovementClosedR,
        stp.cha_cha_cha.BasicMovementInPlaceR,
        stp.cha_cha_cha.BasicMovementOpenR,
        stp.cha_cha_cha.BasicMovementRShadowR,
        stp.cha_cha_cha.ChaChaChaChasseToLeftClosed,
        stp.cha_cha_cha.ChaChaChaChasseToLeftOpen,
        stp.cha_cha_cha.ChaChaChaChasseToLeftRShadow,
        stp.cha_cha_cha.ChaChaChaChasseToRightClosed,
        stp.cha_cha_cha.ChaChaChaChasseToRightOpen,
        stp.cha_cha_cha.ChaChaChaChasseToRightRShadow,
        stp.cha_cha_cha.ChaChaChaLockForwardClosed,
        stp.cha_cha_cha.ChaChaChaLockForwardOpen,
        stp.cha_cha_cha.ChaChaChaLockBackwardClosed,
        stp.cha_cha_cha.ChaChaChaLockBackwardOpen,
        stp.cha_cha_cha.TimeStepL,
        stp.cha_cha_cha.TimeStepR,
        stp.cha_cha_cha.NewYorkL,
        stp.cha_cha_cha.NewYorkR,
        stp.cha_cha_cha.SpotTurnL,
        stp.cha_cha_cha.SpotTurnR,
        stp.cha_cha_cha.SwitchTurnL,
        stp.cha_cha_cha.SwitchTurnR,
        stp.cha_cha_cha.UnderarmTurnL,
        stp.cha_cha_cha.UnderarmTurnR,
        stp.cha_cha_cha.ShoulderToShoulderL,
        stp.cha_cha_cha.ShoulderToShoulderR,
        stp.cha_cha_cha.HandToHandR,
        stp.cha_cha_cha.HandToHandL,
        stp.cha_cha_cha.TheeChaChaChasForward,
        stp.cha_cha_cha.TheeChaChaChasBackward,
        stp.cha_cha_cha.SideStepToLeft,
        stp.cha_cha_cha.SideStepToRight,
        stp.cha_cha_cha.ThereAndBack,
        stp.cha_cha_cha.Fan,
        stp.cha_cha_cha.HockeyStick,
        stp.cha_cha_cha.Alemana,
        stp.cha_cha_cha.AlemanaFromOpenPosition,
        stp.cha_cha_cha.NaturalTop,
        stp.cha_cha_cha.ClosedHipTwist,
        stp.cha_cha_cha.ClosedHipTwistRShadow,
        stp.cha_cha_cha.OpenHipTwist,
        stp.cha_cha_cha.ReverseTop,
        stp.cha_cha_cha.ReverseTop1_10,
        stp.cha_cha_cha.OpeningOutFromReverseTop,
        stp.cha_cha_cha.Aida,
        stp.cha_cha_cha.Spiral,
        stp.cha_cha_cha.Curl,
        stp.cha_cha_cha.RopeSpinning,
        stp.cha_cha_cha.CrossBasic,
        stp.cha_cha_cha.CubanBreaksL,
        stp.cha_cha_cha.CubanBreaksR,
        stp.cha_cha_cha.SplitCubanBreaksL,
        stp.cha_cha_cha.SplitCubanBreaksR,
        stp.cha_cha_cha.CubanBreaksOpenL,
        stp.cha_cha_cha.CubanBreaksOpenR,
        stp.cha_cha_cha.SplitCubanBreaksOpenL,
        stp.cha_cha_cha.SplitCubanBreaksOpenR,
        stp.cha_cha_cha.Chase,
        stp.cha_cha_cha.RondeChasse,
        stp.cha_cha_cha.HipTwistChasse,
        stp.cha_cha_cha.SlipCloseChasse,
        stp.cha_cha_cha.FootChange1,
        stp.cha_cha_cha.FootChange2,
        stp.cha_cha_cha.FootChange3,
        stp.cha_cha_cha.FootChange4,
        stp.cha_cha_cha.TurkishTowel,
        stp.cha_cha_cha.Sweetheart,
        stp.cha_cha_cha.FollowMyLeader,
        stp.cha_cha_cha.ClosedHipTwistSpiral,
        stp.cha_cha_cha.OpenHipTwist1_5,
        stp.cha_cha_cha.OpenHipTwistSpiral,
    ],
)

"""The syllabus steps for the Rumba, as listed in the BDC rulebook."""
RumbaSyllabus = Syllabus(
    name="Rumba",
    figures=[
        stp.cha_cha_cha.BasicMovementClosedL,
        stp.cha_cha_cha.BasicMovementClosedR,
        stp.cha_cha_cha.BasicMovementOpenL,
        stp.cha_cha_cha.BasicMovementOpenR,
        stp.rumba.AlternativeBasicL,
        stp.rumba.AlternativeBasicR,
        stp.rumba.ForwardWalk,
        stp.rumba.BackwardWalk,
        stp.cha_cha_cha.Fan,
        stp.cha_cha_cha.HockeyStick,
        stp.cha_cha_cha.NewYorkL,
        stp.cha_cha_cha.NewYorkR,
        stp.cha_cha_cha.SpotTurnL,
        stp.cha_cha_cha.SpotTurnR,
        stp.cha_cha_cha.SwitchTurnL,
        stp.cha_cha_cha.SwitchTurnR,
        stp.cha_cha_cha.NaturalTop,
        stp.cha_cha_cha.HandToHandL,
        stp.cha_cha_cha.HandToHandR,
        stp.rumba.CucarachasL,
        stp.rumba.CucarachasR,
        stp.cha_cha_cha.ShoulderToShoulderL,
        stp.cha_cha_cha.ShoulderToShoulderR,
        stp.cha_cha_cha.SideStepToLeft,
        stp.cha_cha_cha.SideStepToRight,
        stp.rumba.CubanRocksL,
        stp.rumba.CubanRocksR,
        stp.cha_cha_cha.Alemana,
        stp.cha_cha_cha.AlemanaFromOpenPosition,
        stp.rumba.OpeningOutToLeft,
        stp.rumba.OpeningOutToRight,
        stp.cha_cha_cha.ClosedHipTwistRShadow,
        stp.cha_cha_cha.ReverseTop,
        stp.cha_cha_cha.OpenHipTwist,
        stp.cha_cha_cha.ReverseTop1_10,
        stp.cha_cha_cha.OpeningOutFromReverseTop,
        stp.cha_cha_cha.Aida,
        stp.cha_cha_cha.Spiral,
        stp.cha_cha_cha.Curl,
        stp.cha_cha_cha.RopeSpinning,
        stp.rumba.SlidingDoors,
        stp.rumba.Fencing,
        stp.rumba.ThreeThrees,
        stp.rumba.ThreeAlemanas,
        stp.rumba.ContinuousHipTwist,
        stp.rumba.ContinuousCircularHipTwist,
    ],
)

"""The syllabus steps for the Samba, as listed in the BDC rulebook."""
SambaSyllabus = Syllabus(
    name="Samba",
    figures=[
        stp.samba.BasicMovementNatural,
        stp.samba.BasicMovementReverse,
        stp.samba.BasicMovementSideL,
        stp.samba.BasicMovementSideR,
        stp.samba.BasicMovementProgressive,
        stp.samba.SambaWhiskToL,
        stp.samba.SambaWhiskToR,
        stp.samba.SambaWalksPromenadeLF,
        stp.samba.SambaWalksPromenadeRF,
        stp.samba.SambaWalksSide,
        stp.samba.SambaWalksStationaryLF,
        stp.samba.SambaWalksStationaryRF,
        stp.samba.RhythmBounceRF,
        stp.samba.RhythmBounceLF,
        stp.samba.BotaFogosTravellingForward,
        stp.samba.BotaFogosTravellingForward,
        stp.samba.PromenadeBotaFogos,
        stp.waltz.ReverseTurn,
        stp.samba.CrissCrossVoltas,
        stp.samba.SoloSpotVoltaLF,
        stp.samba.SoloSpotVoltaRF,
        stp.samba.ContinuousVoltaSpotTurnToRight,
        stp.samba.ContinuousVoltaSpotTurnToLeft,
        stp.samba.SambaFootChange1RF,
        stp.samba.SambaFootChange1LF,
        stp.samba.SambaFootChange2RF,
        stp.samba.SambaFootChange2LF,
        stp.samba.ShadowTravellingVoltasLF,
        stp.samba.ShadowTravellingVoltasRF,
        stp.samba.CortaJaca,
        stp.samba.ClosedRocks,
        stp.samba.NaturalRoll,
        stp.samba.OpenRocks,
        stp.samba.BackwardRocksLF,
        stp.samba.BackwardRocksRF,
        stp.samba.PlaitRF,
        stp.samba.PlaitLF,
        stp.samba.RollingOffTheArm,
        stp.samba.RollingOffTheArmSpinEnding1,
        stp.samba.ArgentineCrosses,
        stp.samba.MaypoleTurningL,
        stp.samba.MaypoleTurningR,
        stp.samba.ShadowCircularVoltaLF,
        stp.samba.ShadowCircularVoltaRF,
        stp.samba.ContraBotaFogos,
        stp.samba.RoundaboutToR,
        stp.samba.RoundaboutToL,
        stp.samba.ReverseRoll,
        stp.samba.PromenadeAndCounterPromenadeRuns,
        stp.samba.SambaFootChange3RFPromenadePosition,
        stp.samba.SambaFootChange3RFOpenPromenadePosition,
        stp.samba.SambaFootChange3LFPromenadePosition,
        stp.samba.SambaFootChange3LFOpenPromenadePosition,
        stp.samba.SambaFootChange4RFPromenadePosition,
        stp.samba.SambaFootChange4LFPromenadePosition,
        stp.samba.ThreeStepTurn,
        stp.samba.SambaLocks,
        stp.samba.CruzadosWalksAndLocksLF,
        stp.samba.CruzadosWalksAndLocksRF,
    ],
)

PasoDobleSyllabus = Syllabus(
    name="Paso Doble",
    figures=[
        stp.paso_doble.BasicMovementSurPlace,
        stp.paso_doble.BasicMovementMarchForward,
        stp.paso_doble.BasicMovementMarchBackward,
        stp.paso_doble.ChassesToR,
        stp.paso_doble.ChassesToL,
        stp.paso_doble.Drag,
        stp.paso_doble.Attack,
        stp.paso_doble.PromenadeLink,
        stp.paso_doble.PromenadeClose,
        stp.paso_doble.Ecart,
        stp.paso_doble.Huit,
        stp.paso_doble.Promenade,
        stp.paso_doble.Separation,
        stp.paso_doble.SeparationFallawayEnding,
        stp.paso_doble.Sixteen,
        stp.paso_doble.PromenadeAndCounterPromenade,
        stp.paso_doble.GrandCircle,
        stp.paso_doble.OpenTelemark,
        stp.paso_doble.TwistTurn,
        stp.paso_doble.LaPasse,
        stp.paso_doble.Banderillas,
        stp.paso_doble.FallawayReverse,
        stp.paso_doble.CoupDePique,
        stp.paso_doble.CoupDePiqueRFToLF,
        stp.paso_doble.CoupDePiqueLFToRF,
        stp.paso_doble.LeftFootVariation,
        stp.paso_doble.SpanishLineInvertedCPP,
        stp.paso_doble.SpanishLineInvertedPP,
        stp.paso_doble.FlamencoTapsInvertedCPP,
        stp.paso_doble.FlamencoTapsInvertedPP,
        stp.paso_doble.SyncopatedSeparation,
        stp.paso_doble.TravellingSpinsFromPP,
        stp.paso_doble.TravellingSpinsFromCPP,
        stp.paso_doble.Fregolina,
        stp.paso_doble.Farol,
        stp.paso_doble.Twists,
        stp.paso_doble.ChasseCape,
    ],
)

JiveSyllabus = Syllabus(
    name="Jive",
    figures=[
        stp.jive.ChasseToLeft,
        stp.jive.ChasseToRight,
        stp.jive.BasicInPlace,
        stp.jive.FallawayRockAction,
        stp.jive.BasicInFallaway,
        stp.jive.FallawayThrowaway,
        stp.jive.Link,
        stp.jive.ChangeOfPlaceRightToLeft,
        stp.jive.ChangeOfPlaceLeftToRight,
        stp.jive.ChangeOfHandsBehindTheBack,
        stp.jive.PromenadeWalks,
        stp.jive.HipBump,
        stp.jive.Whip,
        stp.jive.AmericanSpin,
        stp.jive.StopAndGo,
        stp.jive.Mooch,
        stp.jive.WhipThrowaway,
        stp.jive.ReverseWhip,
        stp.jive.Windmill,
        stp.jive.SpanishArms,
        stp.jive.RollingOffTheArm,
        stp.jive.SimpleSpin,
        stp.jive.MiamiSpecial,
        stp.jive.ChangeOfPlaceRightToLeftWithDoubleSpin,
        stp.jive.OverturnedChangeOfPlaceLeftToRight,
        stp.jive.CurlyWhip,
        stp.jive.OverturnedFallawayThrowaway,
        stp.jive.BallChange,
        stp.jive.ShoulderSpin,
        stp.jive.ToeHeelSwivels,
        stp.jive.Chugging,
        stp.jive.ChickenWalks,
        stp.jive.Catapult,
        stp.jive.StalkerWalksFlicksAndBreak,
    ],
)


def main():
    """Run this to test."""

    opts = [
        WaltzSyllabus,
        QuickstepSyllabus,
        FoxtrotSyllabus,
        TangoSyllabus,
        VienneseWaltzSyllabus,
        ChaChaChaSyllabus,
        RumbaSyllabus,
        SambaSyllabus,
        PasoDobleSyllabus,
        JiveSyllabus,
    ]

    for syllabus in opts:
        routine = syllabus.routine()
        start = routine[0]

        print("* %s routine starting with %s" % (syllabus.name, start.name))
        for i, elem in enumerate(routine):
            print("%2d. %s" % (i + 1, elem.name))

        print()


if __name__ == "__main__":
    main()
