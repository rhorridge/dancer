from dancer.competition import Competition
from dancer.couple import Couple
from dancer.university import University


def test_competition_init():
    leicester_comp = Competition(
        name="Leicester Friendly 2021",
        venue="Braunstone Leisure Centre",
    )
    warwick_comp = Competition(
        name="Warwick Varsity 2021",
        venue="Warwick Arts Centre",
    )
    nottingham_comp = Competition(
        name="Nottingham Varsity 2021",
        venue="Nottingham Girls High School",
    )

    print(leicester_comp.json())
    print(warwick_comp.json())
    print(nottingham_comp.json())


def test_entries_for_nottingham():
    comp = Competition(
        name="Nottingham Varsity 2021",
        venue="Nottingham Girls High School",
    )

    couple = Couple(
        leader="Alex Crotaz",
        follower="Jenny Chantratita",
        university=University.from_str("Birmingham"),
    )
    comp.competitors[16] = couple


def test_fmt_adjudicators():
    """Test that fmt_adjudicators works correctly."""

    adj = Competition(
        name="Test",
        adjudicators={
            "A": dict(name="Foo"),
            "B": dict(name="Bar"),
            "C": dict(name="Baz"),
        },
    )
    assert adj.fmt_adjudicators() == "A B C"
