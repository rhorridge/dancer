from rhorridge/python-poetry-docker:0.1.2 as base
arg APP_NAME=dancer
arg APP_PATH=/opt/app/
workdir $APP_PATH
copy ./poetry.lock ./pyproject.toml ./
copy ./$APP_NAME ./$APP_NAME

from base as build
arg APP_PATH=/opt/app/
workdir $APP_PATH
run poetry build --format wheel
run poetry export --format requirements.txt --output constraints.txt --without-hashes



from base as development
arg APP_NAME=dancer
arg APP_PATH=/opt/app/
workdir $APP_PATH
run poetry install
entrypoint ["poetry", "run"]
cmd ["uvicorn", "--host", "0.0.0.0", "dancer.webapp:app", "--reload"]

from python:3.10 as production
arg APP_NAME=dancer
arg APP_PATH=/opt/app/

env \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1
env \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100
workdir $APP_PATH
copy --from=build /opt/app/dist/*.whl ./
copy --from=build /opt/app/constraints.txt ./
run pip install ./$APP_NAME-*.whl --constraint constraints.txt
env PORT=8000
env APP_NAME=$APP_NAME
copy ./docker/docker-entrypoint.sh /docker-entrypoint.sh
run chmod +x /docker-entrypoint.sh
entrypoint ["/docker-entrypoint.sh"]
cmd ["uvicorn", "--host", "0.0.0.0", "--port", "$PORT", "dancer.webapp:app"]
